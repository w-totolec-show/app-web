﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Core.Entities;
using Core.Infrastructure;
using ClosedXML.Excel;
using Core.Services;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Logging;

namespace Web.Areas.Identity.Pages.Account
{
    [AllowAnonymous]
    public class LoginModel : PageModel
    {
        private readonly ILogger<LoginModel> _logger;
        private readonly SignInManager<AppUser> _signInManager;
        private readonly EmailUtilities _emailUtilities;

        public LoginModel(
            SignInManager<AppUser> signInManager,
            ILogger<LoginModel> logger, 
            EmailUtilities emailUtilities)
        {
            _signInManager = signInManager;
            _logger = logger;
            _emailUtilities = emailUtilities;
        }

        [BindProperty]
        public InputModel Input { get; set; }

        public IList<AuthenticationScheme> ExternalLogins { get; set; }

        public string ReturnUrl { get; set; }

        [TempData]
        public string ErrorMessage { get; set; }

        public async Task OnGetAsync(string remoteError, string returnUrl = null)
        {
            ExternalLogins = (await _signInManager.GetExternalAuthenticationSchemesAsync()).ToList();
            
            if (!string.IsNullOrEmpty(ErrorMessage)) ModelState.AddModelError(string.Empty, ErrorMessage);

            returnUrl = returnUrl ?? Url.Content("~/");

            // Clear the existing external cookie to ensure a clean login process
            await HttpContext.SignOutAsync(IdentityConstants.ExternalScheme);

            ExternalLogins = (await _signInManager.GetExternalAuthenticationSchemesAsync()).ToList();

            if (remoteError != null && remoteError.Equals("DuplicateUserName"))
                ModelState.AddModelError(string.Empty, "O e-mail informado já está cadastrado");
            
            ReturnUrl = returnUrl;
        }

        public async Task<IActionResult> OnPostAsync(string returnUrl = null)
        {
            returnUrl ??= Url.Content("~/");

            if (ModelState.IsValid)
            {
                // This doesn't count login failures towards account lockout
                // To enable password failures to trigger account lockout, set lockoutOnFailure: true
                var result = await _signInManager.PasswordSignInAsync(Input.Email, Input.Password, Input.RememberMe, false);
                if (result.Succeeded)
                {
                    try
                    {
                        var appUser = _signInManager.UserManager.Users.SingleOrDefault(u => u.UserName == Input.Email);
                        if (appUser != null)
                        {
                            var userRoles = await _signInManager.UserManager.GetRolesAsync(appUser);
                            if (!userRoles.Contains("Client")) returnUrl = "~/Admin";
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                    }
                    
                    _logger.LogInformation("Usuário conectado");
                    return LocalRedirect(returnUrl);
                }

                if (result.RequiresTwoFactor) return RedirectToPage("./LoginWith2fa", new {ReturnUrl = returnUrl, Input.RememberMe});
                if (result.IsLockedOut) 
                {
                    _logger.LogWarning("Sua conta está bloqueada.");
                    ExternalLogins = (await _signInManager.GetExternalAuthenticationSchemesAsync()).ToList();
                    ModelState.AddModelError(string.Empty, "Seu usuário está bloqueado, por favor entre em contato com o administrador.");
                    return Page();
                }

                ExternalLogins = (await _signInManager.GetExternalAuthenticationSchemesAsync()).ToList();
                ModelState.AddModelError(string.Empty, "E-mail ou senha inválida");
                return Page();
            }

            // If we got this far, something failed, redisplay form
            return Page();
        }

        public class InputModel
        {
            [Required(ErrorMessage = "Insira seu e-mail")]
            [EmailAddress(ErrorMessage = "Insira um e-mail válido")]
            public string Email { get; set; }

            [Required(ErrorMessage = "Insira sua senha")]
            [DataType(DataType.Password)]
            public string Password { get; set; }

            [Display(Name = "Continuar Conectado?")]
            public bool RememberMe { get; set; }
        }
    }
}