﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Core.Entities;
using Core.Services.Interfaces;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Logging;

namespace Web.Areas.Identity.Pages.Account
{
    [AllowAnonymous]
    public class RegisterModel : PageModel
    {
        private readonly IEmailSender _emailSender;
        private readonly ILogger<RegisterModel> _logger;
        private readonly SignInManager<AppUser> _signInManager;
        private readonly UserManager<AppUser> _userManager;
        private readonly IUsersService _usersService;
        private readonly ITermsService _termsService;

        public RegisterModel(
            UserManager<AppUser> userManager,
            SignInManager<AppUser> signInManager,
            ILogger<RegisterModel> logger,
            IEmailSender emailSender,
            IUsersService usersService,
            ITermsService termsService)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _logger = logger;
            _emailSender = emailSender;
            _usersService = usersService;
            _termsService = termsService;
        }

        [BindProperty]
        public InputModel Input { get; set; }

        public string ReturnUrl { get; set; }

        public IList<AuthenticationScheme> ExternalLogins { get; set; }

        public async Task OnGetAsync(string returnUrl = null)
        {
            ReturnUrl = returnUrl;
            var term = await _termsService.GetAsync();
            if (term != null)
            {
                ViewData["TermsOfServices"] = term;
            }
            ExternalLogins = (await _signInManager.GetExternalAuthenticationSchemesAsync()).ToList();
        }

        public async Task<IActionResult> OnPostAsync(string returnUrl = null)
        {
            returnUrl = returnUrl ?? Url.Content("~/");
            ExternalLogins = (await _signInManager.GetExternalAuthenticationSchemesAsync()).ToList();
            if (ModelState.IsValid)
            {
                var user = new AppUser
                {
                    UserName = Input.Email, 
                    Email = Input.Email,
                    CreatedAt = DateTime.UtcNow,
                    IsActive = true,
                    IsAdmin = false,
                    SecurityStamp = Guid.NewGuid().ToString()
                };
                
                var result = await _userManager.CreateAsync(user, Input.Password);
                
                if (result.Succeeded)
                {
                    await _userManager.AddToRolesAsync(user, new[] {"Client"});
                    
                    _logger.LogInformation("User created a new account with password.");

                    //await _signInManager.SignInAsync(user, false); 
                    await _signInManager.PasswordSignInAsync(Input.Email, Input.Password, false, false);

                    return LocalRedirect(returnUrl);
                }

                bool isEmailDuplicate = false;
                foreach (var error in result.Errors)
                {
                    if (error.Description.Contains("O e-mail") && !isEmailDuplicate)
                    {
                        isEmailDuplicate = true;
                        ModelState.AddModelError(string.Empty, error.Description);    
                    } else if (!error.Description.Contains("O e-mail"))
                    {
                        ModelState.AddModelError(string.Empty, error.Description);
                    }
                }
            }

            // If we got this far, something failed, redisplay form
            return Page();
        }

        public class InputModel
        {
            [DisplayName("E-mail")]
            [EmailAddress(ErrorMessage = "Insira um e-mail válido")]
            [Required(ErrorMessage = "Seu e-mail é necessário")]
            public string Email { get; set; }

            [DisplayName("Senha")]
            [Required(ErrorMessage = "Sua senha é necessária")]
            [StringLength(100, ErrorMessage = "Sua senha deve conter no minímo 6 caracteres com letras e números", MinimumLength = 6)]
            [DataType(DataType.Password)]
            public string Password { get; set; }
        
            public bool AcceptedTerm{ get; set; }
        }
    }
}