﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Core.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Logging;

namespace Web.Areas.Identity.Pages.Account
{
    [AllowAnonymous]
    public class ExternalLoginModel : PageModel
    {
        private readonly IEmailSender _emailSender;
        private readonly ILogger<ExternalLoginModel> _logger;
        private readonly SignInManager<AppUser> _signInManager;
        private readonly UserManager<AppUser> _userManager;

        public ExternalLoginModel(
            SignInManager<AppUser> signInManager,
            UserManager<AppUser> userManager,
            ILogger<ExternalLoginModel> logger,
            IEmailSender emailSender)
        {
            _signInManager = signInManager;
            _userManager = userManager;
            _logger = logger;
            _emailSender = emailSender;
        }

        [BindProperty]
        public InputModel Input { get; set; }

        public string LoginProvider { get; set; }

        public string ReturnUrl { get; set; }

        [TempData]
        public string ErrorMessage { get; set; }

        public IActionResult OnGetAsync()
        {
            return RedirectToPage("./Login");
        }

        public IActionResult OnPost(string provider, string returnUrl = null)
        {
            // Request a redirect to the external login provider.
            /*var redirectUrl = Url.Page(
                    "./ExternalLogin",
                    "Callback",
                    new {returnUrl},
                    "https");*/
            
            var redirectUrl = Url.Page("./ExternalLogin", pageHandler: "Callback", values: new {returnUrl});
            
            Console.WriteLine("REDIRECT URL");
            Console.WriteLine(redirectUrl);
            
            var properties = _signInManager.ConfigureExternalAuthenticationProperties(provider, redirectUrl);
            return new ChallengeResult(provider, properties);
        }

        public async Task<IActionResult> OnGetCallbackAsync(string returnUrl = null, string remoteError = null)
        {
            returnUrl = returnUrl ?? Url.Content("~/");
            if (remoteError != null)
            {
                ErrorMessage = $"Error from external provider: {remoteError}";
                return RedirectToPage("./Login", new {ReturnUrl = returnUrl});
            }

            var info = await _signInManager.GetExternalLoginInfoAsync();
            if (info == null)
            {
                ErrorMessage = "Error loading external login information.";
                return RedirectToPage("./Login", new {ReturnUrl = returnUrl});
            }

            // Sign in the user with this external login provider if the user already has a login.
            var result = await _signInManager.ExternalLoginSignInAsync(info.LoginProvider, info.ProviderKey, false, true);
            if (result.Succeeded)
            {
                _logger.LogInformation("{Name} logged in with {LoginProvider} provider.", info.Principal.Identity.Name, info.LoginProvider);
                return LocalRedirect(returnUrl);
            }

            // Register user 
            if (info.Principal.HasClaim(c => c.Type == ClaimTypes.Email))
                Input = new InputModel
                {
                    Email = info.Principal.FindFirstValue(ClaimTypes.Email)
                };

            if (info.ProviderDisplayName.Equals("Google"))
            {
                var register =  await RegisterUser(info);
                if (register.Item1.Succeeded)
                {
                    await _signInManager.SignInAsync(register.Item2, false, info.LoginProvider);

                    return LocalRedirect(returnUrl);
                }

                remoteError = string.Empty;
                foreach (var error in register.Item1.Errors)
                {
                    ModelState.AddModelError(string.Empty, error.Description);

                    if (error.Code == "DuplicateUserName") remoteError = error.Code;
                }
                
                
                return RedirectToPage("./Login", new {remoteError, ReturnUrl = returnUrl});
            }
            else
            {
                if (!string.IsNullOrEmpty(Input.Email))
                {
                    var exist = await _userManager.FindByEmailAsync(Input.Email);
                    if (exist == null)
                    {
                        var register =  await RegisterUser(info);
                        if (register.Item1.Succeeded)
                        {
                            await _signInManager.SignInAsync(register.Item2, false, info.LoginProvider);

                            return LocalRedirect(returnUrl);
                        }
                    }
                }
            }

            if (result.IsLockedOut) return RedirectToPage("./Lockout");

            // If the user does not have an account, then ask the user to create an account.
            ReturnUrl = returnUrl;
            LoginProvider = info.LoginProvider;
            return Page();
        }

        public async Task<IActionResult> OnPostConfirmationAsync(string returnUrl = null)
        {
            returnUrl = returnUrl ?? Url.Content("~/");
            // Get the information about the user from the external login provider
            var info = await _signInManager.GetExternalLoginInfoAsync();
            if (info == null)
            {
                ErrorMessage = "Error loading external login information during confirmation.";
                return RedirectToPage("./Login", new {ReturnUrl = returnUrl});
            }

            if (ModelState.IsValid)
            {
                var user = new AppUser {UserName = Input.Email, Email = Input.Email};
                var result = await _userManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    result = await _userManager.AddLoginAsync(user, info);
                    if (result.Succeeded)
                    {
                        _logger.LogInformation("User created an account using {Name} provider.", info.LoginProvider);

                        var userId = await _userManager.GetUserIdAsync(user);
                        var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                        code = WebEncoders.Base64UrlEncode(Encoding.UTF8.GetBytes(code));
                        var callbackUrl = Url.Page(
                            "/Account/ConfirmEmail",
                            null,
                            new {area = "Identity", userId, code},
                            Request.Scheme);

                        await _emailSender.SendEmailAsync(Input.Email, "Confirm your email",
                            $"Please confirm your account by <a href='{HtmlEncoder.Default.Encode(callbackUrl)}'>clicking here</a>.");

                        // If account confirmation is required, we need to show the link if we don't have a real email sender
                        if (_userManager.Options.SignIn.RequireConfirmedAccount) return RedirectToPage("./RegisterConfirmation", new {Input.Email});

                        await _signInManager.SignInAsync(user, false, info.LoginProvider);

                        return LocalRedirect(returnUrl);
                    }
                }

                foreach (var error in result.Errors) ModelState.AddModelError(string.Empty, error.Description);
            }

            LoginProvider = info.LoginProvider;
            ReturnUrl = returnUrl;
            return Page();
        }

        private async Task<(IdentityResult, AppUser)> RegisterUser(ExternalLoginInfo info)
        {
            var name = info.Principal.Identity.Name;
            var user = new AppUser
            {
                UserName = Input.Email, 
                Email = Input.Email,
                FirstName = name?.Substring(0, name.IndexOf(" ", StringComparison.Ordinal)),
                LastName = name?.Substring(name.IndexOf(" ", StringComparison.Ordinal) + 1),
                CreatedAt = DateTime.UtcNow,
                IsActive = true,
                IsAdmin = false,
                SecurityStamp = Guid.NewGuid().ToString()
            };

            var register = await _userManager.CreateAsync(user);
            if (register.Succeeded)
            {
                await _userManager.AddToRolesAsync(user, new[] {"Client"});
                
                _logger.LogInformation("User created an account using {Name} provider.", info.LoginProvider);
                        
                register = await _userManager.AddLoginAsync(user, info);
            } 

            return (register, user);
        }
        
        public class InputModel
        {
            [Required]
            [EmailAddress]
            public string Email { get; set; }
        }
    }
}