﻿using System;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Text;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Core.Entities;
using Core.Infrastructure;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.WebUtilities;
using ServiceStack;

namespace Web.Areas.Identity.Pages.Account
{
    [AllowAnonymous]
    public class ForgotPasswordModel : PageModel
    {
        private readonly IEmailSender _emailSender;
        private readonly UserManager<AppUser> _userManager;
        private readonly EmailUtilities _emailUtilities;

        public ForgotPasswordModel(UserManager<AppUser> userManager, IEmailSender emailSender, EmailUtilities emailUtilities)
        {
            _userManager = userManager;
            _emailSender = emailSender;
            _emailUtilities = emailUtilities;
        }

        [BindProperty]
        public InputModel Input { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var user = await _userManager.FindByEmailAsync(Input.Email);
                    if (user == null)
                    {
                        ViewData["Error"] = "Usuário não encontrado, por favor verifique o e-mail inserido.";
                        return Page();
                    }

                    // For more information on how to enable account confirmation and password reset please 
                    // visit https://go.microsoft.com/fwlink/?LinkID=532713
                    var code = await _userManager.GeneratePasswordResetTokenAsync(user);
                    code = WebEncoders.Base64UrlEncode(Encoding.UTF8.GetBytes(code));
                    var callbackUrl = Url.Page(
                        "/Account/ResetPassword",
                        null,
                        new {area = "Identity", code},
                        Request.Scheme);

                    var path = Path.Combine(
                        Directory.GetCurrentDirectory(),
                        "EmailTemplates", "EmailResetPassword" + ".html");
                
                    string html = await System.IO.File.ReadAllTextAsync(path);
                    string emailReplace = html.ReplaceAll("#codeUser#", HtmlEncoder.Default.Encode(callbackUrl));

                    await _emailSender.SendEmailAsync(
                        Input.Email,
                        "Redefinir Senha - Totolec Show",
                        emailReplace);

                    ViewData["EmailSend"] = "Send";
                    return Page();
                }
                catch (Exception)
                {
                    ViewData["Error"] = "Erro ao processar sua solicitação";
                }
            }

            return Page();
        }

        public class InputModel
        {
            [Required(ErrorMessage = "Insira seu e-mail")]
            [EmailAddress(ErrorMessage = "Insira um e-mail válido")]
            public string Email { get; set; }
        }
    }
}