﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Core.Entities;

namespace Web.Areas.Admin.ViewModels.Shared
{
    public class AddressViewModel
    {
        [DisplayName("* País")]
        [Required(ErrorMessage = "País obrigatório")]
        public string Country { get; set; }

        [DisplayName("* Estado")]
        [Required(ErrorMessage = "Estado obrigatório")]
        public string State { get; set; }

        [DisplayName("* Cidade")]
        [Required(ErrorMessage = "Cidade obrigatório")]
        public string City { get; set; }

        [DisplayName("* Código Postal")]
        [Required(ErrorMessage = "Código postal obrigatório")]
        public string Zipcode { get; set; }

        [DisplayName("* Bairro")]
        [Required(ErrorMessage = "Bairro obrigatório")]
        public string Neighborhood { get; set; }

        [DisplayName("* Linha 1")]
        [Required(ErrorMessage = "Linha 1 obrigatório")]
        public string Line1 { get; set; }

        [DisplayName("Linha 2")]
        public string Line2 { get; set; }

        [DisplayName("Linha 3")]
        public string Line3 { get; set; }

        [DisplayName("Linha 4")]
        public string Line4 { get; set; }

        [DisplayName("Número")]
        public string Number { get; set; }

        [DisplayName("Observações")]
        public string Observations { get; set; }

        [DisplayName("Latitude")]
        public double? Latitude { get; set; }

        [DisplayName("Longitude")]
        public double? Longitude { get; set; }

        public static AddressViewModel FromModel(Address address)
        {
            var vm = new AddressViewModel();

            if (address != null)
            {
                vm.Country = address.Country;
                vm.State = address.State;
                vm.City = address.City;
                vm.Zipcode = address.Zipcode;
                vm.Neighborhood = address.Neighborhood;
                vm.Line1 = address.Line1;
                vm.Line2 = address.Line2;
                vm.Line3 = address.Line3;
                vm.Line4 = address.Line4;
                vm.Number = address.Number;
                vm.Observations = address.Observations;
                vm.Latitude = address.Latitude;
                vm.Longitude = address.Longitude;
            }

            return vm;
        }
    }

    public static class AddressViewModelExtensionMethods
    {
        public static Address ToModel(this AddressViewModel vm)
        {
            return new Address
            {
                Country = vm.Country,
                State = vm.State,
                City = vm.City,
                Zipcode = vm.Zipcode,
                Neighborhood = vm.Neighborhood,
                Line1 = vm.Line1,
                Line2 = vm.Line2,
                Line3 = vm.Line3,
                Line4 = vm.Line4,
                Number = vm.Number,
                Observations = vm.Observations,
                Latitude = vm.Latitude ?? 0,
                Longitude = vm.Longitude ?? 0
            };
        }
    }
}