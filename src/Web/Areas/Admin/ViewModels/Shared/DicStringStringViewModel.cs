namespace Web.Areas.Admin.ViewModels.Shared
{
    public class DicStringStringViewModel
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }
}