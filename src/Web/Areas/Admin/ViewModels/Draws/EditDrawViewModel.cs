using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Core.Entities;
using Core.Utils;
using Web.Areas.Admin.ViewModels.Shared;

namespace Web.Areas.Admin.ViewModels.Draws
{
    public class EditDrawViewModel
    {
        public Guid? Guid { get; set; }
        
        [DisplayName("Extração/Concurso*")]
        [Required(ErrorMessage = "Número da extração obrigatório")]
        public int DrawNumber { get; set; }
        
        [DisplayName("Nome")]
        public string Name { get; set; }
        public IList<Ticket> Tickets { get; set; }
        
        [DisplayName("Valor da cartela*")]
        [Required(ErrorMessage = "Valor da cartela obrigatório")]
        [DisplayFormat(DataFormatString="{0:C}")]
        public decimal ValueOfTicket { get; set; }
        
        [DisplayName("Início da venda*")]
        [Required(ErrorMessage = "Início da venda obrigatório")]
        public DateTime StartDatePurchase { get; set; }
        
        [DisplayName("Fim da venda*")]
        [Required(ErrorMessage = "Fim da venda obrigatório")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy hh:mm}")]
        public DateTime EndDatePurchase { get; set; }
        public string OriginalLayoutName { get; set; }
        public string LayoutUri { get; set; }
        
        [DisplayName("Data do sorteio*")]
        [Required(ErrorMessage = "Data do sorteio obrigatório")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy hh:mm}")]
        public DateTime DrawDate { get; set; }
        public bool Accomplished { get; set; }
        
        [DisplayName("Observações")]
        public string Observations { get; set; }
        
        [DisplayName("URL do VT")]
        public string UrlCommercial { get; set; }
        
        [DisplayName("Descrição do sorteio")]
        public string DescriptionOfDraw { get; set; }
        
        [DisplayName("Descrição do resultado")]
        public string DescriptionOfResult { get; set; }
        
        [DisplayName("Ativar sorteio?")]
        public bool IsActive { get; set; }
        public int Sold { get; set; }
        public int Pending { get; set; }
        public IList<FileImport> FileImports { get; set; }
        public StatusDraw StatusDraw { get; set; }
        public int CountFileImports { get; set; }
        public DicStringStringViewModel[] Parameters { get; set; }
        
        public static EditDrawViewModel FromModel(Draw draw, int countSold, int countPending)
        {
            var vm = new EditDrawViewModel();

            if (draw != null)
            {
                vm.Guid = draw.Guid;
                vm.DrawNumber = draw.DrawNumber;
                vm.Name = draw.Name;
                vm.ValueOfTicket = draw.ValueOfTicket;
                vm.StartDatePurchase = draw.StartDatePurchase;
                vm.EndDatePurchase = draw.EndDatePurchase;
                vm.DrawDate = draw.DrawDate;
                vm.Observations = draw.Observations;
                vm.Accomplished = draw.Accomplished;
                vm.UrlCommercial = draw.UrlCommercial;
                vm.DescriptionOfDraw = draw.DescriptionOfDraw;
                vm.DescriptionOfResult = draw.DescriptionOfResult;
                vm.Sold = countSold;
                vm.Pending = countPending;
                vm.FileImports = draw.FileImports;
                vm.CountFileImports = draw.FileImports?.Count ?? 0;
                vm.IsActive = draw.IsActive;
                vm.Parameters = draw.Parameters != null
                    ? draw.Parameters.Select(d => new DicStringStringViewModel
                    {
                        Key = d.Key,
                        Value = d.Value
                    }).ToArray()
                    : new DicStringStringViewModel[] { };

                DateTime dateTimeUtc = DateTime.UtcNow;
                var brazilDatetime = dateTimeUtc.ToLocalString();
                DateTime dateTime = DateTime.Parse(brazilDatetime);
                
                if (draw.StartDatePurchase > dateTime)
                {
                    vm.StatusDraw = StatusDraw.NotStarted;
                    return vm;
                }

                if (draw.StartDatePurchase <= dateTime && !(draw.EndDatePurchase <= dateTime) && vm.IsActive)
                {
                    vm.StatusDraw = StatusDraw.InProgress;
                    return vm;
                }

                if (draw.DrawDate >= dateTime && draw.EndDatePurchase <= dateTime || draw.DrawDate <= dateTime)
                {
                    vm.StatusDraw = StatusDraw.Finished;
                    return vm;
                }
                else
                {
                    vm.StatusDraw = StatusDraw.NotStarted;
                    return vm;
                }
            }
            else
            {
                vm.ValueOfTicket = 10;
                DateTime dateTimeUtc = DateTime.UtcNow;
                var brazilDatetime = dateTimeUtc.ToLocalString();
                DateTime dateTime = DateTime.Parse(brazilDatetime);
                vm.DrawDate = dateTime;
                vm.StartDatePurchase = dateTime;
                vm.EndDatePurchase = dateTime;
            }

            return vm;
        }
    }
    
    public enum StatusDraw
    {
        NotStarted,
        InProgress,
        Finished
    }
}