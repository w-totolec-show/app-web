using System;
using System.ComponentModel;
using Core.Entities;
using Core.Entities.AppLogs;
using Web.Areas.Admin.ViewModels.Users;

namespace Web.Areas.Admin.ViewModels.AppLogs
{
    public class AppLogViewModel
    {
        public Guid? Guid { get; set; }

        [DisplayName("Data Criação")]
        public DateTime CreatedAt { get; set; }

        [DisplayName("Tipo Log")]
        public string LogType { get; set; }

        [DisplayName("Usuário")]
        public UserViewModel User { get; set; }

        public static AppLogViewModel FromModel(Log log, AppUser user)
        {
            var vm = new AppLogViewModel();

            if (log != null)
            {
                vm.Guid = log.Guid;
                vm.CreatedAt = log.CreatedAt;
                vm.LogType = log.LogType.ToString();
                vm.User = UserViewModel.FromModel(user, null);
            }

            return vm;
        }
    }
}