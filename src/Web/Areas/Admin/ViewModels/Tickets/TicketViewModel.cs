using System;
using System.ComponentModel;
using Core.Entities;

namespace Web.Areas.Admin.ViewModels.Tickets
{
    public class TicketViewModel
    {
        public Guid Guid { get; set; }
        public Guid DrawGuid { get; set; }
        
        [DisplayName("Número")]
        public string TicketNumber { get; set; }
        
        [DisplayName("Número simplificado")]
        public string SimplifiedTicketNumber { get; set; }
        
        [DisplayName("Extração/Concurso")]
        public string ExtractionNumber { get; set; }
        
        [DisplayName("Bolas")]
        public int[] TicketBalls { get; set; }
        
        [DisplayName("Ano do sorteio")]
        public string DrawYear { get; set; }
        
        [DisplayName("Vendido?")]
        public bool Sold { get; set; }
        
        [DisplayName("Registro")]
        public DateTime CreatedAt { get; set; }
        
        [DisplayName("Data de compra")]
        public DateTime? PurchaseDate { get; set; }
        public AppUser AppUser { get; set; }
        public string GatewayPayment { get; set; }
        public string IdTransaction { get; set; }
        public bool IsActive { get; set; }
        
        public static TicketViewModel FromModel(Ticket ticket)
        {
            var vm = new TicketViewModel();

            if (ticket != null)
            {
                vm.Guid = ticket.Guid;
                vm.DrawGuid = ticket.DrawGuid;
                vm.TicketNumber = ticket.TicketNumber;
                vm.SimplifiedTicketNumber = ticket.SimplifiedTicketNumber;
                vm.ExtractionNumber = ticket.ExtractionNumber;
                vm.TicketBalls = ticket.TicketBalls;
                vm.DrawYear = ticket.DrawYear;
                vm.Sold = ticket.Sold;
                vm.CreatedAt = ticket.CreatedAt;
                vm.PurchaseDate = ticket.PurchaseDate;
                vm.AppUser = ticket.AppUser;
                vm.GatewayPayment = ticket.GatewayPayment;
                vm.IdTransaction = ticket.IdTransaction;
                vm.IsActive = ticket.IsActive;
            }
            
            return vm;
        }
    }
}