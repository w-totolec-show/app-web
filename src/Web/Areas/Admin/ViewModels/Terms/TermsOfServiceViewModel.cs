using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Web.Areas.Admin.ViewModels.Terms
{
    public class TermsOfServiceViewModel
    {
        [DisplayName("Novo termo de serviço")]
        [Required(ErrorMessage = "Termo obrigatório")]
        public string Message { get; set; }
    }
}