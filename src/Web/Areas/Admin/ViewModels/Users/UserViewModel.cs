﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Core.Entities;
using Web.Areas.Admin.ViewModels.Shared;

namespace Web.Areas.Admin.ViewModels.Users
{
    public class UserViewModel
    {
        public Guid? Guid { get; set; }

        [DisplayName("Usuário")]
        public string UserName { get; set; }

        [DisplayName("Nome")]
        public string FirstName { get; set; }

        [DisplayName("Sobrenome")]
        public string Lastname { get; set; }

        [DisplayName("E-mail")]
        public string Email { get; set; }

        [DisplayName("Observações")]
        public string Observations { get; set; }

        [DisplayName("Permissões")]
        public string[] Roles { get; set; }

        [DisplayName("Outras informações")]
        public DicStringStringViewModel[] Data { get; set; }
        
        [DisplayName("CPF")]
        public string SocialId { get; set; } // CPF
        
        [DisplayName("CEP")]
        public string Zipcode { get; set; }
        
        [DisplayName("Data de nascimento")]
        public string Birthday { get; set; }
        
        [DisplayName("Celular")]
        public string Phone { get; set; }

        public bool IsActive { get; set; }

        public static UserViewModel FromModel(AppUser user, IEnumerable<string> Roles)
        {
            var vm = new UserViewModel();

            if (user != null)
            {
                vm.Guid = user.Id;
                vm.UserName = user.UserName;
                vm.FirstName = user.FirstName;
                vm.Lastname = user.LastName;
                vm.Observations = user.Observations;
                vm.Email = user.Email;
                vm.IsActive = user.IsActive;
                vm.Roles = Roles?.ToArray();
                vm.SocialId = user.SocialId;
                vm.Zipcode = user.Zipcode;
                vm.Phone = user.PhoneNumber;
                vm.Birthday = user.DateOfBirth.ToShortDateString();
                vm.Data = user.Data != null
                    ? user.Data.Select(d => new DicStringStringViewModel
                    {
                        Key = d.Key,
                        Value = d.Value
                    }).ToArray()
                    : new DicStringStringViewModel[] { };
            }

            return vm;
        }
    }
}