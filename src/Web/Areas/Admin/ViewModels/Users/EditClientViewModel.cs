using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Core.Entities;
using Web.Areas.Admin.ViewModels.Shared;

namespace Web.Areas.Admin.ViewModels.Users
{
    public class EditClientViewModel
    {
        public Guid? Guid { get; set; }

        [DisplayName("Usuário*")]
        public string UserName { get; set; }

        [DisplayName("Nome*")]
        [Required(ErrorMessage = "Nome obrigatório")]
        public string FirstName { get; set; }

        [DisplayName("Sobrenome*")]
        [Required(ErrorMessage = "Sobrenome obrigatório")]
        public string Lastname { get; set; }

        [DisplayName("E-mail*")]
        [EmailAddress(ErrorMessage = "Digite um E-mail válido")]
        [Required(ErrorMessage = "E-mail obrigatório")]
        public string Email { get; set; }

        [DisplayName("Observações")]
        public string Observations { get; set; }

        [DisplayName("Outras informações")]
        public DicStringStringViewModel[] Data { get; set; }
        
        [DisplayName("CPF*")]
        [Required(ErrorMessage = "CPF obrigatório")]
        public string SocialId { get; set; }
        
        [DisplayName("CEP*")]
        [Required(ErrorMessage = "CEP obrigatório")]
        public string Zipcode { get; set; }
        
        [DisplayName("Data de nascimento*")]
        [Required(ErrorMessage = "Data de nascimento obrigatório")]
        public DateTime DateOfBirth { get; set; }
        
        [DisplayName("Celular*")]
        [Required(ErrorMessage = "Celular obrigatório")]
        public string PhoneNumber { get; set; }
        public bool CompleteRegistration { get; set; }
        public bool IsActive { get; set; }
        
        public static EditClientViewModel FromModel(AppUser user, IEnumerable<string> Roles)
        {
            var vm = new EditClientViewModel();

            if (user != null)
            {
                vm.Guid = user.Id;
                vm.UserName = user.UserName;
                vm.FirstName = user.FirstName;
                vm.Lastname = user.LastName;
                vm.Observations = user.Observations;
                vm.Email = user.Email;
                vm.IsActive = user.IsActive;
                vm.SocialId = user.SocialId;
                vm.Zipcode = user.Zipcode;
                vm.DateOfBirth = user.DateOfBirth;
                vm.PhoneNumber = user.PhoneNumber;
                vm.CompleteRegistration = user.CompleteRegistration;
                vm.Data = user.Data != null
                    ? user.Data.Select(d => new DicStringStringViewModel
                    {
                        Key = d.Key,
                        Value = d.Value
                    }).ToArray()
                    : new DicStringStringViewModel[] { };
            }

            return vm;
        }
    }
}