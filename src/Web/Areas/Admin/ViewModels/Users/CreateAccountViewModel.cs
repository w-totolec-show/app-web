using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Web.Areas.Admin.ViewModels.Users
{
    public class CreateAccountViewModel
    {
        [DisplayName("Usuário*")]
        [Required(ErrorMessage = "Usuário obrigatório")]
        public string UserName { get; set; }

        [DisplayName("Nome*")]
        [Required(ErrorMessage = "Nome obrigatório")]
        public string FirstName { get; set; }

        [DisplayName("Sobrenome*")]
        [Required(ErrorMessage = "Sobrenome obrigatório")]
        public string Lastname { get; set; }

        [DisplayName("E-mail*")]
        [EmailAddress(ErrorMessage = "Digite um E-mail válido")]
        [Required(ErrorMessage = "E-mail obrigatório")]
        public string Email { get; set; }

        [DisplayName("Senha")]
        [StringLength(100, ErrorMessage = "A senha deve ter pelo menos {2} caracteres.", MinimumLength = 8)]
        [RegularExpression("([a-zA-Z]+[0-9]+)", ErrorMessage = "Sua senha deve conter letras e números")]
        public string Password { get; set; }

        [DisplayName("Celular")]
        [Required(ErrorMessage = "Celular é obrigatório")]
        public string Phone { get; set; }
        
        [DisplayName("CPF")]
        [Required(ErrorMessage = "CPF é obrigatório")]
        public string SocialId { get; set; } // CPF
        
        public bool AcceptedTerm{ get; set; }
    }
}