﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Core.Entities;
using Web.Areas.Admin.ViewModels.Shared;

namespace Web.Areas.Admin.ViewModels.Users
{
    public class EditUserViewModel
    {
        public Guid? Guid { get; set; }

        [DisplayName("Usuário*")]
        public string UserName { get; set; }

        [DisplayName("Nome*")]
        [Required(ErrorMessage = "Nome obrigatório")]
        public string FirstName { get; set; }

        [DisplayName("Sobrenome*")]
        [Required(ErrorMessage = "Sobrenome obrigatório")]
        public string Lastname { get; set; }

        [DisplayName("E-mail*")]
        [EmailAddress(ErrorMessage = "Digite um E-mail válido")]
        [Required(ErrorMessage = "E-mail obrigatório")]
        public string Email { get; set; }

        [DisplayName("Senha")]
        [StringLength(100, ErrorMessage = "A senha deve ter pelo menos {2} caracteres.", MinimumLength = 8)]
        public string Password { get; set; }

        [DisplayName("Confirme a senha")]
        [Compare("Password", ErrorMessage = "As senhas não são iguais")]
        public string ConfirmPassword { get; set; }

        [DisplayName("Observações")]
        public string Observations { get; set; }

        [DisplayName("Permissões*")]
        [Required(ErrorMessage = "Selecione uma permissão")]
        public string[] Roles { get; set; }

        [DisplayName("Outras informações")]
        public DicStringStringViewModel[] Data { get; set; }
        
        [DisplayName("CPF")]
        public string SocialId { get; set; }
        
        [DisplayName("CEP")]
        public string Zipcode { get; set; }
        
        [DisplayName("Data de nascimento")]
        public DateTime DateOfBirth { get; set; }
        
        [DisplayName("Celular")]
        public string PhoneNumber { get; set; }
        public bool IsActive { get; set; }

        public static EditUserViewModel FromModel(AppUser user, IEnumerable<string> Roles)
        {
            var vm = new EditUserViewModel();

            if (user != null)
            {
                vm.Guid = user.Id;
                vm.UserName = user.UserName;
                vm.FirstName = user.FirstName;
                vm.Lastname = user.LastName;
                vm.Observations = user.Observations;
                vm.Email = user.Email;
                vm.IsActive = user.IsActive;
                vm.Roles = Roles?.ToArray();
                vm.SocialId = user.SocialId;
                vm.Zipcode = user.Zipcode;
                vm.DateOfBirth = user.DateOfBirth;
                vm.PhoneNumber = user.PhoneNumber;
                vm.Data = user.Data != null
                    ? user.Data.Select(d => new DicStringStringViewModel
                    {
                        Key = d.Key,
                        Value = d.Value
                    }).ToArray()
                    : new DicStringStringViewModel[] { };
            }

            return vm;
        }
    }
}