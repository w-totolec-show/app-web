using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Core.Entities;
using Web.Areas.Admin.ViewModels.Users;

namespace Web.Areas.Admin.ViewModels.Alerts
{
    public class AlertsViewModel
    {
        [DisplayName("Nova Mensagem")]
        [Required(ErrorMessage = "Mensagem obrigatório")]
        public string Message { get; set; }

        public DateTime CreatedAt { get; set; }
        
        public static AlertsViewModel FromModel(Alert alert)
        {
            var vm = new AlertsViewModel();

            if (alert != null)
            {
                vm.CreatedAt = alert.CreatedAt;
                vm.Message = alert.Message;
            }

            return vm;
        }
    }
}