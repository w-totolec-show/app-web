using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.DTOs;
using Core.DTOs.AppLogs;
using Core.Entities;
using Core.Entities.AppLogs;
using Core.Infrastructure;
using Core.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Web.Areas.Admin.ViewModels.Alerts;
using Web.Areas.Admin.ViewModels.Terms;
using X.PagedList;
using Queryable = Core.DTOs.Queryable;

namespace Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Administrators, Dashboards, ManageDraws")]
    public class CustomizationController : Controller
    {
        private readonly IFileManager _fileManager;
        private readonly IDrawsService _drawsService;
        private readonly IAlertsService _alertsService;
        private readonly IFileImportsService _fileImportsService;
        private readonly ITermsService _termsService;
        private readonly IMessagesService _messagesService;
        private readonly IAppLogsService _appLogsService;
        private readonly UserManager<AppUser> _userManager;
        private PaginatedResult<Log> resultLog { get; set; }
        
        public CustomizationController(IFileImportsService fileImportsService, 
                                        IFileManager fileManager, 
                                        IDrawsService drawsService,
                                        IAlertsService alertsService,
                                        ITermsService termsService,
                                        IMessagesService messagesService,
                                        IAppLogsService appLogsService,
                                        UserManager<AppUser> userManager)
        {
            _fileImportsService = fileImportsService;
            _drawsService = drawsService;
            _fileManager = fileManager;
            _alertsService = alertsService;
            _termsService = termsService;
            _messagesService = messagesService;
            _userManager = userManager;
            _appLogsService = appLogsService;
        }

        public async Task<IActionResult> Index(bool isSaved, Queryable pagination)
        {
            var res = await _fileImportsService.ListAsync(TypeFile.Banner);
            var alert = await _alertsService.ListAsync();
            var terms = await _termsService.GetAsync();
            var messagesEmail = await _messagesService.GetAsync(TypeMessage.Email);
            var imgOfEmail = await _fileImportsService.ListAsync(TypeFile.ImgOfEmail);
            var messagesCheckout = await _messagesService.GetAsync(TypeMessage.Checkout);
            var imgOfCheckout = await _fileImportsService.ListAsync(TypeFile.ImgOfCheckout);
            var imgNextDraw = await _fileImportsService.ListAsync(TypeFile.ImgNextDraw);
            var messageNextDraw = await _messagesService.GetAsync(TypeMessage.NextDraw);
            
            resultLog = await _appLogsService.ListAsync(new ListLogsRequest
            {
                Page = pagination.Page,
                PageSize = pagination.PageSize,
                userGuid = new Guid("29b0c45a-d80b-42e0-8249-ef946c8bd0c9")
            });
                
            ViewData["ResultsLogs"] = new StaticPagedList<Log>(
                resultLog.Results, 1, 10, resultLog.Count);
            
            ViewBag.ActiveTab = pagination.ReturnToView ?? "initalPag";

            ViewData["Terms"] = terms;
            ViewData["Alerts"] = alert;
            ViewData["ResultBanners"] = res;
            ViewData["MessageEmail"] = messagesEmail;
            ViewData["ImgOfEmail"] = imgOfEmail;
            ViewData["messageCheckout"] = messagesCheckout;
            ViewData["ImgOfCheckout"] = imgOfCheckout;
            ViewData["ImgNextDraw"] = imgNextDraw;
            ViewData["messageNextDraw"] = messageNextDraw;

            if (isSaved)
            {
                ViewBag.Message = "Alterações salvas com sucesso";
            }
            
            return View();
        }

        public async Task<IActionResult> Upload()
        {
            if (HttpContext.Request.Form.Files.Count == 0)
            {
                return BadRequest("File not found");
            }

            var listBanners = await _fileImportsService.ListAsync(TypeFile.Banner);

            if (listBanners.Count > 0)
            {
                var compare = new List<PropertyLog>();

                foreach (var item in listBanners)
                {
                    var propertyLog = new PropertyLog
                    {
                        LogLevel = 1,
                        Before = item.FileUri,
                        After = "---",
                        Property = "Banner",
                        CreatedAt = DateTime.UtcNow
                    };
                
                    compare.Add(propertyLog);
                }
                            
                var userLog = await _userManager.FindByNameAsync(User.Identity.Name);
                
                var logRequest = new SaveLogRequest
                {
                    LogType = LogType.Banner,
                    PropertyLogs = compare,
                    UserGuid = new Guid("29b0c45a-d80b-42e0-8249-ef946c8bd0c9"),
                    UserModification = userLog.Id
                };

                await _appLogsService.SaveAsync(logRequest);
            }
            
            //Delete all banners
            await _fileImportsService.DeleteAllTypeAsync(TypeFile.Banner);
            
            foreach (var file in HttpContext.Request.Form.Files)
            {
                if (file.Name == "banners")
                {
                    if (file.Length == 0)
                    {
                        return BadRequest("File not found");
                    }
                    
                    var extension = file.FileName.Split(".")[1];
                    var contentType = file.ContentType;
                    var fileUri = await _fileManager.SaveFileAsync(file.OpenReadStream(), extension, contentType, null);
                    
                    var fileImport = new FileImport
                    {
                        Guid = new Guid(),
                        TypeFile = TypeFile.Banner,
                        Status = Status.Imported,
                        FileName = file.FileName,
                        FileNameInternal = fileUri.Split("/")[1],
                        FileUri = fileUri,
                        CreatedAt = DateTime.UtcNow
                    };
                    
                    await _fileImportsService.SaveAsync(fileImport);
                }
            }
            
            return RedirectToAction("Index", new { isSaved = true });
        }

        public async Task<IActionResult> Alert(AlertsViewModel vm)
        {
            if (vm.Message != null)
            {
                var newAlert = new Alert
                {
                    Guid = new Guid(),
                    CreatedAt = DateTime.UtcNow,
                    Message = vm.Message,
                    IsActive = false
                };

                await _alertsService.SaveAsync(newAlert);
            }
            
            return RedirectToAction("Index",new { isSaved = true });
        }
        
        public async Task<IActionResult> ToggleAlert(Guid id)
        {
            await _alertsService.ToggleAlertAsync(id);
            return RedirectToAction("Index", "Customization");
        }

        public async Task<IActionResult> TermsOfService(AlertsViewModel vm)
        {
            var termsOfService = await _termsService.GetAsync();

            if (termsOfService != null)
            {
                var compare = new List<PropertyLog>();

                var propertyLog = new PropertyLog
                {
                    LogLevel = 1,
                    Before = termsOfService.Message,
                    After = vm.Message,
                    Property = "Termo de serviços",
                    CreatedAt = DateTime.UtcNow
                };
                
                compare.Add(propertyLog);
                            
                var userLog = await _userManager.FindByNameAsync(User.Identity.Name);
                
                var logRequest = new SaveLogRequest
                {
                    LogType = LogType.TermsOfService,
                    PropertyLogs = compare,
                    UserGuid = new Guid("29b0c45a-d80b-42e0-8249-ef946c8bd0c9"),
                    UserModification = userLog.Id
                };

                await _appLogsService.SaveAsync(logRequest);
            }
            
            if (vm.Message != null)
            {
                var terms = new TermsOfService
                {
                    Guid = new Guid(),
                    CreatedAt = DateTime.UtcNow,
                    Message = vm.Message
                };

                await _termsService.SaveAsync(terms);
            }
            
            return RedirectToAction("Index",new { isSaved = true });
        }

        public async Task<IActionResult> UploadImgEmail()
        {
            if (HttpContext.Request.Form.Files.Count == 0)
            {
                return BadRequest("File not found");
            }
            
            var listImgOfEmail = await _fileImportsService.ListAsync(TypeFile.ImgOfEmail);

            if (listImgOfEmail.Count > 0)
            {
                var compare = new List<PropertyLog>();

                foreach (var item in listImgOfEmail)
                {
                    var propertyLog = new PropertyLog
                    {
                        LogLevel = 1,
                        Before = item.FileUri,
                        After = "---",
                        Property = "Imagem do e-mail",
                        CreatedAt = DateTime.UtcNow
                    };
                
                    compare.Add(propertyLog);
                }
                            
                var userLog = await _userManager.FindByNameAsync(User.Identity.Name);
                
                var logRequest = new SaveLogRequest
                {
                    LogType = LogType.ImgOfEmail,
                    PropertyLogs = compare,
                    UserGuid = new Guid("29b0c45a-d80b-42e0-8249-ef946c8bd0c9"),
                    UserModification = userLog.Id
                };

                await _appLogsService.SaveAsync(logRequest);
            }
            
            await _fileImportsService.DeleteAllTypeAsync(TypeFile.ImgOfEmail);
            
            foreach (var file in HttpContext.Request.Form.Files)
            {
                if (file.Length == 0)
                {
                    return BadRequest("File not found");
                }
                    
                var extension = file.FileName.Split(".")[1];
                var contentType = file.ContentType;
                var fileUri = await _fileManager.SaveFileAsync(file.OpenReadStream(), extension, contentType, null);
                    
                var fileImport = new FileImport
                {
                    Guid = new Guid(),
                    TypeFile = TypeFile.ImgOfEmail,
                    Status = Status.Imported,
                    FileName = file.FileName,
                    FileNameInternal = fileUri.Split("/")[1],
                    FileUri = fileUri,
                    CreatedAt = DateTime.UtcNow
                };
                    
                await _fileImportsService.SaveAsync(fileImport);
            }
            
            return RedirectToAction("Index", new { isSaved = true });
        }
        
        public async Task<IActionResult> UploadImgCheckout()
        {
            if (HttpContext.Request.Form.Files.Count == 0)
            {
                return BadRequest("File not found");
            }
            
            var listImgOfCheckout = await _fileImportsService.ListAsync(TypeFile.ImgOfCheckout);

            if (listImgOfCheckout.Count > 0)
            {
                var compare = new List<PropertyLog>();

                foreach (var item in listImgOfCheckout)
                {
                    var propertyLog = new PropertyLog
                    {
                        LogLevel = 1,
                        Before = item.FileUri,
                        After = "---",
                        Property = "Imagem do checkout",
                        CreatedAt = DateTime.UtcNow
                    };
                
                    compare.Add(propertyLog);
                }
                            
                var userLog = await _userManager.FindByNameAsync(User.Identity.Name);
                
                var logRequest = new SaveLogRequest
                {
                    LogType = LogType.ImgOfCheckout,
                    PropertyLogs = compare,
                    UserGuid = new Guid("29b0c45a-d80b-42e0-8249-ef946c8bd0c9"),
                    UserModification = userLog.Id
                };

                await _appLogsService.SaveAsync(logRequest);
            }
            
            await _fileImportsService.DeleteAllTypeAsync(TypeFile.ImgOfCheckout);
            
            foreach (var file in HttpContext.Request.Form.Files)
            {
                if (file.Length == 0)
                {
                    return BadRequest("File not found");
                }
                    
                var extension = file.FileName.Split(".")[1];
                var contentType = file.ContentType;
                var fileUri = await _fileManager.SaveFileAsync(file.OpenReadStream(), extension, contentType, null);
                    
                var fileImport = new FileImport
                {
                    Guid = new Guid(),
                    TypeFile = TypeFile.ImgOfCheckout,
                    Status = Status.Imported,
                    FileName = file.FileName,
                    FileNameInternal = fileUri.Split("/")[1],
                    FileUri = fileUri,
                    CreatedAt = DateTime.UtcNow
                };
                    
                await _fileImportsService.SaveAsync(fileImport);
            }
            
            return RedirectToAction("Index", new { isSaved = true });
        }

        public async Task<IActionResult> UploadImgNextDraw()
        {
            if (HttpContext.Request.Form.Files.Count == 0)
            {
                return BadRequest("File not found");
            }
            
            var listImgNextDraw = await _fileImportsService.ListAsync(TypeFile.ImgNextDraw);

            if (listImgNextDraw.Count > 0)
            {
                var compare = new List<PropertyLog>();

                foreach (var item in listImgNextDraw)
                {
                    var propertyLog = new PropertyLog
                    {
                        LogLevel = 1,
                        Before = item.FileUri,
                        After = "---",
                        Property = "Imagem do preparando o próximo sorteio",
                        CreatedAt = DateTime.UtcNow
                    };
                
                    compare.Add(propertyLog);
                }
                            
                var userLog = await _userManager.FindByNameAsync(User.Identity.Name);
                
                var logRequest = new SaveLogRequest
                {
                    LogType = LogType.ImgNextDraw,
                    PropertyLogs = compare,
                    UserGuid = new Guid("29b0c45a-d80b-42e0-8249-ef946c8bd0c9"),
                    UserModification = userLog.Id
                };

                await _appLogsService.SaveAsync(logRequest);
            }
            
            await _fileImportsService.DeleteAllTypeAsync(TypeFile.ImgNextDraw);
            
            foreach (var file in HttpContext.Request.Form.Files)
            {
                if (file.Length == 0)
                {
                    return BadRequest("File not found");
                }
                    
                var extension = file.FileName.Split(".")[1];
                var contentType = file.ContentType;
                var fileUri = await _fileManager.SaveFileAsync(file.OpenReadStream(), extension, contentType, null);
                    
                var fileImport = new FileImport
                {
                    Guid = new Guid(),
                    TypeFile = TypeFile.ImgNextDraw,
                    Status = Status.Imported,
                    FileName = file.FileName,
                    FileNameInternal = fileUri.Split("/")[1],
                    FileUri = fileUri,
                    CreatedAt = DateTime.UtcNow
                };
                    
                await _fileImportsService.SaveAsync(fileImport);
            }
            
            return RedirectToAction("Index", new { isSaved = true });
        }
        
        public async Task<IActionResult> MessageNextDraw(AlertsViewModel vm)
        {
            var messageOfEmail = await _messagesService.GetAsync(TypeMessage.NextDraw);

            if (messageOfEmail != null)
            {
                var compare = new List<PropertyLog>();

                var propertyLog = new PropertyLog
                {
                    LogLevel = 1,
                    Before = messageOfEmail.Message,
                    After = vm.Message,
                    Property = "Mensagem do preparando próximo sorteio",
                    CreatedAt = DateTime.UtcNow
                };
                
                compare.Add(propertyLog);
                            
                var userLog = await _userManager.FindByNameAsync(User.Identity.Name);
                
                var logRequest = new SaveLogRequest
                {
                    LogType = LogType.MessageNextDraw,
                    PropertyLogs = compare,
                    UserGuid = new Guid("29b0c45a-d80b-42e0-8249-ef946c8bd0c9"),
                    UserModification = userLog.Id
                };

                await _appLogsService.SaveAsync(logRequest);
            }
            
            if (vm.Message != null)
            {
                var terms = new Messages
                {
                    Guid = new Guid(),
                    CreatedAt = DateTime.UtcNow,
                    Message = vm.Message,
                    TypeMessage = TypeMessage.NextDraw
                };

                await _messagesService.SaveAsync(terms);
            }
            
            return RedirectToAction("Index",new { isSaved = true });
        }
        
        public async Task<IActionResult> MessageOfEmail(AlertsViewModel vm)
        {
            var messageOfEmail = await _messagesService.GetAsync(TypeMessage.Email);

            if (messageOfEmail != null)
            {
                var compare = new List<PropertyLog>();

                var propertyLog = new PropertyLog
                {
                    LogLevel = 1,
                    Before = messageOfEmail.Message,
                    After = vm.Message,
                    Property = "Mensagem do e-mail",
                    CreatedAt = DateTime.UtcNow
                };
                
                compare.Add(propertyLog);
                            
                var userLog = await _userManager.FindByNameAsync(User.Identity.Name);
                
                var logRequest = new SaveLogRequest
                {
                    LogType = LogType.MessageOfEmail,
                    PropertyLogs = compare,
                    UserGuid = new Guid("29b0c45a-d80b-42e0-8249-ef946c8bd0c9"),
                    UserModification = userLog.Id
                };

                await _appLogsService.SaveAsync(logRequest);
            }
            
            if (vm.Message != null)
            {
                var terms = new Messages
                {
                    Guid = new Guid(),
                    CreatedAt = DateTime.UtcNow,
                    Message = vm.Message,
                    TypeMessage = TypeMessage.Email
                };

                await _messagesService.SaveAsync(terms);
            }
            
            return RedirectToAction("Index",new { isSaved = true });
        }
        
        public async Task<IActionResult> MessageOfCheckout(AlertsViewModel vm)
        {
            var messageOfCheckout = await _messagesService.GetAsync(TypeMessage.Checkout);

            if (messageOfCheckout != null)
            {
                var compare = new List<PropertyLog>();

                var propertyLog = new PropertyLog
                {
                    LogLevel = 1,
                    Before = messageOfCheckout.Message,
                    After = vm.Message,
                    Property = "Mensagem do checkout",
                    CreatedAt = DateTime.UtcNow
                };
                
                compare.Add(propertyLog);
                            
                var userLog = await _userManager.FindByNameAsync(User.Identity.Name);
                
                var logRequest = new SaveLogRequest
                {
                    LogType = LogType.MessageOfCheckout,
                    PropertyLogs = compare,
                    UserGuid = new Guid("29b0c45a-d80b-42e0-8249-ef946c8bd0c9"),
                    UserModification = userLog.Id
                };

                await _appLogsService.SaveAsync(logRequest);
            }
            
            if (vm.Message != null)
            {
                var terms = new Messages
                {
                    Guid = new Guid(),
                    CreatedAt = DateTime.UtcNow,
                    Message = vm.Message,
                    TypeMessage = TypeMessage.Checkout
                };

                await _messagesService.SaveAsync(terms);
            }
            
            return RedirectToAction("Index",new { isSaved = true });
        }
    }
}