using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.IO.Compression;
using System.Threading.Tasks;
using Core.DTOs.Draws;
using Core.Entities;
using Core.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using X.PagedList;
using System.Linq;
using System.Text;
using System.Timers;
using Core.DTOs;
using Core.DTOs.AppLogs;
using Core.DTOs.Tickets;
using Core.Entities.AppLogs;
using Core.Infrastructure;
using Core.Utils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Primitives;
using OfficeOpenXml.FormulaParsing.Excel.Functions.DateTime;
using ServiceStack;
using Web.Areas.Admin.ViewModels.Draws;
using Queryable = Core.DTOs.Queryable;
using Type = Core.Entities.Type;

namespace Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Administrators, Dashboards, ManageDraws")]
    public class DrawsController : Controller
    {
        private readonly IDrawsService _drawsService;
        private readonly ITicketsService _ticketsService;
        private readonly IFileManager _fileManager;
        private readonly IFileImportsService _fileImportsService;
        private readonly UserManager<AppUser> _userManager;
        private readonly IAppLogsService _appLogsService;
        private readonly IQueueItemService _queueItemService;
        private PaginatedResult<Ticket> resultTickets { get; set; }
        private PaginatedResult<Ticket> ticketsSold { get; set; }
        private PaginatedResult<Log> resultLog { get; set; }
        
        private readonly string _uriPath;
        
        public DrawsController(IDrawsService drawsService, 
            ITicketsService ticketsService, 
            IFileManager fileManager, 
            IFileImportsService fileImportsService, 
            UserManager<AppUser> userManager,
            IAppLogsService appLogsService,
            IConfiguration configuration,
            IQueueItemService queueItemService)
        {
            _drawsService = drawsService;
            _ticketsService = ticketsService;
            _fileManager = fileManager;
            _fileImportsService = fileImportsService;
            _userManager = userManager;
            _appLogsService = appLogsService;
            _uriPath = configuration["CdnPath"];
            _queueItemService = queueItemService;
        }

        public async Task<IActionResult> Index(Queryable vm)
        {
            var result = await _drawsService.ListAsync(new ListDrawsRequest
            {
                Page = vm.Page,
                PageSize = vm.PageSize,
                SearchTerm = vm.SearchTerm,
                OrderBy = vm.OrderBy,
                IsActive = vm.IsActive,
            });

            List<EditDrawViewModel> listDraw = new List<EditDrawViewModel>();
            foreach (var draw in result.Results)
            {
                var viewModel = EditDrawViewModel.FromModel(draw, 0, 0);
                listDraw.Add(viewModel);
            }
            
            ViewData["OrderBy"] = vm.OrderBy;
            ViewData["SearchTerm"] = vm.SearchTerm;
            ViewData["IsActive"] = vm.IsActive;
            ViewData["Count"] = result.Count;
            ViewData["Results"] = new StaticPagedList<EditDrawViewModel>(
                listDraw, vm.Page, vm.PageSize, result.Count);

            return View();
        }

        public async Task<IActionResult> Edit(Guid id, EditDrawViewModel vm)
        {
            switch (Request.Method)
            {
                case "GET":
                {
                    // Get existing user
                    int ticketsPending = 0;
                    var draw = await _drawsService.GetUniqueAsync(id);

                    if (draw != null)
                        ticketsPending = await _ticketsService.CountPending(draw.Guid);
                        
                    var viewModel = EditDrawViewModel.FromModel(draw, 0, ticketsPending);

                    ModelState.Clear();
                    return View(viewModel);
                }

                case "POST" when !ModelState.IsValid:
                    ViewBag.Error = "Error";
                    return View(vm);
            }

            if (vm.IsActive)
            {
                //Validações
                DateTime dateTimeUtc = DateTime.UtcNow;
                var brazilDatetime = dateTimeUtc.ToLocalString();
                DateTime dateTime = DateTime.Parse(brazilDatetime);
               
                if (vm.StartDatePurchase < dateTime)
                    ModelState.AddModelError(string.Empty, "Data de início da venda menor que a data atual.");
            
                if(vm.EndDatePurchase < dateTime)
                    ModelState.AddModelError(string.Empty, "Data fim da venda menor que a data atual.");
            
                if(vm.DrawDate < dateTime)
                    ModelState.AddModelError(string.Empty, "Data do sorteio menor que a data atual.");
            
                if (vm.StartDatePurchase > vm.EndDatePurchase)
                    ModelState.AddModelError(string.Empty, "Data de início da venda maior que a data de fim da venda.");
            
                if (vm.DrawDate < vm.EndDatePurchase)
                    ModelState.AddModelError(string.Empty, "Data do sorteio menor que a data de fim da venda.");
            
                if (vm.StartDatePurchase > vm.DrawDate)
                    ModelState.AddModelError(string.Empty, "Data de início da venda maior que a data do sorteio.");
                
                var activeDraw = await _drawsService.GetActiveAsync();
                if (activeDraw != null && activeDraw.Guid != vm.Guid)
                    ModelState.AddModelError(string.Empty, "Outro sorteio está ativo, apenas um sorteio pode está ativo.");
                
                if (vm.Pending == 0)
                    ModelState.AddModelError(string.Empty, "Nenhuma carga de cartelas incluída no sorteio, inclua as cartelas para ativá-lo.");
                
                if (vm.CountFileImports != 3)
                    ModelState.AddModelError(string.Empty, "Algum arquivo pendente no sorteio, verifique a carga de cartelas, imagem da cartela e regulamento.");
            }
            
            if (ModelState.ErrorCount > 0)
            {
                ViewBag.Error = "Error";
                return View(vm);
            }

            var userLog = await _userManager.FindByNameAsync(User.Identity.Name);
            
            var request = new SaveDrawRequest
            {
                Guid = vm.Guid,
                DrawNumber = vm.DrawNumber,
                Name = vm.Name,
                ValueOfTicket = vm.ValueOfTicket,
                StartDatePurchase = vm.StartDatePurchase,
                EndDatePurchase = vm.EndDatePurchase,
                Observations = vm.Observations,
                OriginalLayoutName = vm.OriginalLayoutName,
                LayoutUri = vm.LayoutUri,
                DrawDate = vm.DrawDate,
                Accomplished = vm.Accomplished,
                UrlCommercial = vm.UrlCommercial,
                DescriptionOfDraw = vm.DescriptionOfDraw,
                DescriptionOfResult = vm.DescriptionOfResult,
                IsActive = vm.IsActive,
                UserModification = userLog.Id,
                Parameters = vm.Parameters?
                    .Where(t => t.Key != null && t.Value != null)
                    .ToDictionary(t => t.Key, t => t.Value)
            };

            var res = await _drawsService.SaveAsync(request);

            return RedirectToAction("View", new {id = res.Guid, isSaved = true, alert = true});
        }

        public async Task<IActionResult> View(Guid id, Queryable pagination, bool isSaved = false, string message = null, bool alert = false)
        {
            if (isSaved) ViewBag.Message = message ?? "Sorteio salvo com sucesso";
            ViewBag.Alert = alert ? "Success" : "Error";

            if (pagination.ReturnToView != null && pagination.ReturnToView == "tickets")
            {
                resultTickets = await _ticketsService.ListAsync(new ListTicketsRequest
                {
                    Page = pagination.Page,
                    PageSize = pagination.PageSize,
                    SearchTerm = pagination.SearchTerm,
                    IsActive = pagination.IsActive,
                    DrawGuid = id
                });
                
                ViewData["ResultTickets"] = new StaticPagedList<Ticket>(
                    resultTickets.Results, pagination.Page, pagination.PageSize, resultTickets.Count);
            }
            else if (pagination.ReturnToView != null && pagination.ReturnToView == "historic")
            {
                resultLog = await _appLogsService.ListAsync(new ListLogsRequest
                {
                    Page = pagination.Page,
                    PageSize = pagination.PageSize,
                    userGuid = id
                });
                
                ViewData["ResultsLogs"] = new StaticPagedList<Log>(
                    resultLog.Results, pagination.Page, pagination.PageSize, resultLog.Count);    
            }
            
            ViewBag.ActiveTab = pagination.ReturnToView ?? "overview";

            if (resultLog == null)
            {
                resultLog = await _appLogsService.ListAsync(new ListLogsRequest
                {
                    Page = 1,
                    PageSize = 10,
                    userGuid = id
                });
            
                
                ViewData["ResultsLogs"] = new StaticPagedList<Log>(
                                    resultLog.Results, 1, 10, resultLog.Count);
            }

            if (resultTickets == null)
            {
                resultTickets = await _ticketsService.ListAsync(new ListTicketsRequest
                {
                    Page = 1,
                    PageSize = 10,
                    SearchTerm = pagination.SearchTerm,
                    IsActive = pagination.IsActive,
                    DrawGuid = id
                });
             
                ViewData["ResultTickets"] = new StaticPagedList<Ticket>(
                    resultTickets.Results, 1, 10, resultTickets.Count);
            }

            var draw = await _drawsService.GetUniqueAsync(id);

            var ticketsPending = await _ticketsService.CountPending(draw.Guid);

            if (pagination.ReturnToView != null && pagination.ReturnToView == "overview")
            {
                ticketsSold = await _ticketsService.ListSold(new ListTicketsRequest
                {
                    Page = pagination.Page,
                    PageSize = pagination.PageSize,
                    DrawGuid = id
                });
                
                ViewData["ResultTicketsSold"] = new StaticPagedList<Ticket>(
                    ticketsSold.Results, pagination.Page, pagination.PageSize, ticketsSold.Count);
            }
            else
            {
                ticketsSold = await _ticketsService.ListSold(new ListTicketsRequest
                {
                    Page = 1,
                    PageSize = 10,
                    DrawGuid = id
                });
                
                ViewData["ResultTicketsSold"] = new StaticPagedList<Ticket>(
                    ticketsSold.Results, 1, 10, ticketsSold.Count);
            }
            
            var vm = EditDrawViewModel.FromModel(draw, ticketsSold.Count, ticketsPending);

            var exportLot = await _queueItemService.ListAsync(Type.ExportLot, id);
            
            ViewData["ExportLot"] = exportLot;
            ViewData["SearchTerm"] = pagination.SearchTerm;
            ViewData["CountTickets"] = resultTickets.Count;
            
            return View(vm);
        }
        
        public async Task<IActionResult> Export()
        {
            var draws = await _drawsService.ListAllAsync();
            
            var obj = draws.Select(draw => new
                {
                    Id = draw.Guid,
                    ExtracaoConcurso = draw.DrawNumber,
                    Nome = draw.Name,
                    ValorCartela = $"{draw.ValueOfTicket:C}",
                    DataSorteio = draw.DrawDate,
                    InicioVenda = draw.StartDatePurchase,
                    FimVenda = draw.EndDatePurchase,
                    Ativo = draw.IsActive ? "Sim" : "Não"
                })
                .Cast<object>()
                .ToList();

            await using var memoryStream = new MemoryStream();
            obj.ExportXlsx("Draws", memoryStream);

            return File(
                memoryStream.ToArray(),
                "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                $"Draws-{DateTime.UtcNow:dd-MM-yyyy}.xlsx");
        }

        public async Task<IActionResult> ExportLot(Guid drawGuid, string distributor)
        {
            var userLog = await _userManager.FindByNameAsync(User.Identity.Name);
            var queueItem = new QueueItem
            {
                Guid = new Guid(),
                DrawGuid = drawGuid,
                Type = Type.ExportLot,
                StatusQueueItem = StatusQueueItem.AwaitingProcessing,
                Distributor = distributor,
                CreatedAt = DateTime.UtcNow,
                UserGuid = userLog.Id
            };

            var res = await _queueItemService.SaveAsync(queueItem);
            
            return res ? Json("Success") : Json("Error");
        }
        
        public async Task<IActionResult> ExportTicketsSold(Guid drawGuid)
        {
            var tickets = await _ticketsService.ListAllSold(drawGuid);
            
            var obj = tickets.Select(ticket => new
                {
                    Id = ticket.Guid,
                    PrimeiroNomeComprador = ticket.AppUser != null ? ticket.AppUser.FirstName : "",
                    SobrenomeComprador = ticket.AppUser != null ? ticket.AppUser.LastName : "",
                    EmailComprador = ticket.AppUser != null ? ticket.AppUser.Email : "",
                    CelularComprador = ticket.AppUser != null ? ticket.AppUser.PhoneNumber : "",
                    Bairro = ticket.AppUser != null ? ticket.AppUser.Locality : "",
                    Cidade = ticket.AppUser != null ? ticket.AppUser.City : "",
                    NumeroCartela = ticket.TicketNumber,
                    NumeroSimplificadoCartela = ticket.SimplifiedTicketNumber,
                    ExtracaoConcurso = ticket.ExtractionNumber,
                    DataCompra = ticket.PurchaseDate.Value.ToLocalString(),
                    MeioPagamento = ticket.GatewayPayment,
                    IdTransacao = ticket.IdTransaction
                })
                .Cast<object>()
                .ToList();

            await using var memoryStream = new MemoryStream();
            obj.ExportXlsx("TicketsSold", memoryStream);

            return File(
                memoryStream.ToArray(),
                "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                $"TicketsSold-{DateTime.UtcNow:dd-MM-yyyy}.xlsx");
        }

        [RequestFormLimits(ValueLengthLimit = int.MaxValue, MultipartBodyLengthLimit = int.MaxValue)] 
        [DisableRequestSizeLimit] 
        public async Task<IActionResult> Upload(Guid guid, int pending)
        {
            if (HttpContext.Request.Form.Files.Count == 0)
            {
                return BadRequest("File not found");
            }

            var statusMessage = string.Empty;
            var statusAlert = false;

            foreach (var file in HttpContext.Request.Form.Files)
            {
                switch (file.Name)
                {
                    case "fileTickets":
                        if (file.Length == 0)
                        {
                            return BadRequest("File not found");
                        }
                        
                        var saveFile = await UploadAndSaveFile(TypeFile.Ticket, Status.AwaitingProcessing, file, guid);
                        if (saveFile)
                        {
                            statusMessage = "Carga de cartelas incluída com sucesso";
                            statusAlert = true;
                        }
                        else
                        {
                            statusMessage = "Falha na importação";
                        }
                        break;
                    case "fileImgTicket":
                        if (file.Length == 0)
                        {
                            return BadRequest("File not found");
                        }

                        var userLogT = await _userManager.FindByNameAsync(User.Identity.Name);
                        var drawd = await _drawsService.GetUniqueAsync(guid);
                        var imgTicket = drawd.FileImports.FirstOrDefault(f => f.TypeFile == TypeFile.ImgOfTicket);
                        if (imgTicket != null)
                        {
                            var compare = new List<PropertyLog>();
                            
                            var propertyLog = new PropertyLog
                            {
                                LogLevel = 1,
                                Before = imgTicket.FileUri,
                                After = "---",
                                Property = "Imagem da cartela",
                                CreatedAt = DateTime.UtcNow
                            };
                
                            compare.Add(propertyLog);
                            
                            var logRequest = new SaveLogRequest
                            {
                                LogType = LogType.TicketsSold,
                                UserGuid = guid,
                                PropertyLogs = compare,
                                UserModification = userLogT.Id
                            };

                            await _appLogsService.SaveAsync(logRequest);
                        }
                        
                        var saveFileImg = await UploadAndSaveFile(TypeFile.ImgOfTicket, Status.Imported, file, guid);
                        if (saveFileImg)
                        {
                            statusMessage = "Importação realizada com sucesso";
                            statusAlert = true;
                        }
                        else
                        {
                            statusMessage = "Falha na importação";
                        }
                        break;
                    case "fileRegulations":
                        if (file.Length == 0)
                        {
                            return BadRequest("File not found");
                        }
                        
                        var userLogR = await _userManager.FindByNameAsync(User.Identity.Name);
                        var drawR = await _drawsService.GetUniqueAsync(guid);
                        var imgRegulations = drawR.FileImports.FirstOrDefault(f => f.TypeFile == TypeFile.ImgOfTicket);
                        if (imgRegulations != null)
                        {
                            var compare = new List<PropertyLog>();
                            
                            var propertyLog = new PropertyLog
                            {
                                LogLevel = 1,
                                Before = imgRegulations.FileUri,
                                After = "---",
                                Property = "Regulamento",
                                CreatedAt = DateTime.UtcNow
                            };
                
                            compare.Add(propertyLog);
                            
                            var logRequest = new SaveLogRequest
                            {
                                LogType = LogType.TicketsSold,
                                UserGuid = guid,
                                PropertyLogs = compare,
                                UserModification = userLogR.Id
                            };

                            await _appLogsService.SaveAsync(logRequest);
                        }
                        
                        var saveFileRegulations = await UploadAndSaveFile(TypeFile.Regulations, Status.Imported, file, guid);
                        if (saveFileRegulations)
                        {
                            statusMessage = "Importação realizada com sucesso";
                            statusAlert = true;
                        }
                        else
                        {
                            statusMessage = "Falha na importação";
                        }
                        break;
                    default:
                        return BadRequest("File not found");
                }
            }

            return RedirectToAction(
                "View",
                new
                {
                    id = guid,
                    isSaved = true,
                    message = statusMessage,
                    alert = statusAlert
                });
        }

        public async Task<IActionResult> ToggleAccomplished(Guid id)
        {
            await _drawsService.ToggleAccomplishedAsync(id);
            return RedirectToAction("Index", "Draws");
        }

        /*public async Task<IActionResult> TestDraw(Guid id)
        {
            var listTicketsRequest = new ListTicketsRequest
            {
                DrawGuid = id,
                PageSize = 20000
            };

            var listTickets = await _ticketsService.ListSuggestionsAsync(listTicketsRequest);

            await _ticketsService.AssociateTicketsForUserTest((List<Ticket>) listTickets.Results, new Guid("45449083-b9c6-42cb-8cc9-8be60f198f26"), 7273887);
            
            return RedirectToAction("Index", "Draws");
        }*/

        private async Task<bool> ImportTickets(Stream file, Guid drawGuid, string fileName, int pending)
        {
            try
            {
                if (pending > 0) await _ticketsService.DeleteAllAsync(drawGuid);

                var saveTicketRequests = new List<SaveTicketRequest>();

                string line;
                using var reader = new StreamReader(file, Encoding.UTF8);
                while ((line = await reader.ReadLineAsync()) != null)
                {
                    var ticketRequest = new SaveTicketRequest(drawGuid, fileName, line);
                    saveTicketRequests.Add(ticketRequest);
                }

                return await _ticketsService.SaveAsync(saveTicketRequests);
            }
            catch (Exception)
            {
                return false;
            }
        }

        private async Task<bool> UploadAndSaveFile(TypeFile typeFile, Status status, IFormFile file, Guid drawGuid)
        {
            //Delete all file of type ticket
            await _fileImportsService.DeleteAllTypeAndDrawAsync(typeFile, drawGuid);
            
            var extension = file.FileName.Split(".")[1];
            var contentType = file.ContentType;
            var fileUri = await _fileManager.SaveFileAsync(file.OpenReadStream(), extension, contentType, null);
            var userLog = await _userManager.FindByNameAsync(User.Identity.Name);
            
            var fileImport = new FileImport
            {
                Guid = new Guid(),
                DrawGuid = drawGuid,
                TypeFile = typeFile,
                Status = status,
                FileName = file.FileName,
                FileNameInternal = fileUri.Split("/")[1],
                FileUri = fileUri,
                CreatedAt = DateTime.UtcNow,
                UserGuid = userLog.Id
            };

            return await _fileImportsService.SaveAsync(fileImport);
        }
    }
}