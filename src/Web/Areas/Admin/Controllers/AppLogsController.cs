﻿using Core.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using System.Threading.Tasks;
using Core.Entities.AppLogs;
using Microsoft.AspNetCore.Authorization;
using X.PagedList;
using System;
using System.Collections.Generic;
using System.IO;
using Core.DTOs;
using Core.Utils;
using Core.DTOs.AppLogs;
using Core.Entities;
using Web.Areas.Admin.ViewModels.AppLogs;

namespace Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Administrators")]
    public class AppLogsController : Controller
    {
        private readonly IAppLogsService _appLogsService;
        private readonly IUsersService _usersService;

        public AppLogsController(IAppLogsService appLogsService, IUsersService usersService)
        {
            _appLogsService = appLogsService;
            _usersService = usersService;
        }

        public async Task<IActionResult> Index(Queryable vm)
        {
            var userId = User.FindFirst(ClaimTypes.NameIdentifier).Value;

            if (!User.IsInRole("Administrators"))
                RedirectToAction("Index", "Home");

            var result = await _appLogsService.ListAsync(new ListLogsRequest
            {
                Page = vm.Page,
                PageSize = vm.PageSize,
                SearchTerm = vm.SearchTerm,
                OrderBy = vm.OrderBy,
                IsActive = vm.IsActive,
            });
            
            /*var result = await _appLogsService.ListAsync(new ListLogsRequest
            {
                Page = vm.Page,
                PageSize = vm.PageSize,
                SearchTerm = vm.SearchTerm,
                OrderBy = vm.OrderBy,
                IsActive = vm.IsActive,
            });

            ViewData["OrderBy"] = vm.OrderBy;
            ViewData["SearchTerm"] = vm.SearchTerm;
            ViewData["IsActive"] = vm.IsActive;

            ViewData["Count"] = result.Count;*/
            //ViewData["Results"] = new StaticPagedList<Log>(result.Results, vm.Page, vm.PageSize, result.Count);

            return View();
        }

        public async Task<IActionResult> View(Guid id)
        {
            var log = await _appLogsService.GetAsync(id);

            var user = log.UserGuid.HasValue
                ? await _usersService.GetAsync(log.UserGuid.Value)
                : ((AppUser, IList<string>)?) null;

            var viewModel = AppLogViewModel.FromModel(log, null);

            return View(viewModel);
        }

        public async Task<IActionResult> Export(Queryable vm)
        {
            /*var result = await _appLogsService.ListAsync(new ListLogsRequest
            {
                Page = vm.Page,
                PageSize = vm.PageSize,
                SearchTerm = vm.SearchTerm,
                OrderBy = vm.OrderBy,
                IsActive = vm.IsActive,
            });*/

            await using var memoryStream = new MemoryStream();
            //result.Results.ExportXlsx("Logs", memoryStream);

            return File(
                memoryStream.ToArray(),
                "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                $"logs-{DateTime.UtcNow:dd-MM-yyyy}.xlsx");
        }
    }
}