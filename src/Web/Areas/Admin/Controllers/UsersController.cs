﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Core.DTOs;
using Core.DTOs.AppLogs;
using Core.DTOs.Tickets;
using Core.DTOs.Users;
using Core.Entities;
using Core.Entities.AppLogs;
using Core.Services.Interfaces;
using Core.Utils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Web.Areas.Admin.ViewModels.Users;
using X.PagedList;
using Queryable = Core.DTOs.Queryable;

namespace Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Administrators, ManageDraws")]
    public class UsersController : Controller
    {
        private readonly IUsersService _usersService;
        private PaginatedResult<AppUser> ResultClients { get; set; }
        private PaginatedResult<AppUser> ResultAdmins { get; set; }

        private readonly UserManager<AppUser> _userManager;
        
        private readonly IAppLogsService _appLogsService;

        private readonly ITicketsService _ticketsService;
        
        public UsersController(IUsersService usersService, 
                                UserManager<AppUser> userManager, 
                                IAppLogsService appLogsService,
                                ITicketsService ticketsService)
        {
            _usersService = usersService;
            _userManager = userManager;
            _appLogsService = appLogsService;
            _ticketsService = ticketsService;
        }

        public async Task<IActionResult> Index(Queryable vm)
        {
            if (vm.ReturnToView != null && vm.ReturnToView == "clients")
            {
                ResultClients = await _usersService.ListAsync(new ListUsersRequest
                {
                    Page = vm.Page,
                    PageSize = vm.PageSize,
                    SearchTerm = vm.SearchTerm,
                    OrderBy = vm.OrderBy,
                    IsActive = vm.IsActive,
                    IsAdmin = false
                });
                
                ViewData["ResultClients"] = new StaticPagedList<AppUser>(
                    ResultClients.Results, vm.Page, vm.PageSize, ResultClients.Count);
            }

            if (vm.ReturnToView != null && vm.ReturnToView == "administrators")
            {
                ResultAdmins = await _usersService.ListAsync(new ListUsersRequest
                {
                    Page = vm.Page,
                    PageSize = vm.PageSize,
                    SearchTerm = vm.SearchTerm,
                    OrderBy = vm.OrderBy,
                    IsActive = vm.IsActive,
                    IsAdmin = true
                });
                
                ViewData["Results"] = new StaticPagedList<AppUser>(
                    ResultAdmins.Results, vm.Page, vm.PageSize, ResultAdmins.Count);
            }
            
            if (ResultClients == null || ResultClients.Count == 0)
            {
                ResultClients = await _usersService.ListAsync(new ListUsersRequest
                {
                    Page = 1,
                    PageSize = 10,
                    SearchTerm = vm.SearchTerm,
                    OrderBy = vm.OrderBy,
                    IsActive = vm.IsActive,
                    IsAdmin = false
                });
                
                ViewData["ResultClients"] = new StaticPagedList<AppUser>(
                    ResultClients.Results, 1, 10, ResultClients.Count);
            }
            
            if (ResultAdmins == null ||ResultAdmins.Count == 0)
            {
                ResultAdmins = await _usersService.ListAsync(new ListUsersRequest
                {
                    Page = 1,
                    PageSize = 10,
                    SearchTerm = vm.SearchTerm,
                    OrderBy = vm.OrderBy,
                    IsActive = vm.IsActive,
                    IsAdmin = true
                });
                
                ViewData["Results"] = new StaticPagedList<AppUser>(
                    ResultAdmins.Results, 1, 10, ResultAdmins.Count);
            }

            ViewBag.ActiveTab = vm.ReturnToView;
            ViewData["OrderBy"] = vm.OrderBy;
            ViewData["SearchTerm"] = vm.SearchTerm;
            ViewData["IsActive"] = vm.IsActive;
            ViewData["Count"] = ResultAdmins.Count;
            ViewData["CountClients"] = ResultClients.Count;

            return View();
        }

        public async Task<IActionResult> Export(string isClient)
        {
            IList<AppUser> appUsers;
            if (isClient != null && isClient.Equals("client"))
            {
                appUsers = await _usersService.ListAllAsync(false);
            }
            else
            {
                appUsers = await _usersService.ListAllAsync(true);
            }
            
            var obj = appUsers.Select(user => new
            {
                user.Id,
                PrimeiroNome = user.FirstName,
                Sobrenome = user.LastName,
                user.Email,
                Celular = user.PhoneNumber,
                CPF = user.SocialId,
                CEP = user.Zipcode,
                DataNascimento = user.DateOfBirth,
                CadastroCompleto = user.CompleteRegistration ? "Sim" : "Não",
                Administrador = user.IsAdmin ? "Sim" : "Não",
                Modificacao = user.ModifiedAt,
                Registro = user.CreatedAt,
                Ativo = user.IsActive ? "Sim" : "Não"
            })
            .Cast<object>()
            .ToList();

            await using var memoryStream = new MemoryStream();
            obj.ExportXlsx("Users", memoryStream);

            return File(
                memoryStream.ToArray(),
                "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                $"Users-{DateTime.UtcNow:dd-MM-yyyy}.xlsx");
        }
        
        public async Task<IActionResult> ExportHistoric(Guid guid)
        {
            var resultLog = await _appLogsService.GetUserAsync(guid);

            var objProp = new List<object>();
            foreach (var log in resultLog)
            {
                foreach (var propertyLog in log.PropertyLogs)
                {
                    var prop = new
                    {
                        UsuarioAlteracao = $"{log.AppUser.FirstName} {log.AppUser.LastName}",
                        EmailUsuarioAlteracao = log.AppUser.Email,
                        CampoAlterado = propertyLog.Property,
                        Antes = propertyLog.After,
                        Depois = propertyLog.Before,
                        DataDeModificacao = log.CreatedAt
                    };
                    
                    objProp.Add(prop);
                }
            }
            
            await using var memoryStream = new MemoryStream();
            objProp.ExportXlsx("UserHistoric", memoryStream);

            return File(
                memoryStream.ToArray(),
                "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                $"Users-{DateTime.UtcNow:dd-MM-yyyy}.xlsx");
        }

        public async Task<IActionResult> Edit(Guid id, EditUserViewModel vm)
        {
            // Roles
            ViewBag.Roles = new MultiSelectList(new[]
            {
                new SelectListItem
                {
                    Text = "Master",
                    Value = "Administrators"
                },
                new SelectListItem
                {
                    Text = "Operacional",
                    Value = "Dashboards"
                },
                new SelectListItem
                {
                    Text = "Gerente",
                    Value = "ManageDraws"
                }
            }, "Value", "Text");

            switch (Request.Method)
            {
                case "GET":
                {
                    // Get existing user
                    var (user, roles) = await _usersService.GetAsync(id);
                    var viewModel = EditUserViewModel.FromModel(user, roles);

                    ModelState.Clear();
                    return View(viewModel);
                }

                case "POST" when !ModelState.IsValid:
                    ViewBag.Error = "Error";
                    return View(vm);
            }
            
            var userLog = await _userManager.FindByNameAsync(User.Identity.Name);
            
            var appUser = await  _userManager.FindByEmailAsync(vm.Email);

            if (appUser != null && vm.Guid == null)
            {
                ViewBag.DuplicateEmail = "Error";
                return View(vm);
            }

            var request = new SaveUserRequest
            {
                Id = vm.Guid,
                UserName = vm.Email,
                Password = vm.Password,
                FirstName = vm.FirstName,
                Lastname = vm.Lastname,
                Email = vm.Email,
                Observations = vm.Observations,
                Roles = vm.Roles,
                IsAdmin = true,
                IsActive = true,
                UserModification = userLog.Id,
                Data = vm.Data?
                    .Where(t => t?.Key != null && t.Value != null)
                    .ToDictionary(t => t.Key, t => t.Value)
            };

            var res = await _usersService.SaveAsync(request);

            return RedirectToAction("View", new {id = res.Id, isSaved = true});
        }

        public async Task<IActionResult> EditClient(Guid id, EditClientViewModel vm)
        {
            switch (Request.Method)
            {
                case "GET":
                {
                    var (user, roles) = await _usersService.GetAsync(id);
                    var viewModel = EditClientViewModel.FromModel(user, roles);

                    ModelState.Clear();
                    return View(viewModel);
                }

                case "POST" when !ModelState.IsValid:
                    ViewBag.Error = "Error";
                    return View(vm);
            }
            
            var userLog = await _userManager.FindByNameAsync(User.Identity.Name);
            
            var request = new SaveUserRequest
            {
                Id = vm.Guid,
                UserName = vm.Email,
                FirstName = vm.FirstName,
                Lastname = vm.Lastname,
                Email = vm.Email,
                Observations = vm.Observations,
                SocialId = vm.SocialId,
                DateOfBirth = vm.DateOfBirth,
                PhoneNumber = vm.PhoneNumber,
                Zipcode = vm.Zipcode,
                CompleteRegistration = vm.CompleteRegistration,
                Roles = new[] {"Client"},
                IsAdmin = false,
                IsActive = true,
                UserModification = userLog.Id,
                Data = vm.Data?
                    .Where(t => t?.Key != null && t.Value != null)
                    .ToDictionary(t => t.Key, t => t.Value)
            };

            var res = await _usersService.SaveAsync(request);
            
            return RedirectToAction("ViewClient", new {id = res.Id, isSaved = true});
        }

        public async Task<IActionResult> ViewClient(Queryable logVm, Guid id, bool isSaved = false)
        {
            if (isSaved) ViewBag.Message = "Cliente salvo com sucesso";
            var (user, userRoles) = await _usersService.GetAsync(id);
            var vm = EditClientViewModel.FromModel(user, userRoles);

            var tickets = await _ticketsService.ListCustomerAsync(new ListTicketsRequest
            {
                Page = logVm.Page,
                PageSize = logVm.PageSize,
                SearchTerm = logVm.SearchTerm,
                UserGuid = user.Id
            });
            
            var resultLog = await _appLogsService.ListAsync(new ListLogsRequest
            {
                Page = logVm.Page,
                PageSize = logVm.PageSize,
                userGuid = user.Id
            });

            ViewBag.ActiveTab = logVm.ReturnToView ?? "info";

            ViewData["GuidUser"] = id;
            ViewData["SearchTerm"] = logVm.SearchTerm;
            ViewData["ResultsLogs"] = new StaticPagedList<Log>(
                resultLog.Results, logVm.Page, logVm.PageSize, resultLog.Count);
            
            ViewData["ResultsTickets"] = new StaticPagedList<Ticket>(
                tickets.Results, logVm.Page, logVm.PageSize, tickets.Count);

            return View(vm);
        }

        public async Task<IActionResult> View(Queryable logVm, Guid id, bool isSaved = false)
        {
            if (isSaved) ViewBag.Message = "Usuário salvo com sucesso";

            ViewBag.Roles = new MultiSelectList(new[]
            {
                new SelectListItem
                {
                    Text = "Master",
                    Value = "Administrators"
                },
                new SelectListItem
                {
                    Text = "Operacional",
                    Value = "Dashboards"
                },
                new SelectListItem
                {
                    Text = "Gerente",
                    Value = "ManageDraws"
                }
            }, "Value", "Text");

            var (user, userRoles) = await _usersService.GetAsync(id);
            var vm = UserViewModel.FromModel(user, userRoles);
            
            ViewBag.ActiveTab = logVm.ReturnToView ?? "info";
            
            var resultLog = await _appLogsService.ListAsync(new ListLogsRequest
            {
                Page = logVm.Page,
                PageSize = logVm.PageSize,
                userGuid = user.Id
            });
            
            ViewData["GuidUser"] = id;
            ViewData["SearchTerm"] = logVm.SearchTerm;
            ViewData["ResultsLogs"] = new StaticPagedList<Log>(
                resultLog.Results, logVm.Page, logVm.PageSize, resultLog.Count);

            return View(vm);
        }

        public async Task<IActionResult> ToggleActive(Guid id)
        {
            await _usersService.DisableAsync(id);
            return RedirectToAction("Index", "Users");
        }
        
        public async Task<IActionResult> ToggleDeleteAsync(Guid id)
        {
            await _usersService.DeleteAsync(id);
            return RedirectToAction("Index", "Users");
        }
    }
}