using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using Core.Entities;
using Core.Services;
using Core.Services.Interfaces;
using Core.Utils;
using iText.Kernel.Pdf;
using iText.Signatures;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using ServiceStack;
using Web.Areas.Admin.ViewModels.Tickets;
using X509Certificate = Org.BouncyCastle.X509.X509Certificate;

namespace Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Administrators, Dashboards, ManageDraws")]
    public class TicketsController : Controller
    {
        private readonly ITicketsService _ticketsService;
        private readonly IPaymentService _paymentService;

        public TicketsController(ITicketsService ticketsService, IConfiguration configuration, IPaymentService paymentService)
        {
            _ticketsService = ticketsService;
            _paymentService = paymentService;
        }

        public async Task<IActionResult> View(Guid id, string message)
        {
            var ticket = await _ticketsService.GetAsync(id);
            var vm = TicketViewModel.FromModel(ticket);
            
            if(!string.IsNullOrEmpty(message))
                ViewBag.Message = message;
            
            return View(vm);
        }

        public async Task<IActionResult> ExportPdf(Guid id)
        {
            var ticket = await _ticketsService.GetAsync(id);
            
            return ExportUtilities.ExportPdf(ticket);
        }
        
        public async Task<IActionResult> ValidatePayment(Guid id)
        {
            try
            {
                var ticket = await _ticketsService.GetAsync(id);
                
                var token = await _paymentService.GetTokenAuthorizationAsync();

                var paymentStatus = await _paymentService.GetPaymentAsync(token, ticket.IdTransaction);

                var message = "";
                if (paymentStatus == "2")
                {
                    message = "Autorizado";
                }
                else
                {
                    message = "Sem informação / Não confirmado";
                }
            
                return RedirectToAction(
                    "View",
                    new
                    {
                        id,
                        message
                    });
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            
            return RedirectToAction(
                "View",
                new
                {
                    id
                });
        }
        
        public async Task<IActionResult> ToggleActive(Guid id)
        {
            var ticket = await _ticketsService.GetAsync(id);
            
            await _ticketsService.DisableAsync(id, !ticket.IsActive);
            
            return RedirectToAction(
                "View",
                new
                {
                    id
                });
        }
        
        public async Task<IActionResult> ReturnForSale(Guid id)
        {
            await _ticketsService.ReturnForSaleAsync(id);
            
            return RedirectToAction(
                "View",
                new
                {
                    id
                });
        }
    }
}