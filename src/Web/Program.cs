using System;
using Core.Infrastructure;
using Core.Utils;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Web
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var host = CreateHostBuilder(args).Build();

            using (var scope = host.Services.CreateScope())
            {
                var services = scope.ServiceProvider;
                DatabaseSeeder.Seed(services).Wait();
            }

            host.Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args)
        {
            return Host.CreateDefaultBuilder(args)
                .ConfigureAppConfiguration((hostingEnvironment, config) =>
                {
                    var environmentName = hostingEnvironment.HostingEnvironment.EnvironmentName;

                    // This line was causing docker containers to ignore environment variables defined by microsoft docker tools.
                    //config.Sources.Clear();
                    config.AddProjectConfigurations(environmentName);
                })
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>(); 
                    webBuilder.UseKestrel(o => { o.Limits.KeepAliveTimeout = TimeSpan.FromMinutes(120); }); 
                });
        }
    }
}