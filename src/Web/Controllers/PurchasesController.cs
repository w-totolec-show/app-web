using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Encodings.Web;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Core.DTOs.Tickets;
using Core.DTOs.Users;
using Core.Entities;
using Core.Services.Interfaces;
using Core.Utils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json.Linq;
using RestSharp;
using ServiceStack;
using Web.ViewModels.Purchases;

namespace Web.Controllers
{
    [Authorize]
    public class PurchasesController : Controller
    {
        private readonly ITicketsService _ticketsService;
        private readonly UserManager<AppUser> _userManager;
        private readonly IUsersService _usersService;
        private readonly IDrawsService _drawsService;
        private readonly ICreditCardTokenizeService _creditCardTokenizeService;
        private readonly IMessagesService _messagesService;
        private readonly IFileImportsService _fileImportsService;
        private readonly IEmailSender _emailSender;

        public PurchasesController(ITicketsService ticketsService, 
            UserManager<AppUser> userManager,
            IUsersService usersService,
            IDrawsService drawsService,
            ICreditCardTokenizeService creditCardTokenizeService,
            IMessagesService messagesService,
            IFileImportsService fileImportsService,
            IEmailSender emailSender,
            IConfiguration configuration)
        {
            _ticketsService = ticketsService;
            _userManager = userManager;
            _usersService = usersService;
            _drawsService = drawsService;
            _creditCardTokenizeService = creditCardTokenizeService;
            _messagesService = messagesService;
            _fileImportsService = fileImportsService;
            _emailSender = emailSender;
        }

        public async Task<IActionResult> Index(EditClientViewModel vm)
        {
            var user = await _userManager.FindByNameAsync(User.Identity.Name);
            if (!user.CompleteRegistration)
            {
                var ticketsPendingPurchase = HttpContext.Request.Cookies["Tickets"];
                if (ticketsPendingPurchase != null)
                {
                    switch (Request.Method)
                    {
                        case "GET":
                        {
                            ModelState.Clear();
                            return View();
                        }

                        case "POST" when !ModelState.IsValid:
                            ViewBag.Error = "Error";
                            return View(vm);
                    }

                    if (!ValidateInfo.ValidateCpf(vm.SocialId))
                    {
                        ViewBag.ErrorCpf = "Error";
                        return View(vm);
                    }
                    
                    var client = new RestClient("https://viacep.com.br");
                    var requestApi = new RestRequest($"ws/{vm.Zipcode}/json/", Method.GET);
                    var queryResult = client.Execute<IRestResponse>(requestApi).Content;

                    dynamic data = null;
                    dynamic dataCepLa = null;
                    dynamic dataApiCep = null;
                    if (!string.IsNullOrEmpty(queryResult))
                        data = JObject.Parse(queryResult);
                    else
                    {
                        var clientCepLa = new RestClient("http://cep.la");
                        var requestCepLa = new RestRequest($"/{vm.Zipcode}", Method.GET);
                        requestCepLa.AddHeader("Accept", "application/json");
                        var queryResultCepLa = clientCepLa.Execute<IRestResponse>(requestCepLa).Content;
                        
                        if (!string.IsNullOrEmpty(queryResultCepLa))
                            dataCepLa = JObject.Parse(queryResultCepLa);
                        else
                        {
                            var clientApiCep = new RestClient("https://ws.apicep.com");
                            var requestApiCep = new RestRequest($"/cep.json?code={vm.Zipcode}", Method.GET);
                            var queryResultApiCep = clientApiCep.Execute<IRestResponse>(requestApiCep).Content;
                    
                            if (!string.IsNullOrEmpty(queryResultApiCep))
                                dataApiCep = JObject.Parse(queryResultApiCep);
                        }
                    }

                    string city;
                    string bairro;
                    if (data != null && !string.IsNullOrEmpty(data.ToString()))
                    {
                        city = data.localidade.ToString();
                        bairro = data.bairro.ToString();
                    }
                    else if (dataCepLa != null && !string.IsNullOrEmpty(dataCepLa.ToString()))
                    {
                        city = dataCepLa.cidade.ToString();
                        bairro = dataCepLa.bairro.ToString();
                    } else if (dataApiCep != null && !string.IsNullOrEmpty(dataApiCep.ToString()))
                    {
                        city = dataApiCep.city.ToString();
                        bairro = dataApiCep.district.ToString();
                    }
                    else
                    {
                        city = "************";
                        bairro = "************";
                    }
                    
                    var request = new SaveUserRequest
                    {
                        Id = user.Id,
                        UserName = user.UserName,
                        Email = user.Email,
                        FirstName = vm.Name.Substring(0, vm.Name.IndexOf(" ", StringComparison.Ordinal)),
                        Lastname = vm.Name.Substring(vm.Name.IndexOf(" ", StringComparison.Ordinal) + 1),
                        PhoneNumber = vm.Phone,
                        Zipcode = vm.Zipcode,
                        SocialId = vm.SocialId,
                        DateOfBirth = new DateTime(vm.Year.ToInt(), vm.Month.ToInt(), vm.Day.ToInt()),
                        CompleteRegistration = true,
                        IsAdmin = false,
                        IsActive = true,
                        AddressNumber = vm.AddressNumber,
                        Roles = new string[] { "Client" },
                        UserModification = user.Id,
                        City = city,
                        Locality = bairro
                    };

                    await _usersService.SaveAsync(request);
                }
                else
                {
                    return RedirectToAction("Cart", "Purchases");
                }
            }

            return RedirectToAction("Payment");
        }

        public async Task<IActionResult> Cart(Guid? ticketGuid)
        {
            var ticketsPendingPurchase = HttpContext.Request.Cookies["Tickets"];
            
            if (ticketGuid.HasValue)
            {
                if (ticketsPendingPurchase != null && !ticketsPendingPurchase.Contains(ticketGuid.ToString()))
                    ticketsPendingPurchase = $"{ticketsPendingPurchase}&{ticketGuid.ToString()}";
                else if (ticketsPendingPurchase == null)
                    ticketsPendingPurchase = ticketGuid.ToString();
                
                SetCookieTicket("Tickets", ticketsPendingPurchase, 30);
            }

            if (ticketsPendingPurchase != null)
            {
                string[] ticketsGuid = ticketsPendingPurchase.Split("&")
                    .Where(t => !string.IsNullOrEmpty(t)).ToArray();

                List<Ticket> tickets = new List<Ticket>();
                
                foreach (var guid in ticketsGuid)
                {
                    tickets.Add(await _ticketsService.GetAsync(new Guid(guid)));
                }

                if (tickets.Any())
                {
                    decimal totalValue = 0;
                    foreach (var valueTicket in tickets.Select(t => t.Draw.ValueOfTicket))
                    {
                        totalValue += valueTicket;
                    }

                    ViewData["DrawNumber"] = tickets.Select(t => t.Draw.DrawNumber).FirstOrDefault();
                    ViewData["TotalValue"] = totalValue;
                    ViewData["Results"] = tickets;
                }
            }
            
            return View();
        }
        
        public async Task<IActionResult> Payment(PaymentViewModel vm)
        {
            var ticketsPendingPurchase = HttpContext.Request.Cookies["Tickets"];
            
            var user = await _userManager.FindByNameAsync(User.Identity.Name);
            ViewBag.CreditCardTokens = await _creditCardTokenizeService.ListAsync(user.Id);
            
            switch (Request.Method)
            {
                case "GET":
                {
                    if (ticketsPendingPurchase != null)
                    {
                        string[] ticketsGuid = ticketsPendingPurchase.Split("&")
                            .Where(t => !string.IsNullOrEmpty(t)).ToArray();

                        List<Ticket> tickets = new List<Ticket>();
                
                        foreach (var guid in ticketsGuid)
                        {
                            try
                            {
                                var ticket = await _ticketsService.GetAsync(new Guid(guid));
                                if (ticket != null)
                                    tickets.Add(ticket);
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine(e);
                                
                                ModelState.Clear();
                                return View(vm);
                            }
                        }

                        if (tickets.Any())
                        {
                            decimal totalValue = 0;
                            foreach (var valueTicket in tickets.Select(t => t.Draw.ValueOfTicket))
                            {
                                totalValue += valueTicket;
                            }
                        
                            ViewData["TotalValue"] = totalValue;
                            ViewData["Results"] = tickets;
                        }
                    }
                    
                    ModelState.Clear();
                    return View(vm);
                }

                case "POST" when !ModelState.IsValid:
                    ViewBag.Error = "Error";
                    return View(vm);
            }
            
            if (ticketsPendingPurchase != null)
            {
                try
                {
                    string[] ticketsGuid = ticketsPendingPurchase.Split("&")
                        .Where(t => !string.IsNullOrEmpty(t)).ToArray();

                    var purchase = new PurchaseTicketRequest
                    {
                        TicketsGuid = ticketsGuid,
                        User = user,
                        SelectCreditCardGuid = vm.CreditCardGuid,
                        CardNumber = vm.CardNumber,
                        Holder = vm.Holder,
                        ExpirationDate = vm.ExpirationDate,
                        SecurityCode = vm.SecurityCode
                    };

                    var (returnCode, messageSold, message) = await _ticketsService.PurchaseAsync(purchase);

                    switch (returnCode)
                    { 
                        case "2": // Autorizado
                            await PurchasePaid(ticketsGuid, user);
                            return RedirectToAction("Checkout");
                        case "3": // Recusado
                            var (listTicket, messageError, totalValueDa)  = await PurchaseRefused(ticketsGuid, messageSold);

                            ViewData["TotalValue"] = totalValueDa;
                            ViewData["Results"] = listTicket;
                            ViewBag.Message = messageError;
                            return View(vm);
                        case "TICKETS_SOLD":
                            var (tickets, messageReturn, totalValue) = await TicketsSold(messageSold, ticketsPendingPurchase, message);

                            ViewBag.Message = messageReturn;
                            ViewData["TotalValue"] = totalValue;
                            ViewData["Results"] = tickets;
                            return View(vm);
                        default:
                            List<Ticket> list = new List<Ticket>();
                            foreach (var guid in ticketsGuid)
                            {
                                list.Add(await _ticketsService.GetAsync(new Guid(guid)));
                            }
                            
                            var totalValueD = list.Select(t => t.Draw.ValueOfTicket).Sum();

                            StringBuilder ticketsNumber = new StringBuilder();
                            foreach (var item in list)
                            {
                                ticketsNumber.Append($"{item.TicketNumber} ;");
                            }

                            await _emailSender.SendEmailAsync(
                                "apptotolecshow@gmail.com",
                                "Erro ao processar o pagamento - Totolec Show",
                                $"Cliente: {user.FirstName} <br/> E-mail: {user.Email} <br/> Cartelas: {ticketsNumber} <br/> Erro retornado: {messageSold}");
                        
                            ViewData["TotalValue"] = totalValueD;
                            ViewData["Results"] = list;
                            ViewBag.Message = "Erro no processamento do pagamento, por favor contate um administrador.";
                            return View(vm);
                    }
                }
                catch (Exception e)
                {
                    await _emailSender.SendEmailAsync(
                        "apptotolecshow@gmail.com",
                        "Erro no processamento - Totolec Show",
                        $"Cliente: {user.FirstName} <br/> E-mail: {user.Email} <br/> Erro retornado: {e.Message} <br/> StackTrace: {e.StackTrace}");
                    
                    Console.WriteLine($"{e.Message} - {e.StackTrace}");
                    
                    ViewBag.Message = e.Message;
                    return View(vm);
                }
            }

            ViewBag.Message = "Erro no processamento do pagamento, por favor contate um administrador.";
            return View(vm);
        }

        private async Task<(List<Ticket>, string, decimal)> PurchaseRefused(string[] ticketsGuid, string messageSold)
        {
            List<Ticket> list = new List<Ticket>();
            foreach (var guid in ticketsGuid)
            {
                list.Add(await _ticketsService.GetAsync(new Guid(guid)));
            }

            var totalValue = list.Select(t => t.Draw.ValueOfTicket).Sum();

            var messageErrorRefused = "Pagamento recusado, entre em contato com o banco emissor do cartão para verificar a situação. Revise os dados do cartão de crédito informado.";

            return (list, messageErrorRefused, totalValue);
        }

        private async Task<(List<Ticket>, string, decimal)> PurchaseRefund(string[] ticketsGuid, string messageSold)
        {
            List<Ticket> lista = new List<Ticket>();
            foreach (var guid in ticketsGuid)
            {
                lista.Add(await _ticketsService.GetAsync(new Guid(guid)));
            }

            var totalValue = lista.Select(t => t.Draw.ValueOfTicket).Sum();

            var messageError = "";
            messageError = !string.IsNullOrEmpty(messageSold) ? messageSold : "Erro ao processar seu pagamento, por favor entre em contato com a Totolec para mais informações.";

            return (lista, messageError, totalValue);
        }

        private async Task PurchasePaid(string[] ticketsGuid, AppUser user)
        {
            try
            {
                //Enviar E-mail
                var path = Path.Combine(
                    Directory.GetCurrentDirectory(),
                    "EmailTemplates", "EmailCheckoutPurchase" + ".html");

                string html = await System.IO.File.ReadAllTextAsync(path);

                var textEmail = await _messagesService.GetAsync(TypeMessage.Email);
                string textReplace = html.Replace("#TextMessage#", textEmail.Message);

                var imgCheckout = await _fileImportsService.ListAsync(TypeFile.ImgOfCheckout);
                string imgReplace = textReplace.Replace("#imgCheckout#", imgCheckout.FirstOrDefault()?.FileUri);

                var draw = await _drawsService.GetActiveAsync();
                string dataSorteioReplace = imgReplace.Replace("#DataSorteio#", draw.DrawDate.ToString("dd/MM/yyyy hh:mm"));

                var listTickets = "";
                foreach (var tick in ticketsGuid)
                {
                    var ticket = await _ticketsService.GetAsync(new Guid(tick));

                    var template = @"<tr style=border-bottom: 1px solid rgba(0,0,0,.05);'>"
                                   + @"<td valign='middle' style='text-align:left; padding: 0 2.5em;'>"
                                   + @"<hr/>"
                                   + @"<div class='product-entry'>"
                                   + @"<div class='text'>"
                                   + @"<h6>Número da cartela: " + ticket.TicketNumber + "</h6>"
                                   + @"</div>"
                                   + @"</div>"
                                   + @"</td>"
                                   + @"</tr>";

                    listTickets += template;
                }

                string listTicketsReplace = dataSorteioReplace.Replace("#ListTickets#", listTickets);

                await _emailSender.SendEmailAsync(
                    user.Email,
                    "Compra efetuada - Totolec Show",
                    listTicketsReplace);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            // Remover tickets do cookie
            SetCookieTicket("Tickets", string.Empty, 30);
        }

        private async Task<(List<Ticket>, string, decimal)> TicketsSold(string messageSold, string ticketsPendingPurchase, IList<string> message)
        {
            string[] sold = messageSold.Split("&")
                .Where(t => !string.IsNullOrEmpty(t)).ToArray();

            List<Ticket> ticketsSold = new List<Ticket>();
            List<Ticket> tickets = new List<Ticket>();

            foreach (var guid in sold)
            {
                if (!guid.Contains("&"))
                {
                    var ticket = await _ticketsService.GetAsync(new Guid(guid));

                    if (ticket != null)
                        ticketsSold.Add(ticket);

                    var newTicketsPendingPurchase = ticketsPendingPurchase.Replace(guid, string.Empty);
                    SetCookieTicket("Tickets", newTicketsPendingPurchase, 30);
                }
            }

            foreach (var guid in message)
            {
                if (!sold.Contains(guid))
                {
                    tickets.Add(await _ticketsService.GetAsync(new Guid(guid)));
                }
            }

            var messageReturn = "";
            if (ticketsSold.Count >= 1)
            {
                messageReturn =
                    $"Poxa, a cartela: {ticketsSold.Select(t => t.SimplifiedTicketNumber).FirstOrDefault()} já foi vendida, escolha outra cartela da sorte e aproveite!";
            }
            else
            {
                StringBuilder textSold = new StringBuilder();
                foreach (var item in ticketsSold)
                {
                    textSold.Append($"{item.SimplifiedTicketNumber}; ");
                }

                messageReturn =
                    $"Poxa, as cartelas: {textSold} já foram vendidas, escolha outras cartelas da sorte e aproveite!";
            }

            var totalValue = tickets.Select(t => t.Draw.ValueOfTicket).Sum();

            return (tickets, messageReturn, totalValue);
        }

        public async Task<IActionResult> Checkout()
        {
            var draw = await _drawsService.GetAvailableAsync();
            ViewBag.DrawDate = draw.DrawDate;

            var menssageCheckout = await _messagesService.GetAsync(TypeMessage.Checkout);
            var imgCheckout = await _fileImportsService.ListAsync(TypeFile.ImgOfCheckout);
            
            ViewData["Menssage"] = menssageCheckout.Message;
            ViewData["Image"] = imgCheckout.FirstOrDefault();
            
            return View();
        }
        
        public void SetCookieTicket(string key, string value, int? expireTime)  
        {  
            CookieOptions option = new CookieOptions();

            option.SameSite = SameSiteMode.None;
            option.IsEssential = true;
            option.Secure = true;
            
            if (expireTime.HasValue)  
                option.Expires = DateTime.Now.AddMinutes(expireTime.Value);  
            else  
                option.Expires = DateTime.Now.AddMilliseconds(10);  
   
            HttpContext.Response.Cookies.Append(key, value, option);  
        } 
        
        public async Task<IActionResult> RemoveCookieTicket(string ticket)
        {
            var ticketsPendingPurchase = HttpContext.Request.Cookies["Tickets"];

            if (ticketsPendingPurchase != null && ticketsPendingPurchase.Contains(ticket))
            {
                var newTicketsPendingPurchase = ticketsPendingPurchase.Replace(ticket, string.Empty);
                
                SetCookieTicket("Tickets", newTicketsPendingPurchase, 30);
            }

            return RedirectToAction("Cart", "Purchases", null);
        }

        public async Task<IActionResult> RemoveCreditCard(string guid)
        {
            await _creditCardTokenizeService.DeleteAsync(new Guid(guid));
           
            return RedirectToAction("Payment", "Purchases");
        }
    }
}