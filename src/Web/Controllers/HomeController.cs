﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Core.DTOs.Tickets;
using Core.Entities;
using Core.Services.Interfaces;
using Core.Utils;
using Web.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using X.PagedList;
using Queryable = Core.DTOs.Queryable;

namespace Web.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private readonly ITicketsService _ticketsService;
        private readonly IDrawsService _drawsService;
        private readonly IFileImportsService _fileImportsService;
        private readonly IAlertsService _alertsService;
        private readonly IMessagesService _messagesService;
        private readonly UserManager<AppUser> _userManager;

        public HomeController(ITicketsService ticketsService, 
                                IDrawsService drawsService, 
                                IFileImportsService fileImportsService, 
                                IAlertsService alertsService,
                                IMessagesService messagesService,
                                UserManager<AppUser> userManager)
        {
            _ticketsService = ticketsService;
            _drawsService = drawsService;
            _fileImportsService = fileImportsService;
            _alertsService = alertsService;
            _messagesService = messagesService;
            _userManager = userManager;
        }

        public async Task<IActionResult> Index(Queryable vm, string returnView)
        {
            var banners = await _fileImportsService.ListAsync(TypeFile.Banner);
            var alert = await _alertsService.GetActiveAsync();
            var draw = await _drawsService.GetAvailableAsync();
            if (draw == null) 
            {
                var imgNextDraw = await _fileImportsService.ListAsync(TypeFile.ImgNextDraw);
                var messageNextDraw = await _messagesService.GetAsync(TypeMessage.NextDraw);
                
                ViewData["ImgNextDraw"] = imgNextDraw.FirstOrDefault();
                ViewData["messageNextDraw"] = messageNextDraw.Message;
                
                return View();
            }
            
            var ticketsPendingPurchase = HttpContext.Request.Cookies["Tickets"];

            if (ticketsPendingPurchase == null)
                ticketsPendingPurchase = "";

            int pagSize = 0;
            string[] ticketsGuid = ticketsPendingPurchase.Split("&")
                .Where(t => !string.IsNullOrEmpty(t)).ToArray();

            pagSize = ticketsGuid.Length;
                
            var res = await _ticketsService.ListSuggestionsAsync(new ListTicketsRequest
            {
                Page = vm.Page,
                PageSize = 6 + pagSize,
                DrawGuid = draw.Guid
            });

            List<Ticket> ticketsRes = new List<Ticket>();
            foreach (var item in res.Results)
            {
                if (!ticketsGuid.Contains(item.Guid.ToString()))
                    ticketsRes.Add(item);
            }
                
            if (ticketsRes.Count > 6)
                ticketsRes.RemoveRange(0, ticketsRes.Count - 6);

            ViewBag.ReturnView = returnView;
            ViewBag.UrlVT = draw.UrlCommercial;
            ViewData["Alert"] = alert;
            ViewData["DateDraw"] = draw.DrawDate.ToString("dd/MM/yyyy");
            ViewData["DrawNumber"] = draw.DrawNumber;
            ViewData["Banners"] = banners;
            ViewData["DrawGuid"] = draw.Guid;
            ViewData["Count"] = res.Count;
            ViewBag.PathImageDraw = draw.FileImports.FirstOrDefault(d => d.TypeFile == TypeFile.ImgOfTicket)?.FileUri;
            ViewData["Results"] = new StaticPagedList<Ticket>(
                ticketsRes, vm.Page, vm.PageSize, res.Count);
            
            var user = await _userManager.FindByNameAsync(User.Identity.Name);

            if (user != null && user.CompleteRegistration && string.IsNullOrEmpty(user.AddressNumber))
            {
                ViewBag.AddAddressNumber = "AddressNumber";
            }

            return View();
        }

        public async Task<IActionResult> Suggestions(string balls, Queryable vm, Guid drawGuid, int drawNumber, string returnView)
        {
            var convertBalls = new int[] { };
            
            if (!string.IsNullOrEmpty(balls))
            {
                List<string> ballsArrayConvert = new List<string>();
                var arrayBalls = balls.Split(",");

                foreach (var ball in arrayBalls)
                {
                    if (ball != String.Empty)
                    {
                        ballsArrayConvert.Add(ball);
                    }
                }
                
                convertBalls = Array.ConvertAll(ballsArrayConvert.ToArray(), s => int.Parse(s));
            }
            
            var banners = await _fileImportsService.ListAsync(TypeFile.Banner);
            
            var ticketsPendingPurchase = HttpContext.Request.Cookies["Tickets"];
            
            if (ticketsPendingPurchase == null)
                ticketsPendingPurchase = "";
            
            int pagSize = 0;
            string[] ticketsGuid = ticketsPendingPurchase.Split("&")
                .Where(t => !string.IsNullOrEmpty(t)).ToArray();
            
            pagSize = ticketsGuid.Length;
            
            var result = await _ticketsService.ListSuggestionsAsync(new ListTicketsRequest
            {
                Page = vm.Page,
                PageSize = 9 + pagSize,
                Balls = convertBalls,
                DrawGuid = drawGuid
            });
         
            List<Ticket> ticketsRes = new List<Ticket>();
            foreach (var item in result.Results)
            {
                if (!ticketsGuid.Contains(item.Guid.ToString()))
                    ticketsRes.Add(item);
            }
            
            if (ticketsRes.Count > 9)
                ticketsRes.RemoveRange(0, ticketsRes.Count - 9);

            ViewBag.ReturnView = returnView;
            ViewData["DrawNumber"] = drawNumber;
            ViewData["DrawGuid"] = drawGuid;
            ViewData["Banners"] = banners;
            ViewData["BallsSelected"] = balls;
            ViewData["Count"] = result.Count;
            ViewData["Results"] = new StaticPagedList<Ticket>(
                ticketsRes, vm.Page, vm.PageSize, result.Count);
            
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel {RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier});
        }
    }
}