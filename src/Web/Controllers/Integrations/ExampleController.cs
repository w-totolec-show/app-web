using System;
using System.Threading.Tasks;
using Web.Helpers;
using Microsoft.AspNetCore.Mvc;

namespace Web.Controllers.Integrations
{
    [Route("integrations/example")]
    public class ExampleController : Controller
    {
        private readonly IRazorViewToStringRenderer _razorViewToStringRenderer;

        public ExampleController(
            IRazorViewToStringRenderer razorViewToStringRenderer)
        {
            _razorViewToStringRenderer = razorViewToStringRenderer;
        }

        [HttpGet]
        [Route("potential_saving")]
        public async Task<IActionResult> GetPotentialSaving(string type, string assetGuid, int? yearMonth)
        {
            // if (!AuthorizeRequest())
            //     return StatusCode(StatusCodes.Status401Unauthorized);

            try
            {
                var viewHtml =
                    await _razorViewToStringRenderer
                        .RenderViewToStringAsync($"~/Views/SomeFolder/{type}.cshtml", new { @name = "SomeViewModelHere" });

                return new JsonResult(new
                {
                    renderedHtml = viewHtml
                });
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);

                return new JsonResult(new
                {
                    renderedHtml = "--"
                });
            }
        }
    }
}