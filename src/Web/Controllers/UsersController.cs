using System;
using System.IO;
using System.Threading.Tasks;
using Core.DTOs;
using Core.DTOs.Tickets;
using Core.DTOs.Users;
using Core.Entities;
using Core.Services.Interfaces;
using Core.Utils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using ServiceStack;
using Web.ViewModels.Users;
using X.PagedList;
using System.Linq;
using System.Net;
using Microsoft.AspNetCore.Hosting;
using Newtonsoft.Json.Linq;
using RestSharp;

namespace Web.Controllers
{
    [Authorize(Roles = "Client")]
    public class UsersController : Controller
    {
        private readonly IUsersService _usersService;
        private readonly UserManager<AppUser> _userManager;
        private readonly ITicketsService _ticketsService;
        private readonly IWebHostEnvironment _environment;
        
        public UsersController(IUsersService usersService, UserManager<AppUser> userManager, ITicketsService ticketsService, IWebHostEnvironment environment)
        {
            _usersService = usersService;
            _userManager = userManager;
            _ticketsService = ticketsService;
            _environment = environment;
        }
        
        public async Task<IActionResult> Profile(EditUserViewModel vm, bool isSaved)
        {
            var user = await _userManager.FindByNameAsync(User.Identity.Name);
            
            switch (Request.Method)
            {
                case "GET":
                {
                    vm = EditUserViewModel.FromModel(user);
                    ModelState.Clear();
                    if (isSaved) ViewBag.Message = "Informações salvas com sucesso";
                    return View(vm);
                }

                case "POST" when !ModelState.IsValid:
                    ViewBag.Error = "Error";
                    return View(vm);
            }
            
            if (!ValidateInfo.ValidateCpf(vm.SocialId))
            {
                ViewBag.ErrorCpf = "Error";
                return View(vm);
            }

            string city;
            string bairro;
            
            try
            {
                var client = new RestClient("https://viacep.com.br");
                var requestApi = new RestRequest($"ws/{vm.Zipcode}/json/", Method.GET);
                var queryResult = client.Execute<IRestResponse>(requestApi).Content;

                dynamic data = null;
                dynamic dataCepLa = null;
                dynamic dataApiCep = null;
                if (!string.IsNullOrEmpty(queryResult))
                    data = JObject.Parse(queryResult);
                else
                {
                    var clientCepLa = new RestClient("http://cep.la");
                    var requestCepLa = new RestRequest($"/{vm.Zipcode}", Method.GET);
                    requestCepLa.AddHeader("Accept", "application/json");
                    var queryResultCepLa = clientCepLa.Execute<IRestResponse>(requestCepLa).Content;
                        
                    if (!string.IsNullOrEmpty(queryResultCepLa))
                        dataCepLa = JObject.Parse(queryResultCepLa);
                    else
                    {
                        var clientApiCep = new RestClient("https://ws.apicep.com");
                        var requestApiCep = new RestRequest($"/cep.json?code={vm.Zipcode}", Method.GET);
                        var queryResultApiCep = clientApiCep.Execute<IRestResponse>(requestApiCep).Content;
                    
                        if (!string.IsNullOrEmpty(queryResultApiCep))
                            dataApiCep = JObject.Parse(queryResultApiCep);
                    }
                }
            
                if (data != null && !string.IsNullOrEmpty(data.ToString()))
                {
                    city = data.localidade.ToString();
                    bairro = data.bairro.ToString();
                }
                else if (dataCepLa != null && !string.IsNullOrEmpty(dataCepLa.ToString()))
                {
                    city = dataCepLa.cidade.ToString();
                    bairro = dataCepLa.bairro.ToString();
                } else if (dataApiCep != null && !string.IsNullOrEmpty(dataApiCep.ToString()))
                {
                    city = dataApiCep.city.ToString();
                    bairro = dataApiCep.district.ToString();
                }
                else
                {
                    city = "************";
                    bairro = "************";
                }
            }
            catch (Exception e)
            {
                city = "************";
                bairro = "************";
                
                Console.WriteLine(e);
            }
            
            var request = new SaveUserRequest
            {
                Id = user.Id,
                UserName = user.UserName,
                Email = user.Email,
                FirstName = vm.FirstName,
                Lastname = vm.Lastname,
                PhoneNumber = vm.Phone,
                Zipcode = vm.Zipcode,
                SocialId = vm.SocialId,
                AddressNumber = vm.AddressNumber,
                DateOfBirth = new DateTime(vm.Year.ToInt(), vm.Month.ToInt(), vm.Day.ToInt()),
                CompleteRegistration = true,
                IsAdmin = false,
                IsActive = true,
                Roles = new string[] { "Client" },
                UserModification = user.Id,
                City = city,
                Locality = bairro
            };

            await _usersService.SaveAsync(request);
            
            return RedirectToAction("Profile", new {isSaved = true});
        }

        public async Task<IActionResult> ExportTicket(Guid guid)
        {
            var ticket = await _ticketsService.GetAsync(guid);
            
            var fileUri = ticket.Draw.FileImports.Where(f => f.TypeFile == TypeFile.Regulations);

            byte[] result = null;
            using (WebClient webClient = new WebClient())
            {
                result = webClient.DownloadData(fileUri.First().FileUri);
            }
            
            return ExportUtilities.ExportTicketPdf(ticket, result);
        }

        public async Task<IActionResult> Tickets()
        {
            var user = await _userManager.FindByNameAsync(User.Identity.Name);

            var tickets = await _ticketsService.ListCustomerAllAsync(user.Id);

            var ticketsGroupBy = tickets.GroupBy(t => t.Draw.DrawNumber);

            ViewData["ticketsGroupBy"] = ticketsGroupBy;
            
            return View();
        }
    }
}