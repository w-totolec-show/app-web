﻿using System.ComponentModel;
using Core.Entities;

namespace Web.ViewModels.Shared
{
    public class ContactInfoViewModel
    {
        [DisplayName("Nome")]
        public string Firstname { get; set; }

        [DisplayName("Sobrenome")]
        public string Lastname { get; set; }

        [DisplayName("E-mail")]
        public string Email { get; set; }

        [DisplayName("Telefone")]
        public string Telephone1 { get; set; }

        [DisplayName("Telefone 2")]
        public string Telephone2 { get; set; }

        [DisplayName("Telefone 3")]
        public string Telephone3 { get; set; }

        [DisplayName("Observações")]
        public string Observations { get; set; }

        public static ContactInfoViewModel FromModel(ContactInfo contact)
        {
            var vm = new ContactInfoViewModel();

            if (contact != null)
            {
                vm.Firstname = contact.Firstname;
                vm.Lastname = contact.Lastname;
                vm.Email = contact.Email;
                vm.Telephone1 = contact.Telephone1;
                vm.Telephone2 = contact.Telephone2;
                vm.Telephone3 = contact.Telephone3;
                vm.Observations = contact.Observations;
            }

            return vm;
        }
    }

    public static class ContactInfoViewModelExtensionMethods
    {
        public static ContactInfo ToModel(this ContactInfoViewModel vm)
        {
            if (string.IsNullOrEmpty(vm.Firstname) && string.IsNullOrWhiteSpace(vm.Lastname))
                return null;

            return new ContactInfo
            {
                Firstname = vm.Firstname,
                Lastname = vm.Lastname,
                Email = vm.Email,
                Telephone1 = vm.Telephone1,
                Telephone2 = vm.Telephone2,
                Telephone3 = vm.Telephone3,
                Observations = vm.Observations
            };
        }
    }
}