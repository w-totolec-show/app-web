using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Core.Entities;
using Web.Areas.Admin.ViewModels.Shared;

namespace Web.ViewModels.Users
{
    public class EditUserViewModel
    {
        public Guid? Guid { get; set; }
        
        [DisplayName("Nome*")]
        [Required(ErrorMessage = "Nome obrigatório")]
        public string FirstName { get; set; }

        [DisplayName("Sobrenome*")]
        [Required(ErrorMessage = "Sobrenome obrigatório")]
        public string Lastname { get; set; }
        
        [DisplayName("CPF")]
        [Required(ErrorMessage = "CPF obrigatório")]
        public string SocialId { get; set; } // CPF
        
        [DisplayName("CEP")]
        [Required(ErrorMessage = "CEP obrigatório")]
        public string Zipcode { get; set; }
        
        [DisplayName("Dia")]
        [Required(ErrorMessage = "Dia obrigatório")]
        public string Day { get; set; }
        
        [DisplayName("Mês")]
        [Required(ErrorMessage = "Mês obrigatório")]
        public string Month { get; set; }
        
        [DisplayName("Ano")]
        [Required(ErrorMessage = "Ano obrigatório")]
        public string Year { get; set; }
        
        [DisplayName("Celular")]
        [Required(ErrorMessage = "Celular obrigatório")]
        public string Phone { get; set; }
        
        [DisplayName("Número da residência")]
        [Required(ErrorMessage = "Número da residência obrigatório")]
        public string AddressNumber { get; set; }
        
        public bool CompleteRegistration { get; set; }
        
        public static EditUserViewModel FromModel(AppUser user)
        {
            var vm = new EditUserViewModel();

            if (user != null)
            {
                vm.Guid = user.Id;
                vm.FirstName = user.FirstName;
                vm.Lastname = user.LastName;
                vm.SocialId = user.SocialId;
                vm.Zipcode = user.Zipcode;
                vm.Day = user.DateOfBirth.Day.ToString();
                vm.Month = user.DateOfBirth.Month.ToString();
                vm.Year = user.DateOfBirth.Year.ToString();
                vm.Phone = user.PhoneNumber;
                vm.CompleteRegistration = user.CompleteRegistration;
                vm.AddressNumber = user.AddressNumber;
            }

            return vm;
        }
    }
}