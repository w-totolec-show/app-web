using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Web.ViewModels.Purchases
{
    public class PaymentViewModel
    {
        [DisplayName("Número do cartão de crédito")]
        public string CardNumber { get; set; }
        
        [DisplayName("Titular do cartão")]
        public string Holder { get; set; }
        
        [DisplayName("Data de expiração")]
        public string ExpirationDate { get; set; }
        
        [DisplayName("Código de segurança")]
        public string SecurityCode { get; set; }

        public Guid? CreditCardGuid { get; set; }

        public string CardHash { get; set; }
        public string CardType { get; set; }
    }
}