using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Core.Entities;

namespace Web.ViewModels.Purchases
{
    public class EditClientViewModel
    {
        public Guid? Guid { get; set; }
        
        [DisplayName("Nome completo")]
        [Required(ErrorMessage = "Nome completo obrigatório")]
        public string Name { get; set; }
        
        [DisplayName("CPF")]
        [Required(ErrorMessage = "CPF obrigatório")]
        public string SocialId { get; set; } // CPF
        
        [DisplayName("CEP")]
        [Required(ErrorMessage = "CEP obrigatório")]
        public string Zipcode { get; set; }
        
        [DisplayName("Dia")]
        [Required(ErrorMessage = "Dia obrigatório")]
        public string Day { get; set; }
        
        [DisplayName("Mês")]
        [Required(ErrorMessage = "Mês obrigatório")]
        public string Month { get; set; }
        
        [DisplayName("Ano")]
        [Required(ErrorMessage = "Ano obrigatório")]
        public string Year { get; set; }
        
        [DisplayName("Celular")]
        [Required(ErrorMessage = "Celular obrigatório")]
        public string Phone { get; set; }
        
        [DisplayName("Número da residência")]
        [Required(ErrorMessage = "Número da residência obrigatório")]
        public string AddressNumber { get; set; }
        
        public static EditClientViewModel FromModel(AppUser user)
        {
            var vm = new EditClientViewModel();

            if (user != null)
            {
                vm.Guid = user.Id;
                vm.Name = user.FirstName;
                vm.Phone = user.PhoneNumber;
                vm.SocialId = user.SocialId;
                vm.Zipcode = user.Zipcode;
            }

            return vm;
        }
    }
}