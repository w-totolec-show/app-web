using System;
using System.Data;
using System.Data.Common;
using System.Data.OleDb;
using System.IO;
using System.Runtime.InteropServices;
using FirebirdSql.Data.FirebirdClient;
using FirebirdSql.Data.Services;
using Xunit;
using xdb=lcpi.data.oledb;

namespace Tests.Utils
{
    public class GDBGenarateTests
    {
        [Fact]
        public void GenerateGDB()
        {
            var path = Path.Combine(
                Directory.GetCurrentDirectory(),
                "GDBFiles");
            
            string connectionString =
                "Database=BASE_EXPORTACAO.GDB; Dialect=3; DataSource=209.126.9.195; Port=9030;" +
                "User=totolec; Password=asdf1234;";
            
            /*string connectionString =
                "Database=xKztynekq8WRT8eb; Dialect=3; DataSource=18.217.99.7; Port=8081;" +
                "User=XTeeFM57yVNKq4mu; Password=CzUWbB67ApmEhaaL;";*/
            
            try
            {
                string connt =
                    "User=SYSDBA;Password=masterkey;Database=C:\\Data\\BASE_EXPORTACAO.GDB;DataSource=3.135.220.229;Port=3389;Dialect=3;Charset=NONE;Role=;Connection lifetime=15;Pooling=true;MinPoolSize=0;MaxPoolSize=50;Packet Size=8192;ServerType=0;";
                FbConnection testCnn = new FbConnection(connt);
                testCnn.Open();
                
                
                FbRestore restoreSvc = new FbRestore();

                restoreSvc.ConnectionString = connectionString;
                restoreSvc.Options = FbRestoreFlags.Create | FbRestoreFlags.Replace;
                restoreSvc.Verbose = true;
                restoreSvc.BackupFiles.Add(new FbBackupFile($"{path}/BASE_EXPORTACAO.GDB"));

                //restoreSvc.ServiceOutput += new EventHandler<ServiceOutputEventArgs>(ServiceOutput);

                restoreSvc.Execute();
                
                FbConnection aConnection = new FbConnection(connectionString);
                aConnection.Open();
                IDbCommand dbcmd = aConnection.CreateCommand();
                //string sql = "DROP TABLE GAVETA; DROP TABLE LOTE; DROP TABLE CARTELA;";

                string gaveta = "CREATE TABLE GAVETA ( GVT_COD_DISTR CHAR(3) CHARACTER SET WIN1254 NOT NULL, GVT_NUM_EXTRACAO CHAR(3) CHARACTER SET WIN1254 NOT NULL, GVT_COD CHAR(3) CHARACTER SET WIN1254 NOT NULL, GVT_TRANSF INTEGER DEFAULT 0 );";
                dbcmd.CommandText = gaveta;
                dbcmd.ExecuteNonQuery();
                
                string lote = "CREATE TABLE LOTE ( LTE_COD_DISTR CHAR(3) CHARACTER SET WIN1254 NOT NULL, LTE_NUM_EXTRACAO CHAR(3) CHARACTER SET WIN1254 NOT NULL, GVT_COD CHAR(3) CHARACTER SET WIN1254 NOT NULL, LTE_COD CHAR(4) CHARACTER SET WIN1254 NOT NULL, LTE_TRANSF INTEGER DEFAULT 0 );";
                dbcmd.CommandText = lote;
                dbcmd.ExecuteNonQuery();
                
                string cartela = "CREATE TABLE CARTELA ( LTE_COD_DISTR CHAR(3) CHARACTER SET WIN1254 NOT NULL, LTE_NUM_EXTRACAO CHAR(3) CHARACTER SET WIN1254 NOT NULL, GVT_COD CHAR(3) CHARACTER SET WIN1254 NOT NULL, LTE_COD CHAR(4) CHARACTER SET WIN1254 NOT NULL, CRT_COD_BAR CHAR(17) CHARACTER SET WIN1254 NOT NULL, CRT_TRANSF INTEGER DEFAULT 0 );";
                dbcmd.CommandText = cartela;
                dbcmd.ExecuteNonQuery();
                
                aConnection.Close();
                
                using (var transaction = aConnection.BeginTransaction())
                {
                        
                    using (var test = new FbCommand(
                        "CREATE TABLE CARTELA ( LTE_COD_DISTR CHAR(3) CHARACTER SET WIN1254 NOT NULL, LTE_NUM_EXTRACAO CHAR(3) CHARACTER SET WIN1254 NOT NULL, GVT_COD CHAR(3) CHARACTER SET WIN1254 NOT NULL, LTE_COD CHAR(4) CHARACTER SET WIN1254 NOT NULL, CRT_COD_BAR CHAR(17) CHARACTER SET WIN1254 NOT NULL, CRT_TRANSF INTEGER DEFAULT 0);",
                        aConnection, transaction))
                    {
                        test.ExecuteNonQuery();
                    }
                }
                
                aConnection.Close();
              

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            
        }
    }
}