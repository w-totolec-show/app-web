using Core.Utils;
using Xunit;

namespace Tests.Utils
{
    public class DrawStringTests
    {
        [Fact]
        public void DrawStringTest()
        {
            var draw = DrawString.Draw("//Users/feliperodrigues/Documents/Repository/Totolec-show/app-web/Tests/Utils/Verso.jpg");
            Assert.True(draw);
        }
    }
}