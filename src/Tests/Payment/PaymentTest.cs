using System;
using System.Collections.Generic;
using Core.DTOs.Tickets;
using Core.Infrastructure;
using Core.Services;
using Core.Services.Interfaces;
using Microsoft.Extensions.Configuration;
using Moq;
using Xunit;

namespace Tests.Payment
{
    public class PaymentTest
    {
        /*[Fact]
        public void PaymentCreditTest()
        {
            string projectPath = AppDomain.CurrentDomain.BaseDirectory.Split(new String[] { @"\" }, StringSplitOptions.None)[0];
            IConfiguration config = new ConfigurationBuilder()
                .SetBasePath(projectPath)
                .AddJsonFile("appsettings.Development.json")
                .Build();
            
            var mock = new Mock<PaymentService>(config);

            var card = new CreditCard
            {
                CardNumber = "5154913454193605",
                Holder = "Felipe Rodrigues",
                ExpirationDate = "08/2021",
                SecurityCode = "982"
            };
            
            var tokenize = mock.Object.TokenizeCreditCard(card);
            string codeTypeCard = tokenize.CardType ==  "Débito" ? "4" : "2";
            
            var transaction = new Transaction<CreditCard>()
            {
                IsSandbox = true,

                CallbackUrl = "https://localhost:5001/Purchases/Payment/",
                Application = "Totolec Show",
                Vendor = "Totolec Show",
                Reference = "safe2pay-dotnet",
                Customer = new Customer
                {
                    Name = "Felipe Rodrigues",
                    Identity = "845.596.680-78",
                    Email = "felipe@indevs.com.br",
                    Phone = "51 99999 9999",
                    Address = new Address
                    {
                        Street = "Rua Tomaz Gonzaga",
                        Number = "481",
                        District = "Três Figueiras",
                        ZipCode = "91340-480",
                        CityName = "Porto Alegre",
                        StateInitials = "RS",
                        CountryName = "Brasil"
                    }
                },
                Products = new List<Product>
                {
                    new Product { Code = "001", Description = "Produto 1", UnitPrice = 0.1M, Quantity = 1M },
                },
                PaymentMethod = new PaymentMethod { Code = codeTypeCard },
                PaymentObject = new CreditCard { Token = tokenize.Token }
            };
            
            var res = mock.Object.GenerateTransactionCreditCard(transaction);
 
            Assert.NotNull(res);
        }*/

        [Fact]
        public void SetPaymentTestInTickets()
        {
            string projectPath = AppDomain.CurrentDomain.BaseDirectory.Split(new String[] { @"\" }, StringSplitOptions.None)[0];
            IConfiguration config = new ConfigurationBuilder()
                .SetBasePath(projectPath)
                .AddJsonFile("appsettings.Development.json")
                .Build();
            
            var mockDb = new Mock<ApplicationDbContext>();
            
            var mock = new Mock<TicketsService>(config, mockDb.Object);

            var listTicketsRequest = new ListTicketsRequest
            {
                DrawGuid = new Guid("90fd6832-604b-4f82-ab4d-ad34e69563cb"),
                PageSize = 10
            };

            var listTickets = mock.Object.ListSuggestionsAsync(listTicketsRequest);

            Assert.NotNull(listTickets);
        }
    }
}