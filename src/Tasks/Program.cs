﻿using System;
using System.Threading.Tasks;
using Core.Services;
using Core.Utils;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Tasks
{
    internal static class Program
    {
        private static async Task Main(string[] args)
        {
            var configuration = GetConfigurationBuilder();
            var serviceCollection = new ServiceCollection();
            serviceCollection.Configure(configuration);
            
            serviceCollection.AddScoped<ImportTask>();
            serviceCollection.AddScoped<ExportTask>();
            serviceCollection.AddScoped<Scheduler>();
            
            serviceCollection.AddSingleton((sp) => GetConfigurationBuilder());

            var provider = serviceCollection.BuildServiceProvider();
            var scheduler = provider.GetService<Scheduler>();

            await scheduler.Run();
        }
        
        private static IConfiguration GetConfigurationBuilder()
        {
            var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            var configurationBuilder = new ConfigurationBuilder();
            configurationBuilder.AddProjectConfigurations(environmentName);
     
            var configuration = configurationBuilder.Build();

            return configuration;
        }
    }
}