using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ClosedXML.Excel;
using Core.Entities;
using Core.Infrastructure;
using Core.Services.Interfaces;
using Core.Utils;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Text;
using RestSharp;
using ServiceStack;
using Type = Core.Entities.Type;

namespace Tasks
{
    public class ExportTask
    {
        private readonly IQueueItemService _queueItemService;
        private readonly ITicketsService _ticketsService;
        //private readonly IPaymentService _paymentService;
        private readonly IFileManager _fileManager;
        private readonly string _uriPath;
        private readonly string _tokenSafe2Pay;
        private readonly string _isSandbox;

        public ExportTask(IQueueItemService queueItemService, 
            ITicketsService ticketsService,
            //IPaymentService paymentService,
            IFileManager fileManager,
            IConfiguration configuration)
        {
            _queueItemService = queueItemService;
            _ticketsService = ticketsService;
            //_paymentService = paymentService;
            _fileManager = fileManager;
            _uriPath = configuration["CdnPath"];
            _tokenSafe2Pay = configuration["TokenSafe2Pay"];
            _isSandbox = configuration["IsSandbox"];
        }

        public async Task Run()
        {
            var listExportLot = await _queueItemService.ListAwaitingProcessingAsync(Type.ExportLot);
            
            Console.WriteLine($"Total exportação: {listExportLot.Count}");

            if (listExportLot.Any())
            {
                foreach (var item in listExportLot)
                {
                    try
                    {
                        item.ModifiedAt = DateTime.UtcNow;
                        item.StatusQueueItem = StatusQueueItem.Processing;
                        await _queueItemService.SaveAsync(item);

                        //var validatePaymentTickets = "PAGAMENTO_NAOVALIDADO";
                    
                        var ticketsSold = await _ticketsService.ListAllSold(item.DrawGuid.Value);

                        if (ticketsSold.Any())
                        {
                            var ticketsSoldValid = new List<Ticket>();

                            Console.WriteLine($"Iniciando exportação de {ticketsSold.Count} cartelas");
                            
                            /*try
                            {
                                /*var pageNumber = 1;
                                var paymentDateInitial = ticketsSold.FirstOrDefault().Draw.StartDatePurchase.AddDays(-1).ToString("yyyy-MM-dd");
                                var paymentDateEnd = ticketsSold.FirstOrDefault().Draw.EndDatePurchase.AddDays(1).ToString("yyyy-MM-dd");
                                var client = new RestClient("https://api.safe2pay.com.br");
                                var listIdTransaction = new List<int>();

                                Console.WriteLine($"Buscar os pagamentos na Safe2Pay entre {paymentDateInitial} e {paymentDateEnd}");
                                
                                while (true)
                                {
                                    var requestApi = new RestRequest($"/v2/Transaction/List?PageNumber={pageNumber}&RowsPerPage=50&PaymentDateInitial={paymentDateInitial}&PaymentDateEnd={paymentDateEnd}&Object.PaymentMethod.Code=2&Object.TransactionStatus.Code=3&Object.IsSandbox={_isSandbox}", Method.GET);
                                    requestApi.AddHeader("X-API-KEY", _tokenSafe2Pay);
                                    var queryResult = client.Execute<IRestResponse>(requestApi).Content;

                                    if (!string.IsNullOrEmpty(queryResult))
                                    {
                                        var result = JsonConvert.DeserializeObject<ResponseDetailBase>(queryResult);

                                        if (result?.ResponseDetail != null && result.ResponseDetail.Objects.Any() && !result.ResponseDetail.HasError)
                                        {
                                            var listIds = result.ResponseDetail.Objects.Select(t => t.IdTransaction);
                                            listIdTransaction.AddRange(listIds);
                                            pageNumber++;
                                        }
                                        else
                                        {
                                            break;
                                        }
                                    }
                                    else
                                    {
                                        break;
                                    }
                                }#1#

                                if (listIdTransaction.Any())
                                {
                                    Console.WriteLine($"Inicia validação do pagamento em cada cartela");
                                    
                                    foreach (var ticket in ticketsSold)
                                    {
                                        if (listIdTransaction.Contains(Int32.Parse(ticket.IdTransaction)))
                                        {
                                            ticketsSoldValid.Add(ticket);
                                        }
                                        else if (ticket.IsActive)
                                        {
                                            await _ticketsService.DisableAsync(ticket.Guid, false);
                                        }
                                    }

                                    validatePaymentTickets = "PAGAMENTO_VALIDADO";
                                }
                                else
                                {
                                    ticketsSoldValid.AddRange(ticketsSold);
                                }
                                
                                listIdTransaction.Clear();
                            }
                            catch (Exception e)
                            {
                                ticketsSoldValid.AddRange(ticketsSold);
                                Console.WriteLine(e);
                            }*/

                            ticketsSoldValid.AddRange(ticketsSold);
                            ticketsSold.Clear();
                            
                            if (ticketsSoldValid.Any())
                            {
                                Console.WriteLine($"Inicia extração após pagamento das cartelas validadas");
                                
                                var extractionNumber = ticketsSoldValid.FirstOrDefault().ExtractionNumber;

                                var nameFile =
                                    $"SOR{item.Distributor.ToInt():000}_{extractionNumber}{DateTime.UtcNow:yy}";

                                var lots = Split(ticketsSoldValid);

                                await using var memoryStreamXlsx = new MemoryStream();
                                lots.ExportLot(memoryStreamXlsx, extractionNumber, item.Distributor);

                                var lotsPdf = ExportUtilities.ExportLotPdf(lots);

                                await using var ms = new MemoryStream();
                                using var archive = new ZipArchive(ms, ZipArchiveMode.Create, true);
                                
                                var zipEntryXlsx = archive.CreateEntry($"LOTE-{DateTime.UtcNow:dd-MM-yyyy}.xlsx",
                                    CompressionLevel.Fastest);

                                await using (var zipStream = zipEntryXlsx.Open())
                                { 
                                    await zipStream.WriteAsync(memoryStreamXlsx.ToArray(), 0,
                                        memoryStreamXlsx.ToArray().Length);
                                }

                                var currentLot = 1;
                                foreach (var pdf in lotsPdf)
                                {
                                    var zipEntryPdf = archive.CreateEntry($"LOTE-{currentLot:0000}.pdf",
                                        CompressionLevel.Fastest);

                                    await using (var zipStream = zipEntryPdf.Open())
                                    {
                                        await zipStream.WriteAsync(pdf.ToArray(), 0, pdf.ToArray().Length);
                                    }

                                    currentLot++;
                                }

                                var queryFirebird =
                                    ExportUtilities.ExportQueryFirebird(lots, extractionNumber, item.Distributor);

                                var zipEntrySql = archive.CreateEntry($"Query-LOTE.sql",
                                    CompressionLevel.Fastest);

                                await using (var zipStream = zipEntrySql.Open())
                                {
                                    byte[] buffer = Encoding.ASCII.GetBytes(queryFirebird.ToString());
                                    await zipStream.WriteAsync(buffer, 0, buffer.Length);
                                }
                                
                                archive.Dispose();
                                    
                                var fileUri = await _fileManager.SaveFileAsync(ms, "zip", "application/zip",  $"SOR{item.Distributor.ToInt():000}_");

                                await ms.DisposeAsync();
                                ms.Close();

                                item.ResultFilePath = $"{_uriPath}{fileUri}";
                                item.ModifiedAt = DateTime.UtcNow;
                                item.FileName = nameFile;
                                item.FileNameInternal = fileUri.Split("/")[1];
                                item.StatusQueueItem = StatusQueueItem.Processed;

                                await _queueItemService.SaveAsync(item);
                            }
                            else
                            {
                                item.ModifiedAt = DateTime.UtcNow;
                                item.StatusQueueItem = StatusQueueItem.Failure;
                                await _queueItemService.SaveAsync(item);
                            }
                        }
                        else
                        {
                            item.ModifiedAt = DateTime.UtcNow;
                            item.StatusQueueItem = StatusQueueItem.Failure;
                            await _queueItemService.SaveAsync(item);
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine($"Exception exportação: {e.Message}");
                        
                        item.ModifiedAt = DateTime.UtcNow;
                        item.StatusQueueItem = StatusQueueItem.Failure;
                        await _queueItemService.SaveAsync(item);
                    }
                }
            }
        }

        private static List<List<T>> Split<T>(IList<T> source)
        {
            return source
                .Select((x, i) => new { Index = i, Value = x })
                .GroupBy(x => x.Index / 100)
                .Select(x => x.Select(v => v.Value).ToList())
                .ToList();
        }

    }
}