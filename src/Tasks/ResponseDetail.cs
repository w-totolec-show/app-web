using System.Collections.Generic;

namespace Tasks
{
    public class ResponseDetail
    {
        public int TotalItems { get; set; }
        //public List<TransactionResponse> Objects { get; set; }
        public bool HasError { get; set; }
    }

    public class ResponseDetailBase
    {
        public ResponseDetail ResponseDetail { get; set; }
    }
}