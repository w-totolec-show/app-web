using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Core.DTOs.AppLogs;
using Core.DTOs.Tickets;
using Core.Entities;
using Core.Entities.AppLogs;
using Core.Infrastructure;
using Core.Services.Interfaces;
using Core.Utils;
using Ionic.Zip;
using iText.IO.Source;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.Extensions.Configuration;
using ServiceStack;

namespace Tasks
{
    public class ImportTask
    {
        private readonly ITicketsService _ticketsService;
        private readonly IFileImportsService _fileImportsService;
        private readonly IDrawsService _drawsService;
        private readonly IFileManager _fileManager;
        private readonly IAppLogsService _appLogsService;
        private readonly string _uriPath;

        public ImportTask(ITicketsService ticketsService, 
                            IFileImportsService fileImportsService, 
                            IConfiguration configuration,
                            IDrawsService drawsService,
                            IFileManager fileManager,
                            IAppLogsService appLogsService)
        {
            _ticketsService = ticketsService;
            _fileImportsService = fileImportsService;
            _uriPath = configuration["CdnPath"];
            _drawsService = drawsService;
            _fileManager = fileManager;
            _appLogsService = appLogsService;
        }

        public async Task Run()
        {
            var listOfStatus = await _fileImportsService.ListOfStatusAsync(TypeFile.Ticket, Status.AwaitingProcessing);
            
            Console.WriteLine($"Total importação: {listOfStatus.Count}");

            if (listOfStatus.Any())
            {
                foreach (var fileImport in listOfStatus)
                {
                    try
                    {
                        // Alter status
                        fileImport.Status = Status.Processing;
                        fileImport.ModifiedAt = DateTime.UtcNow;
                        await _fileImportsService.SaveAsync(fileImport);
                        
                        // If you have sold cards, generate excel
                        var soldTickets = await _ticketsService.CountSold(fileImport.DrawGuid.Value);
                        if (soldTickets > 0)
                        {
                            var draw = await _drawsService.GetAsync(fileImport.DrawGuid.Value);
                            var ticketsSold = draw.Tickets.Where(t => t.Sold);
                            
                            var obj = ticketsSold.Select(ticket => new
                                {
                                    Id = ticket.Guid,
                                    PrimeiroNomeComprador = ticket.AppUser != null ? ticket.AppUser.FirstName : "",
                                    SobrenomeComprador = ticket.AppUser != null ? ticket.AppUser.LastName : "",
                                    EmailComprador = ticket.AppUser != null ? ticket.AppUser.Email : "",
                                    CelularComprador = ticket.AppUser != null ? ticket.AppUser.PhoneNumber : "",
                                    NumeroCartela = ticket.TicketNumber,
                                    NumeroSimplificadoCartela = ticket.SimplifiedTicketNumber,
                                    ExtracaoConcurso = ticket.ExtractionNumber,
                                    DataCompra = ticket.PurchaseDate,
                                    MeioPagamento = ticket.GatewayPayment,
                                    IdTransacao = ticket.IdTransaction
                                })
                                .Cast<object>()
                                .ToList();
                            
                            await using var memoryStream = new MemoryStream();
                            obj.ExportXlsx("TicketsSold", memoryStream);

                            var fileUri = await _fileManager.SaveFileAsync(memoryStream, "xlsx", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", null);
            
                            var compare = new List<PropertyLog>();
                            
                            var propertyLog = new PropertyLog
                            {
                                LogLevel = 1,
                                Before = $"{_uriPath}{fileUri}",
                                After = "---",
                                Property = "Cartelas vendidas (Carga de cartelas)",
                                CreatedAt = DateTime.UtcNow
                            };
                
                            compare.Add(propertyLog);
                            
                            var logRequest = new SaveLogRequest
                            {
                                LogType = LogType.TicketsSold,
                                UserGuid = fileImport.DrawGuid.Value,
                                PropertyLogs = compare,
                                UserModification = fileImport.UserGuid.Value
                            };

                            await _appLogsService.SaveAsync(logRequest);
                        }
                        
                        // Remove all previous tickets
                        await _ticketsService.DeleteAllAsync(fileImport.DrawGuid.Value);
                        
                        Console.WriteLine("Importação iniciada");
                        // Starts import tickets
                        using var client = new WebClient();
                        var content = client.DownloadData(fileImport.FileUri);
                        var saveTicketRequests = new List<SaveTicketRequest>();
                        await using (var stream = new MemoryStream(content))
                        {
                            using (var outZip = new ZipArchive(stream, ZipArchiveMode.Read))
                            {
                                var fileExtract = outZip.Entries.FirstOrDefault();
                                    
                                if (fileExtract != null)
                                {
                                    await using var file = fileExtract.Open();
                                    using var reader = new StreamReader(file, Encoding.UTF8);

                                    Console.WriteLine("Iniciar leitura do arquivo");
                                    
                                    string line;
                                    while ((line = await reader.ReadLineAsync()) != null)
                                    {
                                        var ticketRequest = new SaveTicketRequest(fileImport.DrawGuid.Value, fileExtract.FullName, line);
                                        saveTicketRequests.Add(ticketRequest);
                                    }

                                    await file.DisposeAsync();
                                    file.Close();
                                }
                                    
                                outZip.Dispose();
                            }
                            
                            await stream.DisposeAsync();
                            stream.Close();
                        }

                        client.Dispose();

                        Console.WriteLine($"Leitura do arquivo finalizada, iniciar inclusão das {saveTicketRequests.Count} cartelas");
                        
                        var saveTickets = Split(saveTicketRequests);

                        saveTicketRequests.Clear();
                        
                        Console.WriteLine($"{saveTickets.Count} lotes de cartela");

                        bool isSaved = false;
                        int countTicketsLot = 1;
                        foreach (var tickets in saveTickets)
                        {
                            isSaved = await _ticketsService.SaveAsync(tickets);

                            if (!isSaved)
                                break;
                            
                            Console.WriteLine($"Lote {countTicketsLot} inserido");
                            
                            countTicketsLot++;
                        }

                        fileImport.Status = isSaved ? Status.Processed : Status.Failure;
                        fileImport.ModifiedAt = DateTime.UtcNow;

                        await _fileImportsService.SaveAsync(fileImport);
                        
                        Console.WriteLine("Importação finalizada");
                    }
                    catch (Exception e) 
                    {
                        Console.WriteLine($"Exception importação: {e.Message}");
                        
                        fileImport.Status =  Status.Failure;
                        fileImport.ModifiedAt = DateTime.UtcNow;

                        await _fileImportsService.SaveAsync(fileImport);
                    }
                }
            }
        }

        private static List<List<T>> Split<T>(IList<T> source)
        {
            return source
                .Select((x, i) => new { Index = i, Value = x })
                .GroupBy(x => x.Index / 1000)
                .Select(x => x.Select(v => v.Value).ToList())
                .ToList();
        }
    }
}