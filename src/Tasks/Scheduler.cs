using System.Threading;
using System.Threading.Tasks;

namespace Tasks
{
    public class Scheduler
    {
        private const int SleepTime = 60;

        private readonly ImportTask _importTask;
        private readonly ExportTask _exportTask;

        public Scheduler(ImportTask importTask, ExportTask exportTask)
        {
            _importTask = importTask;
            _exportTask = exportTask;
        }

        public async Task Run()
        {
            while (true)
            { 
                await _importTask.Run();
                await _exportTask.Run();
                Thread.Sleep(1000 * SleepTime);
            }
        }
    }
}