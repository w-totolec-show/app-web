using System.IO;
using System.Threading.Tasks;

namespace Core.Infrastructure
{
    /// <summary>
    ///     Provides a set of basic interface for file managers, allowing to easily
    ///     change provider companies and strategies.
    /// </summary>
    public interface IFileManager
    {
        /// <summary>
        ///     Save the file.
        /// </summary>
        /// <param name="fileStream">File open stream.</param>
        /// <returns>The stored file URI.</returns>
        Task<string> SaveFileAsync(Stream fileStream, string extension, string contentType, string fileName);

        /// <summary>
        ///     Get a file.
        /// </summary>
        /// <param name="fileUri">The file URI.</param>
        /// <returns>A temp file URI with the file contents.</returns>
        Task<string> GetFileAsync(string fileUri);
    }
}