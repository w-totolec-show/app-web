﻿using System;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.UI.Services;

namespace Core.Infrastructure
{
    public class EmailUtilities : IEmailSender
    {
        public async Task SendAsync(string[] listTo, string subject, string message)
        {
            var mail = new MailMessage();

            foreach (var to in listTo)
            {
                mail.To.Add(new MailAddress(to));
            }

            mail.From = new MailAddress("apptotolecshow@gmail.com");
            mail.Subject = subject;
            mail.Body = message;
            mail.IsBodyHtml = true;
            mail.Priority = MailPriority.High;

            var smtp = new SmtpClient();
            smtp.Host = "smtp.gmail.com";
            smtp.Port = 587;
            smtp.EnableSsl = true;
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = new NetworkCredential("apptotolecshow@gmail.com", "92569256");

            await smtp.SendMailAsync(mail);
        }

        public async Task SendEmailAsync(string email, string subject, string htmlMessage)
        {
            try
            {
                var mail = new MailMessage();
                mail.To.Add(new MailAddress(email));
                mail.From = new MailAddress("notificacoes@totolecshow.com.br");
                mail.Subject = subject;
                mail.Body = htmlMessage;
                mail.IsBodyHtml = true;
                mail.Priority = MailPriority.High;
            
                var smtp = new SmtpClient();
                smtp.Host = "email-smtp.us-east-2.amazonaws.com";
                smtp.Port = 587;
                smtp.EnableSsl = true;
                //smtp.UseDefaultCredentials = true;
                smtp.Credentials = new NetworkCredential("AKIAT4MHLK35EFCGCZ5Q", "BNhHOEf9o1KKwBO/t2nRUlZkZ72ycgGpzcy6HopbB9+4");

                await smtp.SendMailAsync(mail);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
    }
}