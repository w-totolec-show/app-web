﻿using System;
using System.Threading.Tasks;
using Core.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace Core.Infrastructure
{
    public static class DatabaseSeeder
    {
        public static async Task Seed(IServiceProvider services)
        {
            // DbContext from DI
            var dbContext = services.GetRequiredService<ApplicationDbContext>();
            var userManager = services.GetRequiredService<UserManager<AppUser>>();
            var roleManager = services.GetRequiredService<RoleManager<IdentityRole<Guid>>>();

            // Migrations   
            dbContext.Database.Migrate();

            // Create users, initialize collection and parameters etc

            // Create user admin
            var adminUser = await dbContext.Users
                .FirstOrDefaultAsync(u => u.UserName.ToLower() == "admin");

            if (adminUser == null)
            {
                adminUser = new AppUser
                {
                    UserName = "admin@totolecshow.com",
                    FirstName = "Administrator",
                    LastName = "Totolec",
                    Email = "admin@totolecshow.com",
                    IsAdmin = true,
                    IsActive = true
                };

                await userManager.CreateAsync(adminUser, "1O8Hlbup&ustUfRLsw49");
            }

            // Initial Roles
            var adminRole = await roleManager.FindByNameAsync("Administrators");

            if (adminRole == null)
            {
                var rolesToAdd = new[]
                {
                    "Administrators", // Master
                    "Dashboards", // Operacional
                    "FinancialReports",
                    "ExportData",
                    "ManageDraws", // Gerente
                    "ViewDraws",
                    "Client"
                };

                foreach (var roleToAdd in rolesToAdd)
                    await roleManager.CreateAsync(new IdentityRole<Guid>(roleToAdd));

                await userManager.AddToRolesAsync(adminUser, rolesToAdd);
            }
        }
    }
}