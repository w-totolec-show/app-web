using System.IO;
using System.IO.Compression;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Minio;

namespace Core.Infrastructure
{
    public class MinioFileManager : IFileManager
    {
        private readonly string _bucketName = "totolec-show";
        private readonly string _location = "us-east-1";
        private readonly MinioClient _minioClient;

        public MinioFileManager(IConfiguration configuration)
        {
            var endpoint = configuration["MinioEndpoint"];
            var accessKey = configuration["MinioAccessKey"];
            var secretKey = configuration["MinioSecretKey"];

            _minioClient = new MinioClient(endpoint, accessKey, secretKey);
        }

        public async Task<string> SaveFileAsync(Stream fileStream, string extension, string contentType, string fileName)
        {
            // Ensure that the bucket has been created
            var bucketExists = await _minioClient.BucketExistsAsync(_bucketName);

            if (!bucketExists) await _minioClient.MakeBucketAsync(_bucketName, _location);

            await using (var memoryStream = new MemoryStream())
            {
                await using (var compressionStream = new GZipStream(memoryStream, CompressionMode.Compress, true))
                {
                    await fileStream.CopyToAsync(compressionStream);
                }
                
                var objectName = $"uploads/{Path.GetRandomFileName()}.gz";

                await _minioClient.PutObjectAsync(
                    _bucketName,
                    objectName,
                    memoryStream,
                    memoryStream.Length,
                    "application/octet-stream",
                    null,
                    null);

                return $"{_bucketName}/{objectName}";
            }
        }

        public async Task<string> GetFileAsync(string fileUri)
        {
            var tempFilePath = Path.GetTempFileName();
            var tempFilePath2 = Path.GetTempFileName();

            await _minioClient.GetObjectAsync(_bucketName, fileUri, async stream =>
            {
                using var t2FileStream = new FileStream(tempFilePath2, FileMode.Create);
                stream.CopyTo(t2FileStream);

                await using var tempFileStream = new FileStream(tempFilePath, FileMode.Create);
                await using var decompressionStream = new GZipStream(stream, CompressionMode.Decompress);
                await decompressionStream.CopyToAsync(tempFileStream);
            });

            return tempFilePath;
        }
    }
}