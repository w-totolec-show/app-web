using System;
using System.IO;
using System.IO.Compression;
using System.Net;
using System.Threading.Tasks;
using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using Microsoft.Extensions.Configuration;

namespace Core.Infrastructure
{
    public class S3FileManager : IFileManager
    {
        private readonly string _awsS3Bucket;
        private readonly AmazonS3Client _s3Client;

        public S3FileManager(IConfiguration configuration)
        {
            var awsS3AccessKeyId = configuration["AwsS3AccessKeyId"];
            var awsS3SecretAccessKey = configuration["AwsS3SecretAccessKey"];
            var awsS3Region = configuration["AwsS3Region"];
            var awsS3ServiceUrl = configuration["AwsS3ServiceURL"];

            _awsS3Bucket = configuration["AwsS3Bucket"];

            var config = new AmazonS3Config
            {
                RegionEndpoint = RegionEndpoint.USEast1,
                ServiceURL = awsS3ServiceUrl,
                ForcePathStyle = true,
                SignatureVersion = "v4"
            };

            _s3Client = new AmazonS3Client(
                awsS3AccessKeyId,
                awsS3SecretAccessKey,
                config);
        }

        public async Task<string> SaveFileAsync(Stream fileStream, string extension, string contentType, string fileName)
        {
            //var objectName = $"uploads/{Path.GetRandomFileName().Replace(".", string.Empty)}.gz";
            string objectName;

            if (fileName != null && !string.IsNullOrEmpty(fileName))
                objectName = $"uploads/{fileName}{Path.GetRandomFileName().Replace(".", string.Empty)}.{extension}";
            else
                objectName = $"uploads/{Path.GetRandomFileName().Replace(".", string.Empty)}.{extension}";

            //var compressedStream = new MemoryStream();
            //var compressionStream = new GZipStream(compressedStream, CompressionLevel.NoCompression, true);

            //await fileStream.CopyToAsync(compressionStream);
            
            //compressionStream.Close();

            var putObjectRequest = new PutObjectRequest
            {
                BucketName = _awsS3Bucket,
                Key = objectName,
                InputStream = fileStream,
                ContentType = contentType,
                CannedACL = S3CannedACL.PublicRead
            };

            var putObjectResponse = await _s3Client.PutObjectAsync(putObjectRequest);
            
            return putObjectResponse.HttpStatusCode == HttpStatusCode.OK
                ? objectName
                : string.Empty;
        }

        public async Task<string> GetFileAsync(string fileUri)
        {
            var request = new GetObjectRequest
            {
                BucketName = _awsS3Bucket,
                Key = fileUri
            };

            var tempFilePath = Path.GetTempFileName();

            using var response = await _s3Client.GetObjectAsync(request);
            await using var tempFileStream = new FileStream(tempFilePath, FileMode.Create);
            await using var decompressionStream = new GZipStream(response.ResponseStream, CompressionMode.Decompress);
            await decompressionStream.CopyToAsync(tempFileStream);

            return tempFilePath;
        }
    }
}