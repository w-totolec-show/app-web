﻿using System;
using System.Linq;
using System.Reflection;
using Core.Entities;
using Core.Entities.AppLogs;
using Core.Services;
using Core.Utils;
using Microsoft.AspNetCore.DataProtection.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace Core.Infrastructure
{
    public class ApplicationDbContext :
        IdentityDbContext<AppUser, IdentityRole<Guid>, Guid>,
        IDataProtectionKeyContext
    {
        public DbSet<Log> Logs { get; set; }
        public DbSet<Draw> Draw { get; set; }
        public DbSet<Ticket> Ticket { get; set; }
        public DbSet<FileImport> FileImport { get; set; }
        public DbSet<Alert> Alert { get; set; }
        public DbSet<TermsOfService> TermsOfService { get; set; }
        public DbSet<CreditCardToken> CreditCardToken { get; set; }
        public DbSet<Messages> Messages { get; set; }
        
        public DbSet<DataProtectionKey> DataProtectionKeys { get; set; }
        
        public DbSet<QueueItem> QueueItem { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (optionsBuilder.IsConfigured)
                return;

            var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");

            var configurationBuilder = new ConfigurationBuilder();
            configurationBuilder.AddProjectConfigurations(environmentName);
            var configuration = configurationBuilder.Build();

            // PostgreSQL Connection
            var postgresConnectionString =
                string.Join(';',
                    $"Host={configuration["PostgreHost"]}",
                    $"Port={configuration["PostgrePort"]}",
                    $"Pooling={configuration["PostgrePooling"]}",
                    $"Database={configuration["PostgreDatabase"]}",
                    $"User Id={configuration["PostgreUserId"]}",
                    $"Password={configuration["PostgrePassword"]}");

            optionsBuilder.UseNpgsql(postgresConnectionString);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            //modelBuilder.ApplyConfiguration(new ...());
            modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetAssembly(typeof(ApplicationDbContext)));

            //Remove delete behaiors
            foreach (var relationship in modelBuilder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
                relationship.DeleteBehavior = DeleteBehavior.Restrict;

            // PostgreSQL snake case convention
            foreach (var entity in modelBuilder.Model.GetEntityTypes())
            {
                // Replace table names
                entity.SetTableName(entity.GetTableName().FromCamelToSnakeCase());

                // Replace column names
                foreach (var property in entity.GetProperties())
                    property.SetColumnName(property.GetColumnName().FromCamelToSnakeCase());

                // Replace keys names
                foreach (var key in entity.GetKeys())
                    key.SetName(key.GetName().FromCamelToSnakeCase());

                // Replace foreign keys names
                foreach (var key in entity.GetForeignKeys())
                    key.SetConstraintName(key.GetConstraintName().FromCamelToSnakeCase());

                // Replace indexes names
                foreach (var index in entity.GetIndexes())
                    index.SetName(index.GetName().FromCamelToSnakeCase());
            }
        }
    }
}