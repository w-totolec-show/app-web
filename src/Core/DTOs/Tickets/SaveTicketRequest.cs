using System;
using Core.Entities;

namespace Core.DTOs.Tickets
{
    public class SaveTicketRequest
    {
        public Guid Guid { get; set; }
        public Guid DrawGuid { get; set; }
        public string TicketNumber { get; set; }
        public string SimplifiedTicketNumber { get; set; }
        public string ExtractionNumber { get; set; }
        public int[] TicketBalls { get; set; }
        public string DrawYear { get; set; }
        
        public SaveTicketRequest(Guid drawGuid, string fileName, string line)
        {
            var extractionNumber = fileName.Substring(3, 3);
            var drawYear = fileName.Substring(6, 2);
            var extractionNumberTicket = line.Substring(50, 3);

            if (string.IsNullOrEmpty(line) || !extractionNumber.Equals(extractionNumberTicket))
            {
                throw new Exception("Invalid or inconsistent file");
            }

            DrawGuid = drawGuid;
            ExtractionNumber = extractionNumber;
            DrawYear = drawYear;
            TicketBalls = ParseTicketBalls(line.Substring(0, 40));
            SimplifiedTicketNumber = line.Substring(40, 7);
            TicketNumber = line.Substring(40, 14);
        }

        private static int[] ParseTicketBalls(string ballsNumbers)
        {
            var result = new int[20];

            for (var i = 0; i <= 39; i += 2)
            {
                var ballNumberAsString = ballsNumbers.Substring(i, 2);
                var ballNumber = int.Parse(ballNumberAsString);

                if (ballNumber <= 0 || ballNumber > 50)
                {
                    throw new Exception($"Invalid ball number: {ballNumber} in {ballsNumbers}");
                }

                result[i / 2] = ballNumber;
            }

            return result;
        }
    }
}