using System;

namespace Core.DTOs.Tickets
{
    public class ListTicketsRequest : Queryable
    {
        public Guid DrawGuid { get; set; }
        public int[] Balls { get; set; }

        public Guid? UserGuid { get; set; }
    }
}