using System;
using System.Collections.Generic;
using Core.Entities;

namespace Core.DTOs.Tickets
{
    public class PurchaseTicketRequest
    {
        public IList<string> TicketsGuid { get; set; }
        public Guid? SelectCreditCardGuid { get; set; }
        public AppUser User { get; set; }
        public string HashCard { get; set; }
        public string CardType { get; set; }
        public string CardNumber { get; set; }
        public string Holder { get; set; }
        public string ExpirationDate { get; set; }
        public string SecurityCode { get; set; }
    }
}