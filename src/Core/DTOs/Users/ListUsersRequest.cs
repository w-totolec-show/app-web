namespace Core.DTOs.Users
{
    public class ListUsersRequest : Queryable
    {
        public bool IsAdmin { get; set; }
    }
}