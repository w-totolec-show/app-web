﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Core.DTOs.Users
{
    public class SaveUserRequest
    {
        public Guid? Id { get; set; }

        public string UserName { get; set; }

        public string FirstName { get; set; }

        public string Lastname { get; set; }

        public string Email { get; set; }

        public string PhoneNumber { get; set; }

        public string Password { get; set; }
        
        public string SocialId { get; set; } // CPF

        public bool IsAdmin { get; set; }

        public string Observations { get; set; }

        public string City { get; set; }
        public string Locality { get; set; }
        public string AddressNumber { get; set; }
        public bool IsActive { get; set; }
        public bool CompleteRegistration { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string Zipcode { get; set; }
        public string[] Roles { get; set; }
        public Guid? UserModification { get; set; }
        
        public Dictionary<string, string> Data { get; set; }
    }
}