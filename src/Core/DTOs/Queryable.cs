namespace Core.DTOs
{
    public class Queryable
    {
        public int Page { get; set; } = 1;
        public int PageSize { get; set; } = 10;
        public string SearchTerm { get; set; }
        public string OrderBy { get; set; }
        public bool IsActive { get; set; } = true;
        public string ReturnToView { get; set; }
    }
}