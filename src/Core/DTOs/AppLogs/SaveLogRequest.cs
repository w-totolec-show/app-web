﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Core.Entities.AppLogs;

namespace Core.DTOs.AppLogs
{
    public class SaveLogRequest
    {
        [Required]
        public Guid Guid { get; set; }

        public Guid? UserGuid { get; set; }

        public LogType LogType { get; set; }

        public Guid UserModification { get; set; }

        [Column(TypeName = "jsonb")]
        public IList<PropertyLog> PropertyLogs { get; set; }
    }
}
