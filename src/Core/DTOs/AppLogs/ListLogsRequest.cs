﻿using System;
using Core.Entities.AppLogs;

namespace Core.DTOs.AppLogs
{
    public class ListLogsRequest : Queryable
    {
        public Guid userGuid { get; set; }
        public LogType Type { get; set; }
    }
}
