
using System.Collections.Generic;

namespace Core.DTOs
{
    public class PaginatedResult<T>
    {
        public IEnumerable<T> Results { get; set; }
    
        public int Count { get; set; }

        public PaginatedResult(IEnumerable<T> results, int count)
        {
            Results = results;
            Count = count;
        }
    }
}