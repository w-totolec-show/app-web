﻿using System.ComponentModel.DataAnnotations;

namespace Core.DTOs.Auth
{
    public class AuthenticationRequest
    {
        [Required]
        public string Username { get; set; }

        [Required]
        public string Password { get; set; }
    }
}