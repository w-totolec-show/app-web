using System;
using System.Collections.Generic;
using Core.Entities;

namespace Core.DTOs.Draws
{
    public class SaveDrawRequest
    {
        public Guid? Guid { get; set; }
        public int DrawNumber { get; set; }
        public string Name { get; set; }
        public IList<Ticket> Tickets { get; set; }
        public decimal ValueOfTicket { get; set; }
        public DateTime StartDatePurchase { get; set; }
        public DateTime EndDatePurchase { get; set; }
        public string OriginalLayoutName { get; set; }
        public string LayoutUri { get; set; }
        public DateTime DrawDate { get; set; }
        public bool Accomplished { get; set; }
        public string Observations { get; set; }
        public string UrlCommercial { get; set; }
        public string DescriptionOfDraw { get; set; }
        public string DescriptionOfResult { get; set; }
        public bool IsActive { get; set; }
        public IDictionary<string, string> Parameters { get; set; }
        public Guid? UserModification { get; set; }
    }
}