﻿using System;
using Core.Entities;
using Core.Infrastructure;
using Core.Services.Interfaces;
using Core.Utils;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Razor.TagHelpers;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using StackExchange.Redis;

namespace Core.Services
{
    public static class ServicesConfiguration
    {
        public static void Configure(this IServiceCollection serviceCollection, IConfiguration configuration)
        {
            // PostgreSQL Connection
            var postgresConnectionString = 
                string.Join(';',
                    $"Host={configuration["PostgreHost"]}",
                    $"Port={configuration["PostgrePort"]}",
                    $"Pooling={configuration["PostgrePooling"]}",
                    $"Database={configuration["PostgreDatabase"]}",
                    $"User Id={configuration["PostgreUserId"]}",
                    $"Password={configuration["PostgrePassword"]}");
            
            serviceCollection.AddDbContext<ApplicationDbContext>(
                options => options.UseNpgsql(postgresConnectionString));

            serviceCollection.AddDataProtection()
                .PersistKeysToDbContext<ApplicationDbContext>();

            // Redis
            serviceCollection.AddSingleton(sp =>
            {
                /*var redisUri = string.Concat(
                    configuration["RedisHost"],
                    ":",
                    configuration["RedisPort"],
                    ",password=",
                    configuration["RedisPassword"]
                );*/
                
                var redisUri = string.Concat(
                    configuration["RedisHost"],
                    ":",
                    configuration["RedisPort"]
                );

                return ConnectionMultiplexer.Connect(redisUri);
            });

            // Memory Cache
            serviceCollection.AddMemoryCache();
            
            //serviceCollection.AddTransient<IPasswordValidator<AppUser>, CustomPasswordPolicy>();

            // File Manager (S3, MinIO etc)
            //serviceCollection.AddScoped<IFileManager, MinioFileManager>();
            serviceCollection.AddScoped<IFileManager, S3FileManager>();

            // Email Utilities
            serviceCollection.AddScoped<EmailUtilities, EmailUtilities>();

            // Domain Services
            serviceCollection.AddScoped<IUsersService, UsersService>();
            serviceCollection.AddScoped<IDrawsService, DrawsService>();
            serviceCollection.AddScoped<ITicketsService, TicketsService>();
            serviceCollection.AddScoped<IFileImportsService, FileImportsService>();
            serviceCollection.AddScoped<IAlertsService, AlertsService>();
            serviceCollection.AddScoped<ITermsService, TermsService>();
            serviceCollection.AddScoped<IPaymentService, PaymentService>();
            serviceCollection.AddScoped<ICreditCardTokenizeService, CreditCardTokenizeService>();
            serviceCollection.AddScoped<IMessagesService, MessagesService>();
            serviceCollection.AddScoped<IQueueItemService, QueueItemService>();

            // Logs
            serviceCollection.AddScoped<IAppLogsService, AppLogsService>();
        }
    }
}