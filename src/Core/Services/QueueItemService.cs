using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Entities;
using Core.Infrastructure;
using Core.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using Type = Core.Entities.Type;
using System.Linq;

namespace Core.Services
{
    public class QueueItemService : IQueueItemService
    {
        private readonly ApplicationDbContext _applicationDbContext;

        public QueueItemService(ApplicationDbContext applicationDbContext)
        {
            _applicationDbContext = applicationDbContext;
        }
        
        public async Task<bool> SaveAsync(QueueItem queueItem)
        {
            var item = await _applicationDbContext.QueueItem
                .FirstOrDefaultAsync(d => d.Guid == queueItem.Guid);

            if (item == null)
            {
                _applicationDbContext.QueueItem.Add(queueItem);
            }

            return await _applicationDbContext.SaveChangesAsync() > 0;
        }

        public async Task<IList<QueueItem>> ListAsync(Type type, Guid drawGuid)
        {
            return await _applicationDbContext.QueueItem
                .Where(q => q.Type == type && q.DrawGuid == drawGuid)
                .ToListAsync();
        }

        public async Task<IList<QueueItem>> ListAwaitingProcessingAsync(Type type)
        {
            return await _applicationDbContext.QueueItem
                .Where(q => q.StatusQueueItem == StatusQueueItem.AwaitingProcessing && q.Type == type)
                .ToListAsync();
        }
    }
}