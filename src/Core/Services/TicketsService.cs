using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Core.DTOs;
using Core.DTOs.Tickets;
using Core.Entities;
using Core.Infrastructure;
using Core.Services.Interfaces;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using System.Text.RegularExpressions;
using DocumentFormat.OpenXml.Drawing.Diagrams;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json.Linq;
using RestSharp;
using ServiceStack;

namespace Core.Services
{
    public class TicketsService : ITicketsService
    {
        private readonly ApplicationDbContext _applicationDbContext;
        private readonly IServiceScopeFactory _serviceScopeFactory;
        private readonly IPaymentService _paymentService;
        private readonly ICreditCardTokenizeService _creditCardTokenizeService;

        public TicketsService(ApplicationDbContext applicationDbContext, 
            IConfiguration configuration,
            IPaymentService paymentService,
            ICreditCardTokenizeService creditCardTokenizeService,
            IServiceProvider iServiceProvider)
        {
            _applicationDbContext = applicationDbContext;
            _paymentService = paymentService;
            _creditCardTokenizeService = creditCardTokenizeService;
            _serviceScopeFactory = iServiceProvider.GetRequiredService<IServiceScopeFactory>();
        }

        public async Task<PaginatedResult<Ticket>> ListAsync(ListTicketsRequest request)
        {
            var q = _applicationDbContext.Ticket
                .Include(t => t.AppUser)
                .Where(t => t.DrawGuid == request.DrawGuid)
                .AsQueryable();
            
            if (!string.IsNullOrWhiteSpace(request.SearchTerm))
            {
                q = q.Where(t => t.Guid.ToString().Contains(request.SearchTerm)
                                 || t.ExtractionNumber.Contains(request.SearchTerm)
                                 || t.TicketNumber.Contains(request.SearchTerm)
                                 || t.SimplifiedTicketNumber.Contains(request.SearchTerm)
                                 || t.AppUser.SocialId.Contains(request.SearchTerm));
            }

            /*if (request.IsActive)
            {
                q = q.Where(t => t.Sold == request.IsActive);
                q = q.OrderByDescending(t => t.Sold == request.IsActive);
            }*/
            
            var countRes = await q.CountAsync();

            var res = await q
                .OrderByDescending(t => t.PurchaseDate.HasValue)
                .Skip((request.Page - 1) * request.PageSize)
                .Take(request.PageSize)
                .ToListAsync();

            return new PaginatedResult<Ticket>(res, countRes);
        }

        public async Task<PaginatedResult<Ticket>> ListSuggestionsAsync(ListTicketsRequest request)
        {
            var q = _applicationDbContext.Ticket
                .Include(t => t.Draw)
                .Where(t => t.DrawGuid == request.DrawGuid && t.Sold == false && t.IsActive)
                .AsQueryable();

            if (request.Balls != null && request.Balls.Any())
            {
                foreach (var ball in request.Balls)
                {
                    q = q.Where(t => t.TicketBalls.Contains(ball));
                }
            }
           
            var countRes = await q.CountAsync();

            var res = await q
                .Skip((request.Page - 1) * request.PageSize)
                .Take(request.PageSize)
                .ToListAsync();

            return new PaginatedResult<Ticket>(res, countRes);
        }

        public async Task<PaginatedResult<Ticket>> ListCustomerAsync(ListTicketsRequest request)
        {
            var q = _applicationDbContext.Ticket
                .Include(t => t.Draw)
                .Where(t => t.Sold && t.UserGuid == request.UserGuid)
                .AsQueryable();
            
            if (!string.IsNullOrWhiteSpace(request.SearchTerm))
            {
                q = q.Where(t => t.Guid.ToString().Contains(request.SearchTerm)
                                 || t.ExtractionNumber.Contains(request.SearchTerm)
                                 || t.TicketNumber.Contains(request.SearchTerm)
                                 || t.SimplifiedTicketNumber.Contains(request.SearchTerm)
                                 || t.IdTransaction.Contains(request.SearchTerm));
            }
            
            var countRes = await q.CountAsync();

            var res = await q
                .OrderByDescending(t => t.PurchaseDate)
                .Skip((request.Page - 1) * request.PageSize)
                .Take(request.PageSize)
                .ToListAsync();

            return new PaginatedResult<Ticket>(res, countRes);
        }

        public async Task<IList<Ticket>> ListCustomerAllAsync(Guid userGuid)
        {
            var q = _applicationDbContext.Ticket
                .Include(t => t.Draw)
                .Include(t => t.Draw.FileImports)
                .Where(t => t.Sold && t.UserGuid == userGuid)
                .AsQueryable();

            return await q.OrderByDescending(t => t.Draw.DrawDate).ToListAsync();
        }

        public async Task<Ticket> GetAsync(Guid guid)
        {
            return await _applicationDbContext.Ticket
                .Include(d => d.AppUser)
                .Include(t => t.Draw)
                .Include(t => t.Draw.FileImports)
                .FirstOrDefaultAsync(d => d.Guid == guid);
        }
        
        public async Task<bool> DeleteAllAsync(Guid guid)
        {
            var allTickets = _applicationDbContext.Ticket.Where(d => d.DrawGuid == guid);

            _applicationDbContext.Ticket.RemoveRange(allTickets);

            return await _applicationDbContext.SaveChangesAsync() > 0;
        }

        public async Task<PaginatedResult<Ticket>> ListSold(ListTicketsRequest request)
        {
            var q = _applicationDbContext.Ticket
                .Include(t => t.AppUser)
                .Where(t => t.Sold && t.DrawGuid ==  request.DrawGuid)
                .AsQueryable();
            
            var countRes = await q.CountAsync();

            var res = await q
                .OrderByDescending(t => t.PurchaseDate)
                .Skip((request.Page - 1) * request.PageSize)
                .Take(request.PageSize)
                .ToListAsync();
            
            return new PaginatedResult<Ticket>(res, countRes);
        }
        
        public async Task<IList<Ticket>> ListAllSold(Guid guidDraw)
        {
            var q = _applicationDbContext.Ticket
                .Include(t => t.AppUser)
                .Include(t => t.Draw)
                .Where(t => t.Sold && t.DrawGuid == guidDraw)
                .AsQueryable();

            return await q.ToListAsync();
        }

        public async Task<int> CountSold(Guid guidDraw)
        {
            var q = _applicationDbContext.Ticket
                .Where(t => t.DrawGuid == guidDraw && t.Sold);

            return await q.CountAsync();
        }

        public async Task<int> CountPending(Guid guidDraw)
        {
            var q = _applicationDbContext.Ticket
                .Where(t => t.DrawGuid == guidDraw && t.Sold == false);

            return await q.CountAsync();
        }

        public async Task<Ticket> SaveAsync(SaveTicketRequest request)
        {
            var ticket = await _applicationDbContext.Ticket
                .FirstOrDefaultAsync(d => d.Guid == request.Guid);

            if (ticket == null)
            {
                ticket = new Ticket
                {
                    Guid = new Guid(),
                    DrawGuid = request.DrawGuid,
                    TicketNumber = request.TicketNumber,
                    SimplifiedTicketNumber = request.SimplifiedTicketNumber,
                    ExtractionNumber = request.ExtractionNumber,
                    TicketBalls = request.TicketBalls,
                    DrawYear = request.DrawYear,
                    Sold = false,
                    IsActive = true,
                    CreatedAt = DateTime.UtcNow
                };

                _applicationDbContext.Ticket.Add(ticket);
            }

            return await _applicationDbContext.SaveChangesAsync() > 0 ? ticket : null;
        }
        
        public async Task<bool> SaveAsync(IList<SaveTicketRequest> saveTicketRequests)
        {
            using (var scope = _serviceScopeFactory.CreateScope())
            {
                var context = scope.ServiceProvider.GetService<ApplicationDbContext>();

                foreach (var saveTicketRequest in saveTicketRequests)
                {
                    var ticket = new Ticket
                    {
                        Guid = new Guid(),
                        DrawGuid = saveTicketRequest.DrawGuid,
                        TicketNumber = saveTicketRequest.TicketNumber,
                        SimplifiedTicketNumber = saveTicketRequest.SimplifiedTicketNumber,
                        ExtractionNumber = saveTicketRequest.ExtractionNumber,
                        TicketBalls = saveTicketRequest.TicketBalls,
                        DrawYear = saveTicketRequest.DrawYear,
                        Sold = false,
                        IsActive = true,
                        CreatedAt = DateTime.UtcNow
                    };
                
                    await context.Ticket.AddAsync(ticket);
                }

                var res = await context.SaveChangesAsync() > 0;
                
                saveTicketRequests.Clear();

                await context.DisposeAsync();
                scope.Dispose();

                return res;
            }
        }

        public async Task<bool> DisableAsync(Guid guid, bool isActive)
        {
            using (var scope = _serviceScopeFactory.CreateScope())
            {
                var context = scope.ServiceProvider.GetService<ApplicationDbContext>();
                
                var res = await context.Ticket
                    .FirstOrDefaultAsync(t => t.Guid == guid);
            
                res.IsActive = isActive;
                
                var result = await context.SaveChangesAsync() > 0;

                await context.DisposeAsync();
                scope.Dispose();

                return result;
            }
        }
        
        public async Task<bool> ReturnForSaleAsync(Guid guid)
        {
            using (var scope = _serviceScopeFactory.CreateScope())
            {
                var context = scope.ServiceProvider.GetService<ApplicationDbContext>();
                
                var res = await context.Ticket
                    .FirstOrDefaultAsync(t => t.Guid == guid);
            
                res.Sold = false;
                
                var result = await context.SaveChangesAsync() > 0;

                await context.DisposeAsync();
                scope.Dispose();

                return result;
            }
        }

        public async Task<(string, string, IList<string>)> PurchaseAsync(PurchaseTicketRequest request)
        {
            // Bloquear venda das cartelas selecionadas
            List<bool> resultBlock = new List<bool>();
            StringBuilder ticketsSold = new StringBuilder();
            List<Ticket> tickets = new List<Ticket>();
            foreach (var guidTicket in request.TicketsGuid)
            {
                try
                {
                    var guid = new Guid(guidTicket);
                    
                    var res =await _applicationDbContext.Ticket
                        .Include(t => t.Draw)
                        .FirstOrDefaultAsync(d => 
                            d.Guid == guid
                            && d.Sold == false);
                
                    if (res == null)
                    {
                        ticketsSold.Append($"{guidTicket}&");
                        resultBlock.Add(false);
                    }
                    else
                    {
                        res.Sold = true;

                        var result = await _applicationDbContext.SaveChangesAsync() > 0;
                
                        if(result)
                            tickets.Add(res);
                
                        resultBlock.Add(result);
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine($"{e.Message} - {e.StackTrace}");
                }
            }

            if (!resultBlock.Contains(false))
            {
                try
                {
                    var token = await _paymentService.GetTokenAuthorizationAsync();

                    var userIdAxis = "";
                    if (string.IsNullOrEmpty(request.User.AxisUserId))
                    {
                        var (item1, item2) = await _paymentService.GenerateUserIdAxis(request.User, token);

                        if (item1.Equals("ERROR"))
                        {
                            await ReturnTicketsForSale(tickets);
                            return ("ERROR", item2, null);
                        }

                        userIdAxis = item1;
                    }
                    else
                    {
                        userIdAxis = request.User.AxisUserId;
                    }
                    
                    if (string.IsNullOrEmpty(request.User.AxisAddressId) && userIdAxis != "ERROR")
                    {
                        await _paymentService.GenerateAddressIdAxis(request.User, token, userIdAxis);
                    }
                   
                    var cardIdAxis = "";
                    if (!request.SelectCreditCardGuid.HasValue)
                    {
                        var (item1, item2) = await _paymentService.TokenizeCreditCard(request, token, userIdAxis);

                        if (item1.Equals("ERROR"))
                        {
                            await ReturnTicketsForSale(tickets);
                            return ("ERROR", item2, null);
                        }

                        cardIdAxis = item1;
                    }
                    else
                    {
                        var resCreditCard = await _creditCardTokenizeService.GetAsync(request.SelectCreditCardGuid.Value);
                        cardIdAxis = resCreditCard.Token;
                    }
                    
                    var fullPayment = await _paymentService.FullPaymentAsync(token, userIdAxis, cardIdAxis, tickets, request.User.Id);
                        
                    if (fullPayment.Item1 == "2") // Autorizado
                    {
                        var associateTickets = await AssociateTicketsForUser(tickets, request.User, fullPayment.Item2);
                        if (associateTickets)
                        {
                            if (!request.SelectCreditCardGuid.HasValue)
                            {
                                var creditCard = new CreditCardToken
                                {
                                    Guid = new Guid(),
                                    UserGuid = request.User.Id,
                                    Token = cardIdAxis,
                                    Brand = PaymentService.FindCardType(request.CardNumber.Replace(" ", "")).ToString(),
                                    LastFourNumbers = request.CardNumber.Substring(request.CardNumber.Length - 4),
                                    CreatedAt = DateTime.UtcNow
                                };
                                
                                await _creditCardTokenizeService.SaveAsync(creditCard);
                            }
                            
                            return (fullPayment.Item1, null, null);
                        }
                    }
                        
                    await ReturnTicketsForSale(tickets);
                        
                    return (fullPayment.Item1, fullPayment.Item2, null);
                }
                catch (Exception e)
                {
                    await ReturnTicketsForSale(tickets);
                    Console.WriteLine($"{e.Message} - {e.StackTrace}");
                    return ("ERROR_SYSTEM", e.Message, null);
                }
            }
            
            await ReturnTicketsForSale(tickets);

            return ("TICKETS_SOLD", ticketsSold.ToString(), request.TicketsGuid);
        }

        private async Task<bool> AssociateTicketsForUser(List<Ticket> tickets, AppUser user, string idTransaction)
        {
            foreach (var ticket in tickets)
            {
                ticket.UserGuid = user.Id;
                ticket.PurchaseDate = DateTime.UtcNow;
                ticket.IdTransaction = idTransaction;
                ticket.GatewayPayment = "Axis";
            }

            return await _applicationDbContext.SaveChangesAsync() > 0;
        }
        
        private async Task<bool> ReturnTicketsForSale(List<Ticket> tickets)
        {
            foreach (var ticket in tickets)
            {
                using var scope = _serviceScopeFactory.CreateScope();
                var context = scope.ServiceProvider.GetService<ApplicationDbContext>();
                
                var res = await context.Ticket
                    .FirstOrDefaultAsync(t => t.Guid == ticket.Guid);
            
                res.Sold = false;
                
                var result = await context.SaveChangesAsync() > 0;

                await context.DisposeAsync();
                scope.Dispose();
            }

            return true;
        }
    }
}