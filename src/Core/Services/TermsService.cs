using System.Threading.Tasks;
using Core.Entities;
using Core.Infrastructure;
using Core.Services.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Core.Services
{
    public class TermsService : ITermsService
    {
        private readonly ApplicationDbContext _applicationDbContext;
        
        public TermsService(ApplicationDbContext applicationDbContext)
        {
            _applicationDbContext = applicationDbContext;
        }
        
        public async Task<bool> SaveAsync(TermsOfService terms)
        {
            var termsDelete = _applicationDbContext.TermsOfService;

            _applicationDbContext.RemoveRange(termsDelete);

            _applicationDbContext.TermsOfService.Add(terms);

            return await _applicationDbContext.SaveChangesAsync() > 0;
        }

        public async Task<TermsOfService> GetAsync()
        {
            return await _applicationDbContext.TermsOfService
                .FirstOrDefaultAsync();
        }
    }
}