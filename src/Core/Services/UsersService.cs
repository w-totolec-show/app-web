﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Core.DTOs.Users;
using Core.Entities;
using Core.Infrastructure;
using Core.Services.Interfaces;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System.Linq.Dynamic.Core;
using Core.DTOs;
using Core.DTOs.AppLogs;
using Core.Entities.AppLogs;

namespace Core.Services
{
    public class UsersService : IUsersService
    {
        private readonly ApplicationDbContext _applicationDbContext;
        private readonly RoleManager<IdentityRole<Guid>> _roleManager;
        private readonly UserManager<AppUser> _userManager;
        private readonly IAppLogsService _appLogsService;

        public UsersService(
            ApplicationDbContext applicationApplicationDbContext,
            UserManager<AppUser> userManager,
            RoleManager<IdentityRole<Guid>> roleManager,
            IAppLogsService appLogsService)
        {
            _userManager = userManager;
            _roleManager = roleManager;
            _appLogsService = appLogsService;
            _applicationDbContext = applicationApplicationDbContext;
        }

        public async Task<PaginatedResult<AppUser>> ListAsync(ListUsersRequest request)
        {
            var q = _applicationDbContext.Users
                .Where(u => u.IsAdmin == request.IsAdmin)
                .AsQueryable();

            if (!string.IsNullOrWhiteSpace(request.SearchTerm))
            {
                q = q.Where(u =>
                    u.Id.ToString().Contains(request.SearchTerm)
                    || u.FirstName.ToLower().Contains(request.SearchTerm)
                    || u.LastName.ToLower().Contains(request.SearchTerm)
                    || u.UserName.ToLower().Contains(request.SearchTerm)
                    || u.Email.ToLower().Contains(request.SearchTerm)
                    || u.SocialId.ToLower().Contains(request.SearchTerm)
                    || u.PhoneNumber.ToLower().Contains(request.SearchTerm)
                    || u.FirstName.Contains(request.SearchTerm)
                    || u.LastName.Contains(request.SearchTerm)
                    || u.UserName.Contains(request.SearchTerm)
                    || u.Email.Contains(request.SearchTerm)
                    || u.SocialId.Contains(request.SearchTerm)
                    || u.PhoneNumber.Contains(request.SearchTerm)
                    || u.FirstName.ToUpper().Contains(request.SearchTerm)
                    || u.LastName.ToUpper().Contains(request.SearchTerm)
                    || u.UserName.ToUpper().Contains(request.SearchTerm)
                    || u.Email.ToUpper().Contains(request.SearchTerm)
                    || u.SocialId.ToUpper().Contains(request.SearchTerm)
                    || u.PhoneNumber.ToUpper().Contains(request.SearchTerm)
                );
            }

            if (!request.IsActive)
                q = q.Where(u => u.IsActive == request.IsActive);

            var countRes = await q.CountAsync();

            var res = await q
                .OrderByDescending(u => u.CreatedAt)
                .Skip((request.Page - 1) * request.PageSize)
                .Take(request.PageSize)
                .ToListAsync();

            return new PaginatedResult<AppUser>(res, countRes);
        }

        public async Task<IList<AppUser>> ListAllAsync(bool isAdmin)
        {
            var q = _applicationDbContext.Users
                .Where(u => u.IsAdmin == isAdmin)
                .AsQueryable();
            
            return await q.ToListAsync();
        }
       
        public async Task<IList<IdentityRole<Guid>>> ListAllRolesAsync()
        {
            return await _roleManager.Roles.ToListAsync();
        }

        public async Task<IEnumerable<string>> ListUserRolesAsync(Guid userGuid)
        {
            return await _userManager.GetRolesAsync(new AppUser {Id = userGuid});
        }

        public async Task<(AppUser, IList<string>)> GetAsync(Guid id)
        {
            var res = await _applicationDbContext.Users
                .FirstOrDefaultAsync(u => u.Id == id);
            
            if (res != null)
            {
                var rolesAsync = await _userManager.GetRolesAsync(res);

                return (res, rolesAsync);
            }

            return (res, null);
        }

        public async Task<AppUser> SaveAsync(SaveUserRequest request)
        {
            var appUser = await _applicationDbContext.Users
                .FirstOrDefaultAsync(u => u.Id == request.Id);

            if (appUser == null)
            {
                appUser = new AppUser
                {
                    UserName = request.UserName,
                    FirstName = request.FirstName,
                    LastName = request.Lastname,
                    Email = request.Email,
                    SocialId = request.SocialId,
                    PhoneNumber = request.PhoneNumber,
                    Data = request.Data,
                    Observations = request.Observations,
                    CompleteRegistration = request.CompleteRegistration,
                    DateOfBirth = request.DateOfBirth,
                    Zipcode = request.Zipcode,
                    CreatedAt = DateTime.UtcNow,
                    IsActive = true,
                    IsAdmin = true,
                    City = request.City,
                    Locality = request.Locality,
                    AddressNumber = request.AddressNumber,
                    SecurityStamp = Guid.NewGuid().ToString()
                };

                if (!appUser.IsValid())
                    throw new Exception("Invalid user");

                await _userManager.CreateAsync(appUser, request.Password);
                await _userManager.AddToRolesAsync(appUser, request.Roles);

                return appUser;
            }

            var compare = CompareObjects(appUser, request);
            
            appUser.FirstName = request.FirstName;
            appUser.LastName = request.Lastname;
            appUser.UserName = request.UserName;
            appUser.Email = request.Email;
            appUser.SocialId = request.SocialId;
            appUser.DateOfBirth = request.DateOfBirth;
            appUser.Zipcode = request.Zipcode;
            appUser.CompleteRegistration = request.CompleteRegistration;
            appUser.PhoneNumber = request.PhoneNumber;
            appUser.Observations = request.Observations;
            appUser.Data = request.Data;
            appUser.IsActive = request.IsActive;
            appUser.IsAdmin = request.IsAdmin;
            appUser.ModifiedAt = DateTime.UtcNow;
            appUser.City = request.City;
            appUser.Locality = request.Locality;
            appUser.AddressNumber = request.AddressNumber;
            
            var resUpdateUser = await _applicationDbContext.SaveChangesAsync() > 0;

            // Remove and update roles of user
            var userRoles = await _userManager.GetRolesAsync(appUser);
            await _userManager.RemoveFromRolesAsync(appUser, userRoles);
            var resAddToRoles = await _userManager.AddToRolesAsync(appUser, request.Roles);
            
            var compareRoles = CompareRoles(userRoles, request.Roles);
            
            if (resUpdateUser && resAddToRoles.Succeeded)
            {
                if (request.UserModification != null)
                {
                    if (compareRoles != null)
                    {
                        foreach (var item in compareRoles)
                        {
                            compare.Add(item);
                        }
                    }
                    
                    var logRequest = new SaveLogRequest
                    {
                        LogType = LogType.Users,
                        UserGuid = appUser.Id,
                        PropertyLogs = compare,
                        UserModification = request.UserModification.Value
                    };

                    await _appLogsService.SaveAsync(logRequest);
                }
            }
            
            // Reset password
            if (request.Password != null)
            {
                var code = await _userManager.GeneratePasswordResetTokenAsync(appUser);
                await _userManager.ResetPasswordAsync(appUser, code, request.Password);
            }

            if (resUpdateUser && resAddToRoles.Succeeded)
                return appUser;

            throw new Exception($"An error occurred while trying to save the user {appUser.UserName}");
        }

        public async Task<bool> DisableAsync(Guid id)
        {
            var user = await _applicationDbContext.Users
                .FirstOrDefaultAsync(u => u.Id == id);

            if (user == null) throw new Exception($"No user found, identifier {id}");

            user.IsActive = !user.IsActive;

            await _userManager.SetLockoutEnabledAsync(user, !user.IsActive);
            await _userManager.SetLockoutEndDateAsync(user, DateTimeOffset.Now.AddYears(10));

            return await _applicationDbContext.SaveChangesAsync() > 0;
        }
        
        public async Task<bool> DeleteAsync(Guid id)
        {
            var user = await _applicationDbContext.Users
                .FirstOrDefaultAsync(u => u.Id == id);

            if (user == null) throw new Exception($"No user found, identifier {id}");
            
            var ret = await _userManager.RemoveFromRolesAsync(user, new string[] {"client"});

            _applicationDbContext.Users.Remove(user);

            return await _applicationDbContext.SaveChangesAsync() > 0;
        }
        
        public async Task<bool> SaveUserIdAxis(Guid id, string axisUserId)
        {
            var user = await _applicationDbContext.Users
                .FirstOrDefaultAsync(u => u.Id == id);

            if (user == null) throw new Exception($"No user found, identifier {id}");

            user.AxisUserId = axisUserId;

            return await _applicationDbContext.SaveChangesAsync() > 0;
        }
        
        public async Task<bool> SaveAddressIdAxis(Guid id, string addressIdAxis)
        {
            var user = await _applicationDbContext.Users
                .FirstOrDefaultAsync(u => u.Id == id);

            if (user == null) throw new Exception($"No user found, identifier {id}");

            user.AxisAddressId = addressIdAxis;

            return await _applicationDbContext.SaveChangesAsync() > 0;
        }
        
        public async Task<bool> DisableAsyncTest()
        {
            var users = await _applicationDbContext.Users
                .Where(u => u.CompleteRegistration)
                .ToListAsync();

            foreach (var item in users)
            {
                item.CompleteRegistration = false;
                await _applicationDbContext.SaveChangesAsync();
            }

            return true;
        }

        private IList<PropertyLog> CompareRoles(IList<string> before, string[] after)
        {
            var list = new List<PropertyLog>();
            List<string> listAfter = new List<string>(after);

            if (listAfter.Except(before).ToList().Count > 0 || before.Except(listAfter).ToList().Count > 0)
            {
                var propertyLog = new PropertyLog
                {
                    LogLevel = 1,
                    Before = string.Join(", ", before),
                    After = string.Join(", ", after),
                    Property = "Permissões",
                    CreatedAt = DateTime.UtcNow
                };
                
                list.Add(propertyLog);
            }
            
            return list;
        }
        
        private IList<PropertyLog> CompareObjects(AppUser before, SaveUserRequest after)
        {
            var list = new List<PropertyLog>();
            if (before.FirstName != after.FirstName)
            {
                var propertyLog = new PropertyLog
                {
                    LogLevel = 1,
                    Before = before.FirstName,
                    After = after.FirstName,
                    Property = "Primeiro nome",
                    CreatedAt = DateTime.UtcNow
                };
                
                list.Add(propertyLog);
            }
            
            if (before.LastName != after.Lastname)
            {
                var propertyLog = new PropertyLog
                {
                    LogLevel = 1,
                    Before = before.LastName,
                    After = after.Lastname,
                    Property = "Sobrenome",
                    CreatedAt = DateTime.UtcNow
                };
                
                list.Add(propertyLog);
            }
            
            if (before.Email != after.Email)
            {
                var propertyLog = new PropertyLog
                {
                    LogLevel = 1,
                    Before = before.Email,
                    After = after.Email,
                    Property = "E-mail",
                    CreatedAt = DateTime.UtcNow
                };
                
                list.Add(propertyLog);
            }
            
            if (after.Password != null)
            {
                var propertyLog = new PropertyLog
                {
                    LogLevel = 1,
                    Before = "Campo não visível",
                    After = "Campo não visível",
                    Property = "Redefinição de senha",
                    CreatedAt = DateTime.UtcNow
                };
                
                list.Add(propertyLog);
            }
            
            if (before.PhoneNumber != after.PhoneNumber)
            {
                var propertyLog = new PropertyLog
                {
                    LogLevel = 1,
                    Before = before.PhoneNumber,
                    After = after.PhoneNumber,
                    Property = "Celular",
                    CreatedAt = DateTime.UtcNow
                };
                
                list.Add(propertyLog);
            }
            
            if (before.SocialId != after.SocialId)
            {
                var propertyLog = new PropertyLog
                {
                    LogLevel = 1,
                    Before = before.SocialId,
                    After = after.SocialId,
                    Property = "CPF",
                    CreatedAt = DateTime.UtcNow
                };
                
                list.Add(propertyLog);
            }
            
            if (before.Observations != after.Observations)
            {
                var propertyLog = new PropertyLog
                {
                    LogLevel = 1,
                    Before = before.Observations,
                    After = after.Observations,
                    Property = "Observações",
                    CreatedAt = DateTime.UtcNow
                };
                
                list.Add(propertyLog);
            }
            
            if (before.CompleteRegistration != after.CompleteRegistration)
            {
                var propertyLog = new PropertyLog
                {
                    LogLevel = 1,
                    Before = before.CompleteRegistration.ToString(),
                    After = after.CompleteRegistration.ToString(),
                    Property = "Cadastro completo?",
                    CreatedAt = DateTime.UtcNow
                };
                
                list.Add(propertyLog);
            }
            
            if (before.Zipcode != after.Zipcode)
            {
                var propertyLog = new PropertyLog
                {
                    LogLevel = 1,
                    Before = before.Zipcode,
                    After = after.Zipcode,
                    Property = "CEP",
                    CreatedAt = DateTime.UtcNow
                };
                
                list.Add(propertyLog);
            }
            
            if (before.DateOfBirth != after.DateOfBirth)
            {
                var propertyLog = new PropertyLog
                {
                    LogLevel = 1,
                    Before = before.DateOfBirth.ToString("dd/MM/yyyy"),
                    After = after.DateOfBirth.ToString("dd/MM/yyyy"),
                    Property = "Data de nascimento",
                    CreatedAt = DateTime.UtcNow
                };
                
                list.Add(propertyLog);
            }

            return list;
        }
    }
}