using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Core.Entities;
using Core.Infrastructure;
using Core.Services.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Core.Services
{
    public class CreditCardTokenizeService : ICreditCardTokenizeService
    {
        private readonly ApplicationDbContext _applicationDbContext;

        public CreditCardTokenizeService(ApplicationDbContext applicationDbContext)
        {
            _applicationDbContext = applicationDbContext;
        }
        
        public async Task<bool> SaveAsync(CreditCardToken creditCardToken)
        {
            var res = await _applicationDbContext.CreditCardToken
                .FirstOrDefaultAsync(d => d.Guid == creditCardToken.Guid);

            if (res == null)
            {
                _applicationDbContext.CreditCardToken.Add(creditCardToken);
            }

            return await _applicationDbContext.SaveChangesAsync() > 0;
        }

        public async Task<IList<CreditCardToken>> ListAsync(Guid userGuid)
        {
            var q = _applicationDbContext.CreditCardToken
                .Where(c => c.UserGuid == userGuid)
                .AsQueryable();

            return await q.ToListAsync();
        }

        public async Task<CreditCardToken> GetAsync(Guid guid)
        {
            return await _applicationDbContext.CreditCardToken
                .Where(c => c.Guid == guid)
                .FirstOrDefaultAsync();
        }

        public async Task<bool> DeleteAsync(Guid guid)
        {
            var creditCard = _applicationDbContext.CreditCardToken
                .Where(c => c.Guid == guid);

            _applicationDbContext.RemoveRange(creditCard);
            
            return await _applicationDbContext.SaveChangesAsync() > 0;
        }
    }
}