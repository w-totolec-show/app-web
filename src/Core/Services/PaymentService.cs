using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Core.DTOs.Tickets;
using Core.Entities;
using Core.Services.Interfaces;
using Core.Utils;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json.Linq;
using RestSharp;
using ServiceStack;
using ServiceStack.Text;
using StackExchange.Redis;

namespace Core.Services
{
    public class PaymentService : IPaymentService
    {
        private readonly ConnectionMultiplexer _connectionMultiplexer;
        private const string PasswordPhrase = "U!>o{frnRCZ4!i>Ie$FT)D&x^";
        private readonly string _authorizationServerAxis;
        private readonly string _enterpriseId;
        private readonly string _shopId;
        private readonly string _secretId;
        private readonly string _urlFullPaymentAxis;
        private readonly string _urlCreateUserAxis;
        private readonly string _urlCreateAddressAxis;
        private readonly string _urlTokenizeCreditCard;
        private readonly IUsersService _usersService;
        
        public PaymentService(IConfiguration configuration, IUsersService usersService, ConnectionMultiplexer connectionMultiplexer)
        {
            _connectionMultiplexer = connectionMultiplexer;
            _authorizationServerAxis = configuration["AuthorizationServerAxis"];
            _enterpriseId = configuration["EnterpriseId"];
            _shopId = configuration["ShopId"];
            _secretId = configuration["SecretId"];
            _urlFullPaymentAxis = configuration["UrlFullPaymentAxis"];
            _urlCreateUserAxis = configuration["UrlCreateUserAxis"];
            _urlCreateAddressAxis = configuration["UrlCreateAddressAxis"];
            _urlTokenizeCreditCard = configuration["TokenizeCreditCardAxis"];
            _usersService = usersService;
        }

        public async Task<string> GetTokenAuthorizationAsync()
        {
            var db = _connectionMultiplexer.GetDatabase();
            var key = $"ACCESS_TOKEN_AXIS_PRD";
            var accessToken = await db.StringGetAsync(key);

            if (accessToken.HasValue)
            {
                var decryptedString = StringCipher.Decrypt(accessToken.ToString(), PasswordPhrase);
                return decryptedString;
            }

            var client = new RestClient(_authorizationServerAxis);
            
            var request = new RestRequest(Method.POST);
            request.AddHeader("X-Enterprise-Id", _enterpriseId);
            request.AddHeader("Content-Type", "application/json");

            var objRequest = new
            {
                shopId = _shopId,
                secret = _secretId
            };

            request.AddJsonBody(objRequest);

            var res = await client.PostAsync<Dictionary<string, object>>(request);
            var accessTokenAxis = res["token"].ToString();
            
            var encryptedString = StringCipher.Encrypt(accessTokenAxis, PasswordPhrase);
            
            await db.StringSetAsync(key, encryptedString, TimeSpan.FromMinutes(59));

            return accessTokenAxis;
        }

        public async Task<Tuple<string, string>> FullPaymentAsync(string token, string userIdAxis, string cardIdAxis, IList<Ticket> tickets, Guid userId)
        {
            var client = new RestClient(_urlFullPaymentAxis);
            var request = new RestRequest(Method.POST);
            
            var products = new List<object>();
            foreach (var ticket in tickets)
            {
                var product = new
                {
                    title = $"Cartela: {ticket.TicketNumber}",
                    valueInCents = ticket.Draw.ValueOfTicket * 100
                };
                
                products.Add(product);
            }

            var fullValueInCents = (tickets.Count * tickets.First().Draw.ValueOfTicket) * 100;
            
            var paymentDetails = new
            {
                uniqueClientReferenceId = $"{userId}-{DateTime.UtcNow.ToUnixTime()}",
                products,
                fullValueInCents,
                customerTaxesInCents = 0
            };
            
            var requestBody = new
            {
                userId = userIdAxis,
                cardId = cardIdAxis,
                paymentDetails
            };
            
            request.AddHeader("Authorization", $"Bearer {token}");
            request.AddHeader("Content-Type", "application/json");
            request.AddJsonBody(requestBody);
            
            var res = await client.PostAsync<PaymentChange>(request);

            if (res.success) return new Tuple<string, string>(res.data.paymentStatus, res.data.paymentId);
            
            var errorCode = res.data?.paymentStatus ?? "ERROR";
            
            return new Tuple<string, string>(errorCode, $"{res.title} - {res.message}");
        }

        public async Task<Tuple<string, string>> TokenizeCreditCard(PurchaseTicketRequest requestCard, string token, string userIdAxis)
        {
            try
            {
                var client = new RestClient(_urlTokenizeCreditCard);
                var request = new RestRequest(Method.POST);

                var expirationDateCard = new List<string>();
                var yearExpirationDateCard = "";
                if (requestCard.ExpirationDate.Contains("/"))
                {
                    expirationDateCard = requestCard.ExpirationDate.Replace(" ", "").Split("/").ToList();
                    yearExpirationDateCard = expirationDateCard[1].Substring(expirationDateCard[1].Length - 2);
                }
                else
                {
                    var requestDate = requestCard.ExpirationDate.Replace(" ", "");
                    var moth = requestDate.Substring(0, 2);
                    expirationDateCard.Add(moth);
                    yearExpirationDateCard = requestDate.Substring(requestDate.Length - 2);
                }
                
                var cardNumber = requestCard.CardNumber.Replace(" ", "");
            
                var requestBody = new
                {
                    userId = userIdAxis,
                    cardNumber,
                    nameOnCard = requestCard.Holder,
                    cardBrand = FindCardType(cardNumber).ToString(),
                    dueDate = $"20{yearExpirationDateCard}-{expirationDateCard[0]}-01",
                    securityCode = requestCard.SecurityCode
                };
            
                request.AddHeader("Authorization", $"Bearer {token}");
                request.AddHeader("Content-Type", "application/json");
                request.AddJsonBody(requestBody);
            
                var res = await client.PostAsync<TokenizeCreditCard>(request);
            
                if (!res.success) return new Tuple<string, string>("ERROR", $"{res.title} - {res.message}");
            
                return new Tuple<string, string>(res.data.cardId, null);

            }
            catch (Exception e)
            {
                Console.WriteLine($"{e.Message} - {e.StackTrace}");
                
                return new Tuple<string, string>("ERROR", "Erro na tokenização do cartão");
            }
        }

        public async Task<Tuple<string, string>> GenerateUserIdAxis(AppUser user, string token)
        {
            var client = new RestClient($"{_urlCreateUserAxis}");
            var request = new RestRequest(Method.POST);

            var requestBody = new
            {
                fullName = $"{user.FirstName} {user.LastName}",
                documentNumber = user.SocialId,
                birthDate = user.DateOfBirth.ToString("yyyy-MM-dd"),
                email = user.Email,
                phoneNumber = user.PhoneNumber
            };
            
            request.AddHeader("Authorization", $"Bearer {token}");
            request.AddHeader("Content-Type", "application/json");
            request.AddJsonBody(requestBody);
            
            var res = await client.PostAsync<UserAxis>(request);
            
            if (!res.success) return new Tuple<string, string>("ERROR", $"{res.title} - {res.message}");

            await _usersService.SaveUserIdAxis(user.Id, res.data.userId);
            
            return new Tuple<string, string>(res.data.userId, null);
        }
        
        public async Task<string> GenerateAddressIdAxis(AppUser user, string token, string userIdAxis)
        {
            var client = new RestClient($"{_urlCreateAddressAxis}/{userIdAxis}/address");
            var request = new RestRequest(Method.POST);

            var getAddress = await GetAddressUser(user);
            
            var requestBody = new
            {
                postalCode = getAddress.Zipcode,
                address = getAddress.Line1,
                number = getAddress.Number,
                district = getAddress.Neighborhood,
                city = getAddress.City,
                state = "CE",
            };
            
            request.AddHeader("Authorization", $"Bearer {token}");
            request.AddHeader("Content-Type", "application/json");
            request.AddJsonBody(requestBody);
            
            var res = await client.PostAsync<AddressAxis>(request);
            
            if (!res.success) return "ERROR";

            await _usersService.SaveAddressIdAxis(user.Id, res.data.addressId);
            
            return res.data.addressId;
        }

        public async Task<string> GetPaymentAsync(string token, string idTransaction)
        {
            var client = new RestClient($"https://checkout.axis-mobfintech.com/api/1/payments/{idTransaction}/status");
            var request = new RestRequest(Method.GET);
            
            request.AddHeader("Authorization", $"Bearer {token}");
            request.AddHeader("Content-Type", "application/json");
            
            var res = await client.GetAsync<object>(request);
            dynamic data = JObject.Parse(res.ToJson());

            return data.paymentStatusId;
        }

        private async Task<Address> GetAddressUser(AppUser user)
        {
            string city = "";
            string neighborhood = "";
            string street = "";
            string number = "";

            dynamic data = null;
            try
            {
                var client = new RestClient("https://viacep.com.br");
                var requestApi = new RestRequest($"ws/{user.Zipcode}/json/", Method.GET);
                var queryResult = await client.ExecuteAsync<IRestResponse>(requestApi);
                var requestResult = queryResult.Content;
        
                if (!string.IsNullOrEmpty(requestResult))
                    data = JObject.Parse(requestResult);
                        
                if (data != null && !string.IsNullOrEmpty(data.ToString()))
                {
                    city = data.localidade.ToString();
                    neighborhood = data.bairro.ToString();
                    street = data.logradouro.ToString();
                }
            }
            catch (Exception e)
            {
                street = "Rua 24 de Maio";
                neighborhood = "Centro";
                city = "Fortaleza";
                        
                Console.WriteLine(e);
            }
                    
            number = !string.IsNullOrEmpty(user.AddressNumber) ? user.AddressNumber : "955";
                    
            if (string.IsNullOrEmpty(city) || string.IsNullOrEmpty(neighborhood) || string.IsNullOrEmpty(street))
            {
                street = "Rua 24 de Maio";
                neighborhood = "Centro";
                city = "Fortaleza";
            }

            //var zipCode = user.Zipcode.Replace("-", "");

            var res = new Address
            {
                City = city,
                Neighborhood = neighborhood,
                Line1 = street,
                Number = number,
                Zipcode = user.Zipcode
            };

            return res;
        }
        
        public static CardType FindCardType(string cardNumber)
        {
            //https://www.regular-expressions.info/creditcard.html
            if (Regex.Match(cardNumber, @"^4[0-9]{12}(?:[0-9]{3})?$").Success)
            {
                return CardType.VISA;
            }

            if (Regex.Match(cardNumber, @"^(?:5[1-5][0-9]{2}|222[1-9]|22[3-9][0-9]|2[3-6][0-9]{2}|27[01][0-9]|2720)[0-9]{12}$").Success)
            {
                return CardType.MASTERCARD;
            }

            if (Regex.Match(cardNumber, @"^3[47][0-9]{13}$").Success)
            {
                return CardType.AMEX;
            }

            if (Regex.Match(cardNumber, @"^6(?:011|5[0-9]{2})[0-9]{12}$").Success)
            {
                return CardType.Discover;
            }

            if (Regex.Match(cardNumber, @"^(?:2131|1800|35\d{3})\d{11}$").Success)
            {
                return CardType.JCB;
            }
            
            if (Regex.Match(cardNumber, @"^4011(78|79)|^43(1274|8935)|^45(1416|7393|763(1|2))|^504175|^627780|^63(6297|6368|6369)|(65003[5-9]|65004[0-9]|65005[01])|(65040[5-9]|6504[1-3][0-9])|(65048[5-9]|65049[0-9]|6505[0-2][0-9]|65053[0-8])|(65054[1-9]|6505[5-8][0-9]|65059[0-8])|(65070[0-9]|65071[0-8])|(65072[0-7])|(65090[1-9]|6509[1-6][0-9]|65097[0-8])|(65165[2-9]|6516[67][0-9])|(65500[0-9]|65501[0-9])|(65502[1-9]|6550[34][0-9]|65505[0-8])|^(506699|5067[0-6][0-9]|50677[0-8])|^(509[0-8][0-9]{2}|5099[0-8][0-9]|50999[0-9])|^65003[1-3]|^(65003[5-9]|65004\d|65005[0-1])|^(65040[5-9]|6504[1-3]\d)|^(65048[5-9]|65049\d|6505[0-2]\d|65053[0-8])|^(65054[1-9]|6505[5-8]\d|65059[0-8])|^(65070\d|65071[0-8])|^65072[0-7]|^(65090[1-9]|65091\d|650920)|^(65165[2-9]|6516[6-7]\d)|^(65500\d|65501\d)|^(65502[1-9]|6550[3-4]\d|65505[0-8])").Success)
            {
                return CardType.ELO;
            }
            
            if (Regex.Match(cardNumber, @"^606282|^637095|^637599|^637568").Success)
            {
                return CardType.HIPERCARD;
            }

            return CardType.Unknown;
        }
    }
    
    public enum CardType
    {
        MASTERCARD, VISA, AMEX, Discover, JCB, ELO, HIPERCARD, Unknown
    };
}