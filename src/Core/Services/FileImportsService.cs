using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Entities;
using Core.Infrastructure;
using Core.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Microsoft.Extensions.Configuration;

namespace Core.Services
{
    public class FileImportsService : IFileImportsService
    {
        private readonly ApplicationDbContext _applicationDbContext;
        private readonly string _uriPath;

        public FileImportsService(ApplicationDbContext applicationDbContext, IConfiguration configuration)
        {
            _applicationDbContext = applicationDbContext;
            _uriPath = configuration["CdnPath"];
        }
        
        public async Task<bool> SaveAsync(FileImport fileImport)
        {
            var file = await _applicationDbContext.FileImport
                .FirstOrDefaultAsync(d => d.Guid == fileImport.Guid);

            if (file == null)
            {
                fileImport.FileUri = $"{_uriPath}{fileImport.FileUri}";
                _applicationDbContext.FileImport.Add(fileImport);
            }

            return await _applicationDbContext.SaveChangesAsync() > 0;
        }

        public async Task<IList<FileImport>> ListAsync(TypeFile typeFile)
        {
            return await _applicationDbContext.FileImport
                .Where(f => f.TypeFile == typeFile)
                .ToListAsync();
        }
        
        public async Task<IList<FileImport>> ListOfStatusAsync(TypeFile typeFile, Status status)
        {
            return await _applicationDbContext.FileImport
                .Where(f => f.TypeFile == typeFile && f.Status == status)
                .ToListAsync();
        }

        public async Task<FileImport> GetAsync(Guid guid)
        {
            return await _applicationDbContext.FileImport
                .Where(d => d.Guid == guid)
                .FirstOrDefaultAsync();
        }
        
        public async Task<bool> DeleteAllTypeAsync(TypeFile typeFile)
        {
            var file = _applicationDbContext.FileImport.Where(f => f.TypeFile == typeFile);
            
            _applicationDbContext.RemoveRange(file);
            
            return await _applicationDbContext.SaveChangesAsync() > 0;
        }
        
        public async Task<bool> DeleteAllTypeAndDrawAsync(TypeFile typeFile, Guid drawGuid)
        {
            var file = _applicationDbContext.FileImport.Where(f => f.TypeFile == typeFile && f.DrawGuid == drawGuid);
            
            _applicationDbContext.RemoveRange(file);
            
            return await _applicationDbContext.SaveChangesAsync() > 0;
        }
    }
}