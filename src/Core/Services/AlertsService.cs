using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Entities;
using Core.Infrastructure;
using Core.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Linq.Dynamic.Core;

namespace Core.Services
{
    public class AlertsService : IAlertsService
    {
        private readonly ApplicationDbContext _applicationDbContext;

        public AlertsService(ApplicationDbContext applicationDbContext)
        {
            _applicationDbContext = applicationDbContext;
        }
        
        public async Task<bool> SaveAsync(Alert alert)
        {
            var res = await _applicationDbContext.Alert
                .FirstOrDefaultAsync(d => d.Guid == alert.Guid);

            if (res == null)
            {
                _applicationDbContext.Alert.Add(alert);
            }

            return await _applicationDbContext.SaveChangesAsync() > 0;
        }

        public async Task<bool> DeleteAllAsync()
        {
            var alert = _applicationDbContext.Alert;

            _applicationDbContext.RemoveRange(alert);
            
            return await _applicationDbContext.SaveChangesAsync() > 0;
        }

        public async Task<IList<Alert>> ListAsync()
        {
            return await _applicationDbContext.Alert
                .OrderByDescending(a => a.CreatedAt)
                .ToListAsync();
        }

        public async Task<Alert> GetAsync(Guid guid)
        {
            return await _applicationDbContext.Alert
                .Where(d => d.Guid == guid)
                .FirstOrDefaultAsync();
        }

        public async Task<Alert> GetActiveAsync()
        {
            return await _applicationDbContext.Alert
                .Where(d => d.IsActive)
                .FirstOrDefaultAsync();
        }

        public async Task<bool> ToggleAlertAsync(Guid guid)
        {
            var alert = await _applicationDbContext.Alert.FindAsync(guid);

            if (alert == null) throw new Exception($"No alert found, identifier {guid}");

            alert.IsActive = !alert.IsActive;

            return await _applicationDbContext.SaveChangesAsync() > 0;
        }
    }
}