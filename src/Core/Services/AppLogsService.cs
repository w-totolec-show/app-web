﻿using Core.DTOs.AppLogs;
using Core.Entities.AppLogs;
using Core.Infrastructure;
using Core.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Core.DTOs;

namespace Core.Services
{
    public class AppLogsService : IAppLogsService
    {
        private readonly ApplicationDbContext _applicationDbContext;

        public AppLogsService(ApplicationDbContext applicationDbContext)
        {
            _applicationDbContext = applicationDbContext;
        }

        public async Task<PaginatedResult<Log>> ListAsync(ListLogsRequest request)
        {
            var q = _applicationDbContext.Logs
                .Include(u => u.AppUser)
                .Where(u => u.UserGuid == request.userGuid)
                .AsQueryable();
            
            var countRes = await q.CountAsync();

            var res = await q
                .OrderByDescending(u => u.CreatedAt)
                .Skip((request.Page - 1) * request.PageSize)
                .Take(request.PageSize)
                .ToListAsync();

            return new PaginatedResult<Log>(res, countRes);
        }
        
        public async Task<PaginatedResult<Log>> ListTypeAsync(ListLogsRequest request)
        {
            var q = _applicationDbContext.Logs
                .Include(u => u.AppUser)
                .Where(u => u.LogType == request.Type)
                .AsQueryable();
            
            var countRes = await q.CountAsync();

            var res = await q
                .Skip((request.Page - 1) * request.PageSize)
                .Take(request.PageSize)
                .OrderByDescending(u => u.CreatedAt)
                .ToListAsync();

            return new PaginatedResult<Log>(res, countRes);
        }

        public async Task<Log> GetAsync(Guid guid)
        {
            return await _applicationDbContext.Logs
                .FirstOrDefaultAsync(lg => lg.Guid == guid);
        }

        public async Task<IList<Log>> GetUserAsync(Guid guid)
        {
            return await _applicationDbContext.Logs
                .Include(lg => lg.AppUser)
                .Where(lg => lg.UserGuid == guid)
                .ToListAsync();
        }

        public async Task<bool> SaveAsync(SaveLogRequest request)
        {
            var log = new Log
            {
                Guid = new Guid(),
                LogType = LogType.Users,
                UserGuid = request.UserGuid,
                CreatedAt = DateTime.UtcNow,
                UserModification = request.UserModification,
                PropertyLogs = request.PropertyLogs
            };

            _applicationDbContext.Logs.Add(log);

            return await _applicationDbContext.SaveChangesAsync() > 0;
        }
    }
}