using System;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Core.Entities;
using Core.Infrastructure;
using Core.Services.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Core.Services
{
    public class MessagesService : IMessagesService
    {
        private readonly ApplicationDbContext _applicationDbContext;
        
        public MessagesService(ApplicationDbContext applicationDbContext)
        {
            _applicationDbContext = applicationDbContext;
        }
        
        public async Task<bool> SaveAsync(Messages messages)
        {
            var messagesExist = await _applicationDbContext.Messages
                .FirstOrDefaultAsync(m => m.TypeMessage == messages.TypeMessage);

            if (messagesExist != null)
                _applicationDbContext.RemoveRange(messagesExist);

            _applicationDbContext.Messages.Add(messages);

            return await _applicationDbContext.SaveChangesAsync() > 0;
        }

        public Task<bool> DeleteAllTypeAsync(TypeMessage typeMessage)
        {
            throw new NotImplementedException();
        }

        public async Task<Messages> GetAsync(TypeMessage typeMessage)
        {
            return await _applicationDbContext.Messages
                .FirstOrDefaultAsync(m => m.TypeMessage == typeMessage);
        }
    }
}