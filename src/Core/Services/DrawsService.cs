using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Core.DTOs;
using Core.DTOs.AppLogs;
using Core.DTOs.Draws;
using Core.Entities;
using Core.Entities.AppLogs;
using Core.Infrastructure;
using Core.Services.Interfaces;
using Core.Utils;
using FirebirdSql.Data.FirebirdClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using ServiceStack;
using StackExchange.Redis;

namespace Core.Services
{
    public class DrawsService : IDrawsService
    {
        private readonly ApplicationDbContext _applicationDbContext;
        //private readonly ConnectionMultiplexer _connectionMultiplexer;
        private readonly IAppLogsService _appLogsService;
        private readonly ITicketsService _ticketsService;

        public DrawsService(ApplicationDbContext applicationDbContext,
                            IAppLogsService appLogsService,
                            ITicketsService ticketsService)
        {
            _applicationDbContext = applicationDbContext;
            _ticketsService = ticketsService;
            //_connectionMultiplexer = connectionMultiplexer;
            _appLogsService = appLogsService;
        }
        
        public async Task<PaginatedResult<Draw>> ListAsync(ListDrawsRequest request)
        {
            var q = _applicationDbContext.Draw
                .OrderBy(request.OrderBy ?? "Guid")
                .AsQueryable();
            
            if (!string.IsNullOrWhiteSpace(request.SearchTerm))
            {
                q = q.Where(d => 
                    d.Guid.ToString().Contains(request.SearchTerm) 
                    || d.Name.ToLower().Contains(request.SearchTerm)
                    || d.DrawNumber.ToString().Contains(request.SearchTerm));
            }
            
            if (!request.IsActive)
                q = q.Where(u => u.IsActive == !request.IsActive);
            
            var countRes = await q.CountAsync();

            var res = await q
                .OrderByDescending(d => d.DrawDate)
                .Skip((request.Page - 1) * request.PageSize)
                .Take(request.PageSize)
                .ToListAsync();

            return new PaginatedResult<Draw>(res, countRes);
        }

        public async Task<IList<Draw>> ListAllAsync()
        {
            var q = _applicationDbContext.Draw
                .OrderBy("Guid")
                .AsQueryable();

            return await q.ToListAsync();
        }

        public async Task<Draw> GetActiveAsync()
        {
            return await _applicationDbContext.Draw
                .Include(d => d.FileImports)
                .Where(d => d.IsActive)
                .FirstOrDefaultAsync();
        }

        public async Task<Draw> GetAvailableAsync()
        {
            var res = await _applicationDbContext.Draw
                .Include(d => d.FileImports)
                .Where(d => d.IsActive)
                .FirstOrDefaultAsync();

            if (res != null)
            {
                DateTime dateTimeUtc = DateTime.UtcNow;
                var brazilDatetime = dateTimeUtc.ToLocalString();
                DateTime dateTime = DateTime.Parse(brazilDatetime);
                
                if (!(res.StartDatePurchase <= dateTime && res.EndDatePurchase >= dateTime))
                    return null;
            }

            return res;
        }
        
        public async Task<Draw> GetAsync(Guid guid)
        {
            /*var db = _connectionMultiplexer.GetDatabase();
            var key = $"DRAW_{guid}";
            var drawJson = await db.StringGetAsync(key);
            
            if (drawJson.HasValue)
                return drawJson.ToString().FromJson<(Draw, int, int)>();*/

            var q = _applicationDbContext.Draw
                .Include(d => d.Tickets)
                .ThenInclude(d => d.AppUser)
                .Include(d => d.FileImports)
                .Where(d => d.Guid == guid)
                .AsQueryable();

            var res = await q.FirstOrDefaultAsync();
                
            //await db.StringSetAsync(key, new Tuple<Draw, int, int>(res, countSold, countPending).ToSafeJson(), TimeSpan.FromMinutes(2));
                
            return res;
        }

        public async Task<Draw> GetUniqueAsync(Guid guid)
        {
            var q = _applicationDbContext.Draw
                .Include(d => d.FileImports)
                .Where(d => d.Guid == guid)
                .AsQueryable();

            var res = await q.FirstOrDefaultAsync();

            return res;
        }

        public async Task<Draw> SaveAsync(SaveDrawRequest request)
        {
            var draw = await _applicationDbContext.Draw
                .FirstOrDefaultAsync(d => d.Guid == request.Guid);

            if (draw == null)
            {
                draw = new Draw
                {
                    Guid = new Guid(),
                    Name = request.Name,
                    DrawNumber = request.DrawNumber,
                    Parameters = request.Parameters,
                    OriginalLayoutName = request.OriginalLayoutName,
                    LayoutUri = request.LayoutUri,
                    DrawDate = request.DrawDate,
                    StartDatePurchase = request.StartDatePurchase,
                    EndDatePurchase = request.EndDatePurchase,
                    ValueOfTicket = request.ValueOfTicket,
                    Observations = request.Observations,
                    UrlCommercial = request.UrlCommercial,
                    DescriptionOfDraw = request.DescriptionOfDraw,
                    DescriptionOfResult = request.DescriptionOfResult,
                    Accomplished = false,
                    IsActive = request.IsActive,
                    CreatedAt = DateTime.UtcNow
                };

                _applicationDbContext.Draw.Add(draw);
            }
            else
            {
                var compare = CompareObjects(draw, request);

                if (compare.Any())
                {
                    if (request.UserModification != null)
                    {
                        var logRequest = new SaveLogRequest
                        {
                            LogType = LogType.Draw,
                            UserGuid = draw.Guid,
                            PropertyLogs = compare,
                            UserModification = request.UserModification.Value
                        };

                        await _appLogsService.SaveAsync(logRequest);
                    }
                }
                
                draw.Name = request.Name;
                draw.DrawNumber = request.DrawNumber;
                draw.OriginalLayoutName = request.OriginalLayoutName;
                draw.LayoutUri = request.LayoutUri;
                draw.Observations = request.Observations;
                draw.UrlCommercial = request.UrlCommercial;
                draw.DescriptionOfDraw = request.DescriptionOfDraw;
                draw.DescriptionOfResult = request.DescriptionOfResult;
                draw.Parameters = request.Parameters;
                draw.DrawDate = request.DrawDate;
                draw.Tickets = request.Tickets;
                draw.IsActive = request.IsActive;
                draw.StartDatePurchase = request.StartDatePurchase;
                draw.EndDatePurchase = request.EndDatePurchase;
                draw.ValueOfTicket = request.ValueOfTicket;
                draw.ModifiedAt = DateTime.UtcNow;
            }

            return await _applicationDbContext.SaveChangesAsync() > 0 ? draw : null;
        }
        
        public async Task<bool> ToggleAccomplishedAsync(Guid guid)
        {
            var draw = await _applicationDbContext.Draw.FindAsync(guid);

            if (draw == null) throw new Exception($"No draw found, identifier {guid}");

            draw.Accomplished = !draw.Accomplished;

            return await _applicationDbContext.SaveChangesAsync() > 0;
        }

        private IList<PropertyLog> CompareObjects(Draw before, SaveDrawRequest after)
        {
            var list = new List<PropertyLog>();

            if (before.DrawNumber != after.DrawNumber)
            {
                var propertyLog = new PropertyLog
                {
                    LogLevel = 1,
                    Before = before.DrawNumber.ToString(),
                    After = after.DrawNumber.ToString(),
                    Property = "Número do sorteio",
                    CreatedAt = DateTime.UtcNow
                };
                
                list.Add(propertyLog);
            }
            
            if (before.Name != after.Name)
            {
                var propertyLog = new PropertyLog
                {
                    LogLevel = 1,
                    Before = before.Name,
                    After = after.Name,
                    Property = "Nome",
                    CreatedAt = DateTime.UtcNow
                };
                
                list.Add(propertyLog);
            }
            
            if (before.Name != after.Name)
            {
                var propertyLog = new PropertyLog
                {
                    LogLevel = 1,
                    Before = before.Name,
                    After = after.Name,
                    Property = "Nome",
                    CreatedAt = DateTime.UtcNow
                };
                
                list.Add(propertyLog);
            }
            
            if (before.ValueOfTicket != after.ValueOfTicket)
            {
                var propertyLog = new PropertyLog
                {
                    LogLevel = 1,
                    Before = $"{before.ValueOfTicket:C}",
                    After = $"{after.ValueOfTicket:C}",
                    Property = "Valor da cartela",
                    CreatedAt = DateTime.UtcNow
                };
                
                list.Add(propertyLog);
            }
            
            if (before.DrawDate != after.DrawDate)
            {
                var propertyLog = new PropertyLog
                {
                    LogLevel = 1,
                    Before = before.DrawDate.ToString(CultureInfo.InvariantCulture),
                    After = after.DrawDate.ToString(CultureInfo.InvariantCulture),
                    Property = "Data do sorteio",
                    CreatedAt = DateTime.UtcNow
                };
                
                list.Add(propertyLog);
            }
            
            if (before.StartDatePurchase != after.StartDatePurchase)
            {
                var propertyLog = new PropertyLog
                {
                    LogLevel = 1,
                    Before = before.StartDatePurchase.ToString(CultureInfo.InvariantCulture),
                    After = after.StartDatePurchase.ToString(CultureInfo.InvariantCulture),
                    Property = "Data início de venda",
                    CreatedAt = DateTime.UtcNow
                };
                
                list.Add(propertyLog);
            }
            
            if (before.EndDatePurchase != after.EndDatePurchase)
            {
                var propertyLog = new PropertyLog
                {
                    LogLevel = 1,
                    Before = before.EndDatePurchase.ToString(CultureInfo.InvariantCulture),
                    After = after.EndDatePurchase.ToString(CultureInfo.InvariantCulture),
                    Property = "Data fim de venda",
                    CreatedAt = DateTime.UtcNow
                };
                
                list.Add(propertyLog);
            }
            
            if (before.Observations != after.Observations)
            {
                var propertyLog = new PropertyLog
                {
                    LogLevel = 1,
                    Before = before.Observations,
                    After = after.Observations,
                    Property = "Observações",
                    CreatedAt = DateTime.UtcNow
                };
                
                list.Add(propertyLog);
            }
            
            if (before.UrlCommercial != after.UrlCommercial)
            {
                var propertyLog = new PropertyLog
                {
                    LogLevel = 1,
                    Before = before.UrlCommercial,
                    After = after.UrlCommercial,
                    Property = "URL do VT",
                    CreatedAt = DateTime.UtcNow
                };
                
                list.Add(propertyLog);
            }
            
            if (before.DescriptionOfDraw != after.DescriptionOfDraw)
            {
                var propertyLog = new PropertyLog
                {
                    LogLevel = 1,
                    Before = before.DescriptionOfDraw,
                    After = after.DescriptionOfDraw,
                    Property = "Descrição do sorteio",
                    CreatedAt = DateTime.UtcNow
                };
                
                list.Add(propertyLog);
            }
            
            if (before.DescriptionOfResult != after.DescriptionOfResult)
            {
                var propertyLog = new PropertyLog
                {
                    LogLevel = 1,
                    Before = before.DescriptionOfResult,
                    After = after.DescriptionOfResult,
                    Property = "Descrição do resultado",
                    CreatedAt = DateTime.UtcNow
                };
                
                list.Add(propertyLog);
            }
            
            if (before.IsActive != after.IsActive)
            {
                var propertyLog = new PropertyLog
                {
                    LogLevel = 1,
                    Before = before.IsActive ? "Sim" : "Não",
                    After = after.IsActive ? "Sim" : "Não",
                    Property = "Ativo?",
                    CreatedAt = DateTime.UtcNow
                };
                
                list.Add(propertyLog);
            }
            
            return list;
        }
    }
}