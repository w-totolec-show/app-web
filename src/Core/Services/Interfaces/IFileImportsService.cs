using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Entities;

namespace Core.Services.Interfaces
{
    public interface IFileImportsService
    {
        Task<bool> SaveAsync(FileImport fileImport);

        Task<bool> DeleteAllTypeAsync(TypeFile typeFile);
        
        Task<bool> DeleteAllTypeAndDrawAsync(TypeFile typeFile, Guid drawGuid);
        
        Task<IList<FileImport>> ListAsync(TypeFile typeFile);
        
        Task<IList<FileImport>> ListOfStatusAsync(TypeFile typeFile, Status status);

        Task<FileImport> GetAsync(Guid guid);
    }
}