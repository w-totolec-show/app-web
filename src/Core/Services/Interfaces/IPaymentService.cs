using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Core.DTOs.Tickets;
using Core.Entities;
using RestSharp;

namespace Core.Services.Interfaces
{
    public interface IPaymentService
    {
        Task<string> GetTokenAuthorizationAsync();
        
        Task<Tuple<string, string>> FullPaymentAsync(string token, string userIdAxis, string cardIdAxis, IList<Ticket> tickets, Guid userId);

        Task<Tuple<string, string>> TokenizeCreditCard(PurchaseTicketRequest request, string token, string userIdAxis);

        Task<Tuple<string, string>> GenerateUserIdAxis(AppUser user, string token);
        Task<string> GenerateAddressIdAxis(AppUser user, string token, string userIdAxis);

        Task<string> GetPaymentAsync(string token, string idTransaction);
    }
}