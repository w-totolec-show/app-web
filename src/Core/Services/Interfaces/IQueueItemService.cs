using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Entities;
using Type = Core.Entities.Type;

namespace Core.Services.Interfaces
{
    public interface IQueueItemService
    {
        Task<bool> SaveAsync(QueueItem queueItem);
        
        Task<IList<QueueItem>> ListAsync(Type type, Guid drawGuid);
        
        Task<IList<QueueItem>> ListAwaitingProcessingAsync(Type type);
    }
}