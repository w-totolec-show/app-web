using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Entities;

namespace Core.Services.Interfaces
{
    public interface IAlertsService
    {
        Task<bool> SaveAsync(Alert alert);

        Task<bool> DeleteAllAsync();
        
        Task<IList<Alert>> ListAsync();

        Task<Alert> GetAsync(Guid guid);
        
        Task<Alert> GetActiveAsync();
        
        Task<bool> ToggleAlertAsync(Guid guid);
    }
}