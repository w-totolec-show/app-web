using System.Threading.Tasks;
using Core.Entities;

namespace Core.Services.Interfaces
{
    public interface ITermsService
    {
        Task<bool> SaveAsync(TermsOfService alert);
        Task<TermsOfService> GetAsync();
    }
}