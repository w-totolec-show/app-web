using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Core.DTOs;
using Core.DTOs.Tickets;
using Core.Entities;

namespace Core.Services.Interfaces
{
    public interface ITicketsService
    {
        Task<PaginatedResult<Ticket>> ListAsync(ListTicketsRequest request);
        
        Task<PaginatedResult<Ticket>> ListSuggestionsAsync(ListTicketsRequest request);
        
        Task<PaginatedResult<Ticket>> ListCustomerAsync(ListTicketsRequest request);
        
        Task<IList<Ticket>> ListCustomerAllAsync(Guid userGuid);
        
        Task<Ticket> GetAsync(Guid guid);
        
        Task<bool> DeleteAllAsync(Guid guid);
        Task<PaginatedResult<Ticket>> ListSold(ListTicketsRequest request);
        Task<int> CountSold(Guid guidDraw);
        Task<int> CountPending(Guid guidDraw);
        Task<IList<Ticket>> ListAllSold(Guid guidDraw);

        Task<Ticket> SaveAsync(SaveTicketRequest request);

        Task<(string, string, IList<string>)> PurchaseAsync(PurchaseTicketRequest request);
        
        Task<bool> SaveAsync(IList<SaveTicketRequest> saveTicketRequests);

        Task<bool> DisableAsync(Guid guid, bool isActive);
        Task<bool> ReturnForSaleAsync(Guid guid);
    }
}