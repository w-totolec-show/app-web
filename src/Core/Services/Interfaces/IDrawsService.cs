using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Core.DTOs;
using Core.DTOs.Draws;
using Core.Entities;

namespace Core.Services.Interfaces
{
    public interface IDrawsService
    {
        Task<PaginatedResult<Draw>> ListAsync(ListDrawsRequest request);
        
        Task<IList<Draw>> ListAllAsync();
        
        Task<Draw> GetAsync(Guid guid);
        Task<Draw> GetUniqueAsync(Guid guid);
        
        Task<Draw> GetActiveAsync();
        
        Task<Draw> GetAvailableAsync();

        Task<Draw> SaveAsync(SaveDrawRequest request);
        
        Task<bool> ToggleAccomplishedAsync(Guid guid);
    }
}