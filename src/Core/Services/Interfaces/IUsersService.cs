using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Core.DTOs;
using Core.DTOs.Users;
using Core.Entities;
using Microsoft.AspNetCore.Identity;

namespace Core.Services.Interfaces
{
    public interface IUsersService
    {
        Task<PaginatedResult<AppUser>> ListAsync(ListUsersRequest request);
        
        Task<IList<AppUser>> ListAllAsync(bool isAdmin);

        Task<IList<IdentityRole<Guid>>> ListAllRolesAsync();

        Task<IEnumerable<string>> ListUserRolesAsync(Guid id);

        Task<(AppUser, IList<string>)> GetAsync(Guid id);

        Task<AppUser> SaveAsync(SaveUserRequest request);

        Task<bool> DisableAsync(Guid id);
        
        Task<bool> DeleteAsync(Guid id);

        Task<bool> DisableAsyncTest();

        Task<bool> SaveUserIdAxis(Guid id, string userIdAxis);

        Task<bool> SaveAddressIdAxis(Guid id, string addressIdAxis);
    }
}