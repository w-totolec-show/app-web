using System;
using System.Threading.Tasks;
using Core.Entities;

namespace Core.Services.Interfaces
{
    public interface IMessagesService
    {
        Task<bool> SaveAsync(Messages messages);
        
        Task<bool> DeleteAllTypeAsync(TypeMessage typeMessage);
        
        Task<Messages> GetAsync(TypeMessage typeMessage);
    }
}