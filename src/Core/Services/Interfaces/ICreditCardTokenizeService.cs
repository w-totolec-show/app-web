using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Entities;

namespace Core.Services.Interfaces
{
    public interface ICreditCardTokenizeService
    {
        Task<bool> SaveAsync(CreditCardToken creditCardToken);

        Task<IList<CreditCardToken>> ListAsync(Guid userGuid);
        
        Task<CreditCardToken> GetAsync(Guid guid);

        Task<bool> DeleteAsync(Guid guid);
    }
}