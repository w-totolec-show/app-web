﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Core.DTOs;
using Core.DTOs.AppLogs;
using Core.Entities.AppLogs;

namespace Core.Services.Interfaces
{
    public interface IAppLogsService
    {
        Task<PaginatedResult<Log>> ListAsync(ListLogsRequest request);
        Task<PaginatedResult<Log>> ListTypeAsync(ListLogsRequest request);

        Task<Log> GetAsync(Guid guid);
        
        Task<IList<Log>> GetUserAsync(Guid guid);

        Task<bool> SaveAsync(SaveLogRequest request);
    }
}
