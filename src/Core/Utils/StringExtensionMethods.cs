﻿using System;
using System.Text.RegularExpressions;

namespace Core.Utils
{
    public static class StringExtensionMethods
    {
        public static string FromCamelToSnakeCase(this string input)
        {
            if (string.IsNullOrEmpty(input)) return input;

            var startUnderscores = Regex.Match(input, @"^_+");
            return startUnderscores + Regex.Replace(input, @"([a-z0-9])([A-Z])", "$1_$2").ToLower();
        }
    }
}