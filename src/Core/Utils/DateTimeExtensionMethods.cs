using System;
using System.Runtime.InteropServices;

namespace Core.Utils
{
    public static class DateTimeExtensionMethods
    {
        private static readonly TimeZoneInfo DefaultTimezoneInfo =
            RuntimeInformation.IsOSPlatform(OSPlatform.Windows)
                ? TimeZoneInfo.FindSystemTimeZoneById("E. South America Standard Time")
                : TimeZoneInfo.FindSystemTimeZoneById("America/Sao_Paulo");

        public static DateTime ToLocal(this DateTime utcDateTime, TimeZoneInfo timeZoneInfo = null)
        {
            timeZoneInfo ??= DefaultTimezoneInfo;

            utcDateTime = DateTime.SpecifyKind(utcDateTime, DateTimeKind.Utc);

            return TimeZoneInfo.ConvertTimeFromUtc(utcDateTime, timeZoneInfo);
        }

        public static string ToLocalString(this DateTime utcDateTime, TimeZoneInfo timeZoneInfo = null)
        {
            var localDateTime = utcDateTime.ToLocal(timeZoneInfo);

            return localDateTime.ToString("dd/MM/yyyy HH:mm:ss");
        }
    }
}