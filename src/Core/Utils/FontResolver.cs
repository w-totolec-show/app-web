using System;
using System.IO;
using PdfSharpCore.Fonts;

namespace Core.Utils
{
    public class FontResolver : IFontResolver
    {
        public string DefaultFontName { get; }

        public byte[] GetFont(string faceName)
        {
            using(var ms = new MemoryStream())
            {
                using(var fs = File.Open(faceName, FileMode.Open))
                {
                    fs.CopyTo(ms);
                    ms.Position = 0;
                    return ms.ToArray();
                }
            }
        }

        public FontResolverInfo ResolveTypeface(string familyName, bool isBold, bool isItalic)
        {
            if (familyName.Equals("OpenSans", StringComparison.CurrentCultureIgnoreCase))
            {
                var path = Path.Combine(
                    Directory.GetCurrentDirectory(),
                    "Fonts");
                
                if (isBold && isItalic)
                {
                    return new FontResolverInfo($"{path}/OpenSans-BoldItalic.ttf");
                }
                else if (isBold)
                {
                    return new FontResolverInfo($"{path}/OpenSans-Bold.ttf");
                }
                else if (isItalic)
                {
                    return new FontResolverInfo($"{path}/OpenSans-Italic.ttf");
                }
                else
                {
                    return new FontResolverInfo($"{path}/OpenSans-Regular.ttf");
                }
            }
            return null;
        }
    }
}