using System;
using System.Security.Cryptography;
using System.Text;

namespace Core.Utils
{
    public static class StringCipher
    {
        public static string Encrypt(string plainText, string passPhrase)
        {
            byte[] data = Encoding.UTF8.GetBytes(plainText);
            using (MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider())
            {
                byte[] keys = md5.ComputeHash(Encoding.UTF8.GetBytes(passPhrase));
                using (TripleDESCryptoServiceProvider tripDes = new TripleDESCryptoServiceProvider() {
                    Key = keys,
                    Mode = CipherMode.ECB,
                    Padding = PaddingMode.PKCS7
                })
                {
                    ICryptoTransform transform = tripDes.CreateEncryptor();
                    byte[] results = transform.TransformFinalBlock(data, 0, data.Length);
                    
                    return Convert.ToBase64String(results, 0, results.Length);
                }
            }
        }
        
        public static string Decrypt(string cipherText, string passPhrase)
        {
            byte[] data = Convert.FromBase64String(cipherText);
            using (MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider())
            {
                byte[] keys = md5.ComputeHash(Encoding.UTF8.GetBytes(passPhrase));
                using (TripleDESCryptoServiceProvider tripDes = new TripleDESCryptoServiceProvider() {
                    Key = keys,
                    Mode = CipherMode.ECB,
                    Padding = PaddingMode.PKCS7
                })
                {
                    ICryptoTransform transform = tripDes.CreateDecryptor();
                    byte[] results = transform.TransformFinalBlock(data, 0, data.Length);
                    
                    return Encoding.UTF8.GetString(results);
                }
            }
        }
    }
}