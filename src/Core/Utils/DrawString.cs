using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

namespace Core.Utils
{
    public static class DrawString
    {
        public static bool Draw(string path)
        {
            try
            {
                Image image = Image.FromFile(path);

                //Bitmap bmp = new Bitmap(path);
                Graphics gra = Graphics.FromImage(image);
                string text = "Hello\nWorld";

                gra.DrawString(text, new Font("Verdana", 24), Brushes.Black, new PointF(0, 0));

                image.Save("myfile.Jpeg", ImageFormat.Jpeg);

                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }
}