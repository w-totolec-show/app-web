using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography.Xml;
using ClosedXML.Excel;
using Core.Entities;
using iText.Signatures;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using PdfSharpCore.Drawing;
using PdfSharpCore.Fonts;
using PdfSharpCore.Pdf;
using System.Drawing;
using System.Drawing.Imaging;
using System.Text;
using System.Threading;
using DocumentFormat.OpenXml.Spreadsheet;
using Newtonsoft.Json.Linq;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Text;
using RestSharp;
using ServiceStack;

namespace Core.Utils
{
    public static class ExportUtilities
    {
        private static int _x = 0;
        
        public static void ExportXlsx(this IEnumerable<object> objs, string sheetName, Stream stream)
        {
            // Enumerate objects to void multiple-enumerations
            var objects = objs as List<object> ?? objs.ToList();

            // Safe guards
            if (!objects.Any()) return;

            if (stream == null || !stream.CanWrite) return;

            using var workbook = new XLWorkbook();
            var worksheet = workbook.Worksheets.Add(sheetName);
            var currentRow = 1;

            var firstObject = objects.First();
            var properties = firstObject.GetType().GetProperties();

            // Headers
            foreach (var propertyInfo in properties)
            {
                var displayNameAttribute = (DisplayNameAttribute) propertyInfo.GetCustomAttribute(typeof(DisplayNameAttribute));
                var ignoreDataMemberAttribute = (IgnoreDataMemberAttribute) propertyInfo.GetCustomAttribute(typeof(IgnoreDataMemberAttribute));

                if (ignoreDataMemberAttribute != null)
                    continue;

                worksheet.Cell(currentRow, Array.IndexOf(properties, propertyInfo) + 1).Value
                    = displayNameAttribute?.DisplayName ?? propertyInfo.Name;
            }

            // Values
            foreach (var o in objects)
            {
                currentRow++;

                foreach (var propertyInfo in properties)
                {
                    var ignoreDataMemberAttribute = (IgnoreDataMemberAttribute) propertyInfo.GetCustomAttribute(typeof(IgnoreDataMemberAttribute));

                    if (ignoreDataMemberAttribute != null)
                        continue;

                    var propValue = propertyInfo.GetValue(o);

                    if (propValue is IEnumerable<object> enumerable)
                        worksheet.Cell(currentRow, Array.IndexOf(properties, propertyInfo) + 1).Value
                            = enumerable.Count();

                    else if (propValue is IDictionary dictionary)
                        worksheet.Cell(currentRow, Array.IndexOf(properties, propertyInfo) + 1).Value
                            = dictionary.Count;
                    else
                        worksheet.Cell(currentRow, Array.IndexOf(properties, propertyInfo) + 1).Value
                            = propValue;
                }
            }

            workbook.SaveAs(stream);
        }
        
        public static void ExportLot(this List<List<Ticket>> lots, Stream stream, string extractionNumber, string distributor)
        {
            // Safe guards
            if (!lots.Any()) return;

            if (stream == null || !stream.CanWrite) return;

            using var workbook = new XLWorkbook();

            distributor = $"{distributor.ToInt():000}";
            
            DataTable tableDrawer = new DataTable();
            tableDrawer.TableName = "GAVETA";
            tableDrawer.Columns.Add("GVT_COD_DISTR");
            tableDrawer.Columns.Add("GVT_NUM_EXTRACAO");
            tableDrawer.Columns.Add("GVT_COD");
            tableDrawer.Columns.Add("GVT_TRANSF");
            
            tableDrawer.Rows.Add($"{distributor}", extractionNumber, "001", "0");
            
            DataTable tableLot = new DataTable();
            tableLot.TableName = "LOTE";
            tableLot.Columns.Add("LTE_COD_DISTR");
            tableLot.Columns.Add("LTE_NUM_EXTRACAO");
            tableLot.Columns.Add("GVT_COD");
            tableLot.Columns.Add("LTE_COD");
            tableLot.Columns.Add("LTE_TRANSF");
            
            DataTable tableTicket = new DataTable();
            tableTicket.TableName = "CARTELA";
            tableTicket.Columns.Add("LTE_COD_DISTR");
            tableTicket.Columns.Add("LTE_NUM_EXTRACAO");
            tableTicket.Columns.Add("GVT_COD");
            tableTicket.Columns.Add("LTE_COD");
            tableTicket.Columns.Add("CRT_COD_BAR");
            tableTicket.Columns.Add("CRT_TRANSF");

            var currentLot = 1;
            foreach (var lot in lots)
            {
                tableLot.Rows.Add($"{distributor}", extractionNumber, "001", $"{currentLot:0000}", "0");

                foreach (var ticket in lot)
                {
                    var number = ticket.TicketNumber.Insert(7, "-");
                    number = number.Insert(11, "-");
                    number = number.Insert(15, "-");
                    tableTicket.Rows.Add($"{distributor}", extractionNumber, "001", $"{currentLot:0000}", number, "0");
                }
                
                currentLot++;
            }

            workbook.Worksheets.Add(tableDrawer);
            workbook.Worksheets.Add(tableLot);
            workbook.Worksheets.Add(tableTicket);
            
            workbook.SaveAs(stream);
        }
        
        public static List<MemoryStream> ExportLotPdf(List<List<Ticket>> lots)
        {
            try
            {
                var fontResolverType = GlobalFontSettings.FontResolver.GetType(); 
                if (fontResolverType != typeof(FontResolver))
                {
                    GlobalFontSettings.FontResolver = new FontResolver();
                }
            
                List<MemoryStream> memoryStreams = new List<MemoryStream>();

                var path = Path.Combine(
                    Directory.GetCurrentDirectory(),
                    "Images");
            
                XImage image = XImage.FromFile($"{path}/CANHOTO.PNG");
                XFont font = new XFont("OpenSans", 12, XFontStyle.Regular);

                var currentLot = 1;
            
                Console.WriteLine($"Gerando PDFs - Total de lotes {lots.Count}");
            
                foreach (var lot in lots)
                {
                    PdfDocument document = new PdfDocument();

                    foreach (var ticket in lot)
                    {
                        if (ticket.AppUser != null)
                        {
                            PdfPage page = document.AddPage();
                    
                            XGraphics gfx = XGraphics.FromPdfPage(page);
                    
                            gfx.DrawImage(image, 7, 280, 580, 270);
                    
                            var state = gfx.Save();
                            gfx.RotateAtTransform(270, new XPoint(400, 480));
                            gfx.DrawString($"{ticket.TicketNumber.Substring(0, 7)}.{ticket.TicketNumber.Substring(7, 3)}.{ticket.TicketNumber.Substring(10, 3)}.{ticket.TicketNumber.Substring(13, 1)}", font, XBrushes.Black, 410, 650);
                            gfx.Restore(state);
                    
                            gfx.DrawString($"{ticket.AppUser.FirstName} {ticket.AppUser.LastName}", font, XBrushes.Black, 145, 368);

                            string city = "";
                            string bairro = "";
                            if (string.IsNullOrEmpty(ticket.AppUser.Locality) || string.IsNullOrEmpty(ticket.AppUser.City) || ticket.AppUser.City.Equals("************"))
                            {
                                try
                                {
                                    var client = new RestClient("https://viacep.com.br");
                                    var request = new RestRequest($"ws/{ticket.AppUser.Zipcode}/json/", Method.GET);
                                    var queryResult = client.Execute<IRestResponse>(request).Content;

                                    dynamic data = null;
                                    dynamic dataCepLa = null;
                                    dynamic dataApiCep = null;
                                    if (!string.IsNullOrEmpty(queryResult))
                                        data = JObject.Parse(queryResult);
                                    else
                                    {
                                        var clientCepLa = new RestClient("http://cep.la");
                                        var requestCepLa = new RestRequest($"/{ticket.AppUser.Zipcode}", Method.GET);
                                        requestCepLa.AddHeader("Accept", "application/json");
                                        var queryResultCepLa = clientCepLa.Execute<IRestResponse>(requestCepLa).Content;
                        
                                        if (!string.IsNullOrEmpty(queryResultCepLa))
                                            dataCepLa = JObject.Parse(queryResultCepLa);
                                        else
                                        {
                                            var clientApiCep = new RestClient("https://ws.apicep.com");
                                            var requestApiCep = new RestRequest($"/cep.json?code={ticket.AppUser.Zipcode}", Method.GET);
                                            var queryResultApiCep = clientApiCep.Execute<IRestResponse>(requestApiCep).Content;
                    
                                            if (!string.IsNullOrEmpty(queryResultApiCep))
                                                dataApiCep = JObject.Parse(queryResultApiCep);
                                        }
                                    }
                        
                                    if (data != null && !string.IsNullOrEmpty(data.ToString()))
                                    {
                                        city = data.localidade.ToString();
                                        bairro = data.bairro.ToString();
                                    }
                                    else if (dataApiCep != null && !string.IsNullOrEmpty(dataApiCep.ToString()))
                                    {
                                        city = dataApiCep.city.ToString();
                                        bairro = dataApiCep.district.ToString();
                                    }
                                    else if (dataCepLa != null && !string.IsNullOrEmpty(dataCepLa.ToString()))
                                    {
                                        city = dataCepLa.cidade.ToString();
                                        bairro = dataCepLa.bairro.ToString();
                                    }
                                    else
                                    {
                                        bairro = "************";
                                        city = "************";
                                    }

                                }
                                catch (Exception e)
                                {
                                    bairro = "************";
                                    city = "************";
                                    
                                    Console.WriteLine(e);
                                }
                            }
                            else
                            {
                                bairro = ticket.AppUser.Locality;
                                city = ticket.AppUser.City;
                            }
                            
                            /*if (!string.IsNullOrEmpty(ticket.AppUser.Locality))
                                bairro = ticket.AppUser.Locality;
                            else
                            {
                                bairro = "************";
                            }
                                
                            if (!string.IsNullOrEmpty(ticket.AppUser.City))
                                city = ticket.AppUser.City;
                            else
                            {
                                city = "************";
                            }*/
                            
                            gfx.DrawString(bairro, font, XBrushes.Black, 145, 422);
                    
                            gfx.DrawString(city, font, XBrushes.Black, 350, 422);

                            gfx.DrawString($"***.***.{ticket.AppUser.SocialId.Substring(8, 6)}", font, XBrushes.Black, 145, 475);
                    
                            gfx.DrawString($"{ticket.AppUser.PhoneNumber.Substring(0, 9)}-****", font, XBrushes.Black, 350, 475);

                            gfx.DrawString($"{ticket.SimplifiedTicketNumber.Substring(0, 1)}.{ticket.SimplifiedTicketNumber.Substring(1, 3)}.{ticket.SimplifiedTicketNumber.Substring(4, 3)}", font, XBrushes.White, 50, 475);

                            gfx.DrawString(ticket.Draw.DrawDate.ToString("dd/MM/yyyy"), font, XBrushes.Black, 47, 430);
                        }
                        else
                        {
                            Console.WriteLine(ticket.TicketNumber);
                        }
                    }
                
                    MemoryStream stream = new MemoryStream();

                    document.Info.Title = $"LOTE - {currentLot:0000}";
                    document.Info.Author = "Totolec Show";
                    document.Info.Creator = "Totolec Show - Online";
                    document.Info.CreationDate = DateTime.UtcNow;
            
                    document.SecuritySettings.PermitAccessibilityExtractContent = false;
                    document.SecuritySettings.PermitAnnotations = false;
                    document.SecuritySettings.PermitAssembleDocument = false;
                    document.SecuritySettings.PermitExtractContent = false;
                    document.SecuritySettings.PermitFormsFill = false;
                    document.SecuritySettings.PermitFullQualityPrint = true;
                    document.SecuritySettings.PermitModifyDocument = false;
                    document.SecuritySettings.PermitPrint = false;
            
                    document.Save(stream);
  
                    stream.Position = 0;
                
                    memoryStreams.Add(stream);
                
                    Console.WriteLine($"PDF do lote {currentLot} gerado");

                    currentLot++;
                }
            
                return memoryStreams;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public static StringBuilder ExportQueryFirebird(List<List<Ticket>> lots, string extractionNumber, string distributor)
        {
            StringBuilder stringBuilder = new StringBuilder();

            stringBuilder.Append($"INSERT INTO GAVETA VALUES ('" + $"{distributor.ToInt():000}" + "', '" + extractionNumber + "', '001', 0);");

            stringBuilder.Append(Environment.NewLine);
            
            var currentLot = 1;
            foreach (var lot in lots)
            {
                stringBuilder.Append($"INSERT INTO LOTE VALUES ('" + $"{distributor.ToInt():000}" + "', '" + extractionNumber + "', '001', '" + $"{currentLot:0000}" + "', 0);");

                stringBuilder.Append(Environment.NewLine);
                
                foreach (var ticket in lot)
                {
                    var number = ticket.TicketNumber.Insert(7, "-");
                    number = number.Insert(11, "-");
                    number = number.Insert(15, "-");
                    stringBuilder.Append($"INSERT INTO CARTELA VALUES ('" + $"{distributor.ToInt():000}" + "', '" + extractionNumber + "', '001', '" + $"{currentLot:0000}" + "', '" + number + "', 0);");
                    stringBuilder.Append(Environment.NewLine);
                }

                currentLot++;
            }
            
            return stringBuilder;
        }
        
        public static FileStreamResult ExportPdf(Ticket ticket)
        {
            var fontResolverType = GlobalFontSettings.FontResolver.GetType(); 
            if (fontResolverType != typeof(FontResolver))
            {
                GlobalFontSettings.FontResolver = new FontResolver();
            }
            
            PdfDocument document = new PdfDocument();

            PdfPage page = document.AddPage();

            XGraphics gfx = XGraphics.FromPdfPage(page);
  
            XFont fontTitle = new XFont("OpenSans", 20, XFontStyle.Bold);
            XFont font = new XFont("OpenSans", 16, XFontStyle.Bold);
            XFont fontItem = new XFont("OpenSans", 12, XFontStyle.Bold);
            XFont fontItemRes = new XFont("OpenSans", 12, XFontStyle.Regular);
            
            /*var path = environment.WebRootFileProvider.GetFileInfo("images/logo-normal-icon.png").PhysicalPath;
  
            XImage image = XImage.FromFile(path);
            gfx.DrawImage(image, 25, 25, 50, 50);*/
            gfx.DrawString("Totolec Show - Cartela Online", fontTitle, XBrushes.Gray, 160, 50);
            gfx.DrawString("Cartela", font, XBrushes.Black, 35, 100);
            
            gfx.DrawString("Extração/Concurso:", fontItem, XBrushes.Black, 45, 130);
            gfx.DrawString(ticket.ExtractionNumber, fontItemRes, XBrushes.Black, 200, 130);
            
            gfx.DrawString("Número simplificado:", fontItem, XBrushes.Black, 45, 160);
            gfx.DrawString(ticket.SimplifiedTicketNumber, fontItemRes, XBrushes.Black, 200, 160);
            
            gfx.DrawString("Número:", fontItem, XBrushes.Black, 45, 190);
            gfx.DrawString(ticket.TicketNumber, fontItemRes, XBrushes.Black, 200, 190);
            
            gfx.DrawString("Bolas:", fontItem, XBrushes.Black, 45, 220);

            _x = 180;
            for (int i = 0; i < 10; i++)
            {
                _x += 20;
                var ball = ticket.TicketBalls[i];
                gfx.DrawString($"{ball:00}", fontItemRes, XBrushes.Black, _x, 220);
            }

            _x = 180;
            for (int i = 10; i < 20; i++)
            {
                _x += 20;
                var ball = ticket.TicketBalls[i];
                gfx.DrawString($"{ball:00}", fontItemRes, XBrushes.Black, _x, 240);
            }
            
            gfx.DrawString("Comprador", font, XBrushes.Black, 35, 270);
            
            gfx.DrawString("Nome:", fontItem, XBrushes.Black, 45, 300);
            gfx.DrawString($"{ticket.AppUser?.FirstName} {ticket.AppUser?.LastName}", fontItemRes, XBrushes.Black, 200, 300);
            
            gfx.DrawString("E-mail:", fontItem, XBrushes.Black, 45, 330);
            gfx.DrawString($"{ticket.AppUser?.Email}", fontItemRes, XBrushes.Black, 200, 330);
            
            gfx.DrawString("Celular:", fontItem, XBrushes.Black, 45, 360);
            gfx.DrawString($"{ticket.AppUser?.PhoneNumber}", fontItemRes, XBrushes.Black, 200, 360);
            
            gfx.DrawString("CPF:", fontItem, XBrushes.Black, 45, 390);
            gfx.DrawString($"{ticket.AppUser?.SocialId}", fontItemRes, XBrushes.Black, 200, 390);
            
            gfx.DrawString("Data de Nascimento:", fontItem, XBrushes.Black, 45, 420);
            gfx.DrawString($"{ticket.AppUser?.DateOfBirth:dd/MM/yyyy}", fontItemRes, XBrushes.Black, 200, 420);
            
            gfx.DrawString("CEP:", fontItem, XBrushes.Black, 45, 450);
            gfx.DrawString($"{ticket.AppUser?.Zipcode}", fontItemRes, XBrushes.Black, 200, 450);
            
            gfx.DrawString("Pagamento", font, XBrushes.Black, 35, 480);

            var gatewayPayment = ticket.GatewayPayment ?? "";
            gfx.DrawString("Meio pagamento:", fontItem, XBrushes.Black, 45, 510);
            gfx.DrawString(gatewayPayment, fontItemRes, XBrushes.Black, 200, 510);
            
            var idTransaction = ticket.IdTransaction ?? "";
            gfx.DrawString("ID Transação:", fontItem, XBrushes.Black, 45, 540);
            gfx.DrawString(idTransaction, fontItemRes, XBrushes.Black, 200, 540);
            
            gfx.DrawString("Data da compra:", fontItem, XBrushes.Black, 45, 570);
            if (ticket.PurchaseDate != null)
                gfx.DrawString(ticket.PurchaseDate.Value.ToLocalString(), fontItemRes, XBrushes.Black, 200, 570);

            gfx.DrawString($"Documento gerado em {DateTime.UtcNow.ToLocalString()}", fontItemRes, XBrushes.Black, 35, 800);
            
            MemoryStream stream = new MemoryStream();

            document.Info.Title = $"Cartela - {ticket.TicketNumber}";
            document.Info.Author = "Totolec Show";
            document.Info.Creator = "Totolec Show - Online";
            document.Info.CreationDate = DateTime.UtcNow;
            
            document.SecuritySettings.PermitAccessibilityExtractContent = false;
            document.SecuritySettings.PermitAnnotations = false;
            document.SecuritySettings.PermitAssembleDocument = false;
            document.SecuritySettings.PermitExtractContent = false;
            document.SecuritySettings.PermitFormsFill = false;
            document.SecuritySettings.PermitFullQualityPrint = true;
            document.SecuritySettings.PermitModifyDocument = false;
            document.SecuritySettings.PermitPrint = false;
            
            document.Save(stream);
  
            stream.Position = 0;
  
            FileStreamResult fileStreamResult = new FileStreamResult(stream, "application/pdf");
              
            fileStreamResult.FileDownloadName = $"{ticket.TicketNumber}-{DateTime.UtcNow.ToLocalString()}.pdf";
            
            return fileStreamResult;
        }

        public static FileStreamResult ExportTicketPdf(Ticket ticket, byte[] stream)
        {
            var fontResolverType = GlobalFontSettings.FontResolver.GetType(); 
            if (fontResolverType != typeof(FontResolver))
            {
                GlobalFontSettings.FontResolver = new FontResolver();
            }
            
            PdfDocument document = new PdfDocument();
            
            PdfPage page = document.AddPage();

            XGraphics gfx = XGraphics.FromPdfPage(page);

            XImage image = XImage.FromStream(() => new MemoryStream(stream));

            gfx.DrawImage(image, 0, 0,600, 850);
            
            XFont font = new XFont("OpenSans", 12, XFontStyle.Bold);
            XFont fontDetails = new XFont("OpenSans", 10, XFontStyle.Bold);
            gfx.DrawString(ticket.TicketNumber, font, XBrushes.Black, 450, 63);
            gfx.DrawString(ticket.TicketNumber, font, XBrushes.Black, 47, 733);
            gfx.DrawString($"{ticket.AppUser.FirstName} {ticket.AppUser.LastName}", fontDetails, XBrushes.Black, 67, 675);
            gfx.DrawString(ticket.AppUser.PhoneNumber, fontDetails, XBrushes.Black, 206, 722);
            gfx.DrawString(ticket.AppUser.SocialId, fontDetails, XBrushes.Black, 430, 721);
            gfx.DrawString("Totolec Show - Online", fontDetails, XBrushes.Black, 450, 748);
            
            MemoryStream result = new MemoryStream();

            document.Info.Title = $"Cartela - {ticket.TicketNumber}";
            document.Info.Author = "Totolec Show";
            document.Info.Creator = "Totolec Show - Online";
            document.Info.CreationDate = DateTime.UtcNow;
            
            document.SecuritySettings.PermitAccessibilityExtractContent = false;
            document.SecuritySettings.PermitAnnotations = false;
            document.SecuritySettings.PermitAssembleDocument = false;
            document.SecuritySettings.PermitExtractContent = false;
            document.SecuritySettings.PermitFormsFill = false;
            document.SecuritySettings.PermitFullQualityPrint = true;
            document.SecuritySettings.PermitModifyDocument = false;
            document.SecuritySettings.PermitPrint = false;
            
            document.Save(result);
  
            result.Position = 0;
  
            FileStreamResult fileStreamResult = new FileStreamResult(result, "application/pdf");
              
            fileStreamResult.FileDownloadName = $"{ticket.TicketNumber}-{DateTime.UtcNow.ToLocalString()}.pdf";
            
            return fileStreamResult;
        }
    }
}