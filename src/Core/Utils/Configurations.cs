﻿using System;
using Microsoft.Extensions.Configuration;

namespace Core.Utils
{
    public static class Configurations
    {
        public static void AddProjectConfigurations(this IConfigurationBuilder config, string environmentName)
        {
            Console.WriteLine($"Loading environment: {environmentName}");
            
            config.AddJsonFile("appsettings.json", false, true);
            config.AddJsonFile($"appsettings.{environmentName}.json", true);
            config.AddEnvironmentVariables();
        }
    }
}