using System;
using Microsoft.AspNetCore.Razor.TagHelpers;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;

namespace Core.Utils
{
    public class GoogleAnalyticsTagHelperComponent : TagHelperComponent
    {
        private readonly string _trackingCode;
        
        public GoogleAnalyticsTagHelperComponent(IConfiguration configuration)
        {
            _trackingCode = configuration["TrackingCode"];
        }

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            if (string.Equals(output.TagName, "head", StringComparison.OrdinalIgnoreCase))
            {
                if (!string.IsNullOrEmpty(_trackingCode))
                {
                    output.PostContent
                        .AppendHtml("<script async src='https://www.googletagmanager.com/gtag/js?id=")
                        .AppendHtml(_trackingCode)
                        .AppendHtml("'></script><script>window.dataLayer=window.dataLayer||[];function gtag(){dataLayer.push(arguments)}gtag('js',new Date);gtag('config','")
                        .AppendHtml(_trackingCode)
                        .AppendHtml("',{displayFeaturesTask:'null'});</script>");
                }
            }
        }
    }
}