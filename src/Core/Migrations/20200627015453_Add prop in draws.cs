﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Core.Migrations
{
    public partial class Addpropindraws : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "description_of_draw",
                table: "draw",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "description_of_result",
                table: "draw",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "url_commercial",
                table: "draw",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "description_of_draw",
                table: "draw");

            migrationBuilder.DropColumn(
                name: "description_of_result",
                table: "draw");

            migrationBuilder.DropColumn(
                name: "url_commercial",
                table: "draw");
        }
    }
}
