﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Core.Migrations
{
    public partial class AddentityQueueItem : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "queue_item",
                columns: table => new
                {
                    guid = table.Column<Guid>(nullable: false),
                    draw_guid = table.Column<Guid>(nullable: true),
                    user_guid = table.Column<Guid>(nullable: true),
                    type = table.Column<int>(nullable: false),
                    status_queue_item = table.Column<int>(nullable: false),
                    file_path = table.Column<string>(nullable: true),
                    result_file_path = table.Column<string>(nullable: true),
                    distributor = table.Column<string>(nullable: true),
                    created_at = table.Column<DateTime>(nullable: false),
                    modified_at = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_queue_item", x => x.guid);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "queue_item");
        }
    }
}
