﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Core.Migrations
{
    public partial class AddpropIsActiveinDraws : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "is_active",
                table: "draw",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "is_active",
                table: "draw");
        }
    }
}
