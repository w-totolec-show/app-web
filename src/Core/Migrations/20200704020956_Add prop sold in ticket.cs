﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Core.Migrations
{
    public partial class Addpropsoldinticket : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "fk_ticket_draw_draw_guid1",
                table: "ticket");

            migrationBuilder.DropIndex(
                name: "ix_ticket_draw_guid1",
                table: "ticket");

            migrationBuilder.DropColumn(
                name: "draw_guid1",
                table: "ticket");

            migrationBuilder.AddColumn<bool>(
                name: "sold",
                table: "ticket",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "sold",
                table: "ticket");

            migrationBuilder.AddColumn<Guid>(
                name: "draw_guid1",
                table: "ticket",
                type: "uuid",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "ix_ticket_draw_guid1",
                table: "ticket",
                column: "draw_guid1");

            migrationBuilder.AddForeignKey(
                name: "fk_ticket_draw_draw_guid1",
                table: "ticket",
                column: "draw_guid1",
                principalTable: "draw",
                principalColumn: "guid",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
