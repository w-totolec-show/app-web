﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Core.Migrations
{
    public partial class ModifypropTicketNumber : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "full_ticket_number",
                table: "ticket");

            migrationBuilder.AddColumn<string>(
                name: "ticket_number",
                table: "ticket",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ticket_number",
                table: "ticket");

            migrationBuilder.AddColumn<string>(
                name: "full_ticket_number",
                table: "ticket",
                type: "text",
                nullable: true);
        }
    }
}
