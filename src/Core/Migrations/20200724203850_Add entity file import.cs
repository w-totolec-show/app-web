﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Core.Migrations
{
    public partial class Addentityfileimport : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "file_import",
                columns: table => new
                {
                    guid = table.Column<Guid>(nullable: false),
                    draw_guid = table.Column<Guid>(nullable: false),
                    type_file = table.Column<int>(nullable: false),
                    status = table.Column<int>(nullable: false),
                    file_name = table.Column<string>(nullable: true),
                    file_name_internal = table.Column<string>(nullable: true),
                    file_uri = table.Column<string>(nullable: true),
                    created_at = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_file_import", x => x.guid);
                    table.ForeignKey(
                        name: "fk_file_import_draw_draw_guid",
                        column: x => x.draw_guid,
                        principalTable: "draw",
                        principalColumn: "guid",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "ix_file_import_draw_guid",
                table: "file_import",
                column: "draw_guid");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "file_import");
        }
    }
}
