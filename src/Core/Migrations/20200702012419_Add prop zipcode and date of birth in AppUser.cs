﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Core.Migrations
{
    public partial class AddpropzipcodeanddateofbirthinAppUser : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "complete_registration",
                table: "asp_net_users",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "date_of_birth",
                table: "asp_net_users",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "zipcode",
                table: "asp_net_users",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "complete_registration",
                table: "asp_net_users");

            migrationBuilder.DropColumn(
                name: "date_of_birth",
                table: "asp_net_users");

            migrationBuilder.DropColumn(
                name: "zipcode",
                table: "asp_net_users");
        }
    }
}
