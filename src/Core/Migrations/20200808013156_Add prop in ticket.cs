﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Core.Migrations
{
    public partial class Addpropinticket : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "fk_credit_card_token_asp_net_users_app_user_id1",
                table: "credit_card_token");

            migrationBuilder.DropIndex(
                name: "ix_credit_card_token_app_user_id1",
                table: "credit_card_token");

            migrationBuilder.DropColumn(
                name: "app_user_id1",
                table: "credit_card_token");

            migrationBuilder.AddColumn<string>(
                name: "gateway_payment",
                table: "ticket",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "ix_logs_user_modification",
                table: "logs",
                column: "user_modification");

            migrationBuilder.AddForeignKey(
                name: "fk_logs_asp_net_users_user_modification",
                table: "logs",
                column: "user_modification",
                principalTable: "asp_net_users",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "fk_logs_asp_net_users_user_modification",
                table: "logs");

            migrationBuilder.DropIndex(
                name: "ix_logs_user_modification",
                table: "logs");

            migrationBuilder.DropColumn(
                name: "gateway_payment",
                table: "ticket");

            migrationBuilder.AddColumn<Guid>(
                name: "app_user_id1",
                table: "credit_card_token",
                type: "uuid",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "ix_credit_card_token_app_user_id1",
                table: "credit_card_token",
                column: "app_user_id1");

            migrationBuilder.AddForeignKey(
                name: "fk_credit_card_token_asp_net_users_app_user_id1",
                table: "credit_card_token",
                column: "app_user_id1",
                principalTable: "asp_net_users",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
