﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Core.Migrations
{
    public partial class AddentitiesofCardandDraw : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "draw",
                columns: table => new
                {
                    guid = table.Column<Guid>(nullable: false),
                    draw_number = table.Column<int>(nullable: false),
                    name = table.Column<string>(nullable: true),
                    original_layout_name = table.Column<string>(nullable: true),
                    layout_uri = table.Column<string>(nullable: true),
                    draw_date = table.Column<DateTime>(nullable: false),
                    created_at = table.Column<DateTime>(nullable: false),
                    modified_at = table.Column<DateTime>(nullable: true),
                    accomplished = table.Column<bool>(nullable: false),
                    observations = table.Column<string>(nullable: true),
                    parameters = table.Column<IDictionary<string, string>>(type: "jsonb", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_draw", x => x.guid);
                });

            migrationBuilder.CreateTable(
                name: "card",
                columns: table => new
                {
                    guid = table.Column<Guid>(nullable: false),
                    draw_guid = table.Column<Guid>(nullable: false),
                    card_number = table.Column<int>(nullable: false),
                    created_at = table.Column<DateTime>(nullable: false),
                    purchase_date = table.Column<DateTime>(nullable: true),
                    user_guid = table.Column<Guid>(nullable: true),
                    draw_guid1 = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_card", x => x.guid);
                    table.ForeignKey(
                        name: "fk_card_draw_draw_guid",
                        column: x => x.draw_guid,
                        principalTable: "draw",
                        principalColumn: "guid",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_card_draw_draw_guid1",
                        column: x => x.draw_guid1,
                        principalTable: "draw",
                        principalColumn: "guid",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_card_asp_net_users_user_guid",
                        column: x => x.user_guid,
                        principalTable: "asp_net_users",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "ix_card_draw_guid",
                table: "card",
                column: "draw_guid");

            migrationBuilder.CreateIndex(
                name: "ix_card_draw_guid1",
                table: "card",
                column: "draw_guid1");

            migrationBuilder.CreateIndex(
                name: "ix_card_user_guid",
                table: "card",
                column: "user_guid");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "card");

            migrationBuilder.DropTable(
                name: "draw");
        }
    }
}
