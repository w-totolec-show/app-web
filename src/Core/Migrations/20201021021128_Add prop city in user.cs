﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Core.Migrations
{
    public partial class Addpropcityinuser : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "city",
                table: "asp_net_users",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "locality",
                table: "asp_net_users",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "city",
                table: "asp_net_users");

            migrationBuilder.DropColumn(
                name: "locality",
                table: "asp_net_users");
        }
    }
}
