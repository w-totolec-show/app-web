﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Core.Migrations
{
    public partial class Refactoringticket : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "tickect_balls",
                table: "ticket");

            migrationBuilder.AddColumn<int[]>(
                name: "ticket_balls",
                table: "ticket",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ticket_balls",
                table: "ticket");

            migrationBuilder.AddColumn<string>(
                name: "tickect_balls",
                table: "ticket",
                type: "text",
                nullable: true);
        }
    }
}
