﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Core.Migrations
{
    public partial class Addentitiecreditcardtoken : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "credit_card_token",
                columns: table => new
                {
                    guid = table.Column<Guid>(nullable: false),
                    user_guid = table.Column<Guid>(nullable: false),
                    token = table.Column<string>(nullable: true),
                    brand = table.Column<string>(nullable: true),
                    card_type = table.Column<string>(nullable: true),
                    app_user_id1 = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_credit_card_token", x => x.guid);
                    table.ForeignKey(
                        name: "fk_credit_card_token_asp_net_users_app_user_id1",
                        column: x => x.app_user_id1,
                        principalTable: "asp_net_users",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_credit_card_token_asp_net_users_user_guid",
                        column: x => x.user_guid,
                        principalTable: "asp_net_users",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "ix_credit_card_token_app_user_id1",
                table: "credit_card_token",
                column: "app_user_id1");

            migrationBuilder.CreateIndex(
                name: "ix_credit_card_token_user_guid",
                table: "credit_card_token",
                column: "user_guid");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "credit_card_token");
        }
    }
}
