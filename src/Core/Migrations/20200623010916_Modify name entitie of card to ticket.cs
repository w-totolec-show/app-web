﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Core.Migrations
{
    public partial class Modifynameentitieofcardtoticket : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "card");

            migrationBuilder.AddColumn<DateTime>(
                name: "end_date_purchase",
                table: "draw",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "start_date_purchase",
                table: "draw",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<decimal>(
                name: "value_of_ticket",
                table: "draw",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.CreateTable(
                name: "ticket",
                columns: table => new
                {
                    guid = table.Column<Guid>(nullable: false),
                    draw_guid = table.Column<Guid>(nullable: false),
                    ticket_number = table.Column<int>(nullable: false),
                    created_at = table.Column<DateTime>(nullable: false),
                    purchase_date = table.Column<DateTime>(nullable: true),
                    user_guid = table.Column<Guid>(nullable: true),
                    draw_guid1 = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_ticket", x => x.guid);
                    table.ForeignKey(
                        name: "fk_ticket_draw_draw_guid",
                        column: x => x.draw_guid,
                        principalTable: "draw",
                        principalColumn: "guid",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_ticket_draw_draw_guid1",
                        column: x => x.draw_guid1,
                        principalTable: "draw",
                        principalColumn: "guid",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_ticket_asp_net_users_user_guid",
                        column: x => x.user_guid,
                        principalTable: "asp_net_users",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "ix_ticket_draw_guid",
                table: "ticket",
                column: "draw_guid");

            migrationBuilder.CreateIndex(
                name: "ix_ticket_draw_guid1",
                table: "ticket",
                column: "draw_guid1");

            migrationBuilder.CreateIndex(
                name: "ix_ticket_user_guid",
                table: "ticket",
                column: "user_guid");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ticket");

            migrationBuilder.DropColumn(
                name: "end_date_purchase",
                table: "draw");

            migrationBuilder.DropColumn(
                name: "start_date_purchase",
                table: "draw");

            migrationBuilder.DropColumn(
                name: "value_of_ticket",
                table: "draw");

            migrationBuilder.CreateTable(
                name: "card",
                columns: table => new
                {
                    guid = table.Column<Guid>(type: "uuid", nullable: false),
                    card_number = table.Column<int>(type: "integer", nullable: false),
                    created_at = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    draw_guid = table.Column<Guid>(type: "uuid", nullable: false),
                    draw_guid1 = table.Column<Guid>(type: "uuid", nullable: true),
                    purchase_date = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    user_guid = table.Column<Guid>(type: "uuid", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_card", x => x.guid);
                    table.ForeignKey(
                        name: "fk_card_draw_draw_guid",
                        column: x => x.draw_guid,
                        principalTable: "draw",
                        principalColumn: "guid",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_card_draw_draw_guid1",
                        column: x => x.draw_guid1,
                        principalTable: "draw",
                        principalColumn: "guid",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_card_asp_net_users_user_guid",
                        column: x => x.user_guid,
                        principalTable: "asp_net_users",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "ix_card_draw_guid",
                table: "card",
                column: "draw_guid");

            migrationBuilder.CreateIndex(
                name: "ix_card_draw_guid1",
                table: "card",
                column: "draw_guid1");

            migrationBuilder.CreateIndex(
                name: "ix_card_user_guid",
                table: "card",
                column: "user_guid");
        }
    }
}
