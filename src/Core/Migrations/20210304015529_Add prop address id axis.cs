﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Core.Migrations
{
    public partial class Addpropaddressidaxis : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "axis_address_id",
                table: "asp_net_users",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "axis_address_id",
                table: "asp_net_users");
        }
    }
}
