﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Core.Migrations
{
    public partial class AddpropsinTicket : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ticket_number",
                table: "ticket");

            migrationBuilder.AddColumn<int>(
                name: "draw_year",
                table: "ticket",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "extraction_number",
                table: "ticket",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "full_ticket_number",
                table: "ticket",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "simplified_ticket_number",
                table: "ticket",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "tickect_balls",
                table: "ticket",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "draw_year",
                table: "ticket");

            migrationBuilder.DropColumn(
                name: "extraction_number",
                table: "ticket");

            migrationBuilder.DropColumn(
                name: "full_ticket_number",
                table: "ticket");

            migrationBuilder.DropColumn(
                name: "simplified_ticket_number",
                table: "ticket");

            migrationBuilder.DropColumn(
                name: "tickect_balls",
                table: "ticket");

            migrationBuilder.AddColumn<int>(
                name: "ticket_number",
                table: "ticket",
                type: "integer",
                nullable: false,
                defaultValue: 0);
        }
    }
}
