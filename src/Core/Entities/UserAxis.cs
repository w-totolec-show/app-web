namespace Core.Entities
{
    public class UserAxis
    {
        public string id { get; set; }
        public string title { get; set; }
        public string message { get; set; }
        public bool success { get; set; }
        public Data data { get; set; }
    }

    public class Data
    {
        public string userId { get; set; }
    }
}