namespace Core.Entities
{
    public class Payment
    {
        public string id { get; set; }
        public string chargeId { get; set; }
        public string status { get; set; }
        public string failReason { get; set; }
    }
}