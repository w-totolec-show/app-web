namespace Core.Entities
{
    public class DetailsErrorChange
    {
        public string field { get; set; }
        public string message { get; set; }
        public string errorCode { get; set; }
    }
}