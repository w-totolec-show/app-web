namespace Core.Entities
{
    public class AddressAxis
    {
        public string id { get; set; }
        public string title { get; set; }
        public string message { get; set; }
        public bool success { get; set; }
        public DataAddress data { get; set; }
    }
    
    public class DataAddress
    {
        public string addressId { get; set; }
    }
}