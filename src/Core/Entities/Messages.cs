using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Core.Entities
{
    public class Messages : IEntityTypeConfiguration<Messages>
    {
        public Guid Guid { get; set; } 
        public string Message { get; set; }
        public TypeMessage TypeMessage { get; set; }
        public DateTime CreatedAt { get; set; }
        
        public void Configure(EntityTypeBuilder<Messages> builder)
        {
            builder.HasKey(a => a.Guid);
        }
    }
    
    public enum TypeMessage
    {
        Email,
        Checkout,
        NextDraw
    }
}