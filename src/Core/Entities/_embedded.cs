using System.Collections.Generic;

namespace Core.Entities
{
    public class _embedded
    {
        public IList<charge> charges { get; set; }
    }
}