using System;
using System.Runtime.Serialization;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Core.Entities
{
    public class FileImport : IEntityTypeConfiguration<FileImport>
    {
        public Guid Guid { get; set; } 
        public Guid? DrawGuid { get; set; }
        public Guid? UserGuid { get; set; }
        public TypeFile TypeFile { get; set; }
        public Status Status { get; set; }
        public string FileName { get; set; }
        public string FileNameInternal { get; set; }
        public string FileUri { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime ModifiedAt { get; set; }
        
        [IgnoreDataMember]
        public Draw Draw { get; set; }

        public void Configure(EntityTypeBuilder<FileImport> builder)
        {
            builder.HasKey(a => a.Guid);
            
            builder.HasOne(d => d.Draw)
                .WithMany(a => a.FileImports)
                .HasForeignKey(d => d.DrawGuid);
        }
    }

    public enum Status
    {
        Processed,
        Processing,
        Imported,
        AwaitingProcessing,
        Failure
    }

    public enum TypeFile
    {
        Ticket,
        ImgOfTicket,
        Regulations,
        Banner,
        ImgOfEmail,
        ImgOfCheckout,
        LogTicketsSold,
        ImgNextDraw
    }
}