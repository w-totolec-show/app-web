using System;
using System.Runtime.Serialization;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Core.Entities
{
    public class Ticket : IEntityTypeConfiguration<Ticket>
    {
        public Guid Guid { get; set; }
        public Guid DrawGuid { get; set; }
        public string TicketNumber { get; set; }
        public string SimplifiedTicketNumber { get; set; }
        public string ExtractionNumber { get; set; }
        public int[] TicketBalls { get; set; }
        public string DrawYear { get; set; }
        public bool Sold { get; set; }
        public bool IsActive { get; set; }
        public string IdTransaction { get; set; }
        public string GatewayPayment { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? PurchaseDate { get; set; }
        public Guid? UserGuid { get; set; }
        
        [IgnoreDataMember]
        public Draw Draw { get; set; }
        
        [IgnoreDataMember]
        public AppUser AppUser { get; set; }
        
        public void Configure(EntityTypeBuilder<Ticket> builder)
        {
            builder.HasKey(a => a.Guid);
            
            builder.HasOne(d => d.Draw)
                .WithMany(a => a.Tickets)
                .HasForeignKey(d => d.DrawGuid);
            
            builder.HasOne(c => c.AppUser)
                .WithMany()
                .HasForeignKey(a => a.UserGuid);
        }
    }
}