using System.Collections.Generic;

namespace Core.Entities
{
    public class PaymentChange
    {
        /*public _embedded _embedded { get; set; }
        public string timestamp { get; set; }
        public string status { get; set; }
        public string error { get; set; }
        public IList<DetailsErrorChange> details { get; set; }
        public string path { get; set; }
        public string transactionId { get; set; }
        public IList<Payment> payments { get; set; }*/
        
        public string id { get; set; }
        public string title { get; set; }
        public string message { get; set; }
        public bool success { get; set; }
        public DataPaymentChange data { get; set; }
    }
    
    public class DataPaymentChange
    {
        public string paymentId { get; set; }
        public string paymentStatus { get; set; }
        public string uniqueClientReferenceId { get; set; }
    }
}