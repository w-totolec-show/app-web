﻿using System;

namespace Core.Entities.AppLogs
{
    public class PropertyLog
    {
        public int LogLevel { get; set; }
        public string Before { get; set; }        
        public string After { get; set; }
        public string Property { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
