﻿using System.Collections.Generic;

namespace Core.Entities.AppLogs
{
    public enum LogType
    {
        Users = 0,
        Draw = 1,
        TicketsSold = 2,
        Banner = 3,
        TermsOfService = 4,
        MessageOfEmail = 5,
        ImgOfEmail = 6,
        MessageOfCheckout = 7,
        ImgOfCheckout = 8,
        ImgNextDraw = 9,
        MessageNextDraw = 10
    }

    public static class LogTypeExtensionMethods
    {
        private static IDictionary<string, LogType> dic =
                new Dictionary<string, LogType>
                {
                    {"Users", LogType.Users},
                };

        private static IDictionary<LogType, string> inverseDic =
            new Dictionary<LogType, string>
            {
                    {LogType.Users, "Users"},
            };
    }
}