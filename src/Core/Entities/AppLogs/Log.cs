﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Core.Entities.AppLogs
{
    public class Log : IEntityTypeConfiguration<Log>
    {
        public Guid Guid { get; set; }
        public Guid? UserGuid { get; set; }
        public LogType LogType { get; set; }

        [Column(TypeName = "jsonb")]
        public IList<PropertyLog> PropertyLogs { get; set; }
        public DateTime CreatedAt { get; set; }
        public Guid UserModification { get; set; }
        
        [IgnoreDataMember]
        public AppUser AppUser { get; set; }

        public void Configure(EntityTypeBuilder<Log> builder)
        {
            builder.HasKey(lg => lg.Guid);
            
            builder.HasOne(c => c.AppUser)
                .WithMany()
                .HasForeignKey(a => a.UserModification);
        }
    }
}
