using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Core.Entities
{
    public class Draw : IEntityTypeConfiguration<Draw>
    {
        public Draw()
        {
            Tickets = new List<Ticket>();
            FileImports = new List<FileImport>();
        }
        
        public Guid Guid { get; set; }
        public int DrawNumber { get; set; }
        public string Name { get; set; }
        public string OriginalLayoutName { get; set; }
        public string LayoutUri { get; set; }
        public decimal ValueOfTicket { get; set; }
        
        public DateTime DrawDate { get; set; }
        public DateTime StartDatePurchase { get; set; }
        public DateTime EndDatePurchase { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? ModifiedAt { get; set; }
        public bool Accomplished { get; set; }
        public string Observations { get; set; }
        public string UrlCommercial { get; set; }
        public string DescriptionOfDraw { get; set; }
        public string DescriptionOfResult { get; set; }
        public bool IsActive { get; set; }
        [Column(TypeName = "jsonb")]
        public IDictionary<string, string> Parameters { get; set; }
        public IList<Ticket> Tickets { get; set; }
        public IList<FileImport> FileImports { get; set; }

        public void Configure(EntityTypeBuilder<Draw> builder)
        {
            builder.HasKey(a => a.Guid);
        }
        
        public bool IsValid()
        {
            var guards = new[]
            {
                !string.IsNullOrWhiteSpace(Name),
                !string.IsNullOrWhiteSpace(OriginalLayoutName),
                !string.IsNullOrWhiteSpace(LayoutUri)
            };

            return guards.All(g => g);
        }
    }
}