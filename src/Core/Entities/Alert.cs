using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Core.Entities
{
    public class Alert : IEntityTypeConfiguration<Alert>
    {
        public Guid Guid { get; set; } 
        public string Message { get; set; }
        public DateTime CreatedAt { get; set; }
        public bool IsActive { get; set; }
        public void Configure(EntityTypeBuilder<Alert> builder)
        {
            builder.HasKey(a => a.Guid);
        }
    }
}