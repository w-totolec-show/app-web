using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Core.Entities
{
    public class TermsOfService : IEntityTypeConfiguration<TermsOfService>
    {
        public Guid Guid { get; set; } 
        public string Message { get; set; }
        public DateTime CreatedAt { get; set; }
        
        public void Configure(EntityTypeBuilder<TermsOfService> builder)
        {
            builder.HasKey(a => a.Guid);
        }
    }
}