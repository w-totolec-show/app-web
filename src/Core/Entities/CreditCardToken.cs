using System;
using System.Runtime.Serialization;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Core.Entities
{
    public class CreditCardToken : IEntityTypeConfiguration<CreditCardToken>
    {
        public Guid Guid { get; set; }
        
        public Guid UserGuid { get; set; }
        
        public string Token { get; set; }

        public string Brand { get; set; }

        public string CardType { get; set; }

        public string LastFourNumbers { get; set; }
        
        public DateTime CreatedAt { get; set; }
        
        [IgnoreDataMember]
        public AppUser AppUser { get; set; }
        
        public void Configure(EntityTypeBuilder<CreditCardToken> builder)
        {
            builder.HasKey(a => a.Guid);
            
            builder.HasOne(c => c.AppUser)
                .WithMany()
                .HasForeignKey(a => a.UserGuid);
        }
    }
}