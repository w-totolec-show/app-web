using System.Collections.Generic;

namespace Core.Entities
{
    public class TokenizeCreditCard
    {
        public string creditCardId { get; set; }
        /*public string last4CardNumber { get; set; }
        public string expirationMonth { get; set; }
        public string expirationYear { get; set; }
        public string timestamp { get; set; }
        public string status { get; set; }
        public string error { get; set; }
        public IList<DetailsErrorChange> details { get; set; }*/

        public string id { get; set; }
        public string title { get; set; }
        public string message { get; set; }
        public bool success { get; set; }
        public DataTokenizeCreditCard data { get; set; }
    }
    
    public class DataTokenizeCreditCard
    {
        public string cardId { get; set; }
    }
}