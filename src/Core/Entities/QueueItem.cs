using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Core.Entities
{
    public class QueueItem : IEntityTypeConfiguration<QueueItem>
    {
        public Guid Guid { get; set; } 
        public Guid? DrawGuid { get; set; }
        public Guid? UserGuid { get; set; }
        public Type Type { get; set; }
        public StatusQueueItem StatusQueueItem { get; set; }
        public string FilePath { get; set; }
        public string ResultFilePath { get; set; }
        public string Distributor { get; set; }
        public string FileName { get; set; }
        public string FileNameInternal { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime ModifiedAt { get; set; }
        
        public void Configure(EntityTypeBuilder<QueueItem> builder)
        {
            builder.HasKey(a => a.Guid);
        }
    }
    
    public enum StatusQueueItem
    {
        Processed,
        Processing,
        Imported,
        AwaitingProcessing,
        Failure
    }
    
    public enum Type
    {
        ExportLot
    }
}