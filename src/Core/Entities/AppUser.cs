﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Core.Entities
{
    public class AppUser : IdentityUser<Guid>, IEntityTypeConfiguration<AppUser>
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string SocialId { get; set; } // CPF
        public DateTime CreatedAt { get; set; }
        public DateTime ModifiedAt { get; set; }
        public string Observations { get; set; }
        public bool IsActive { get; set; }
        public bool IsAdmin { get; set; }
        public bool CompleteRegistration { get; set; }
        public string Zipcode { get; set; }
        public string City { get; set; }
        public string AddressNumber { get; set; }
        public string Locality { get; set; }
        public DateTime DateOfBirth { get; set; }

        [Column(TypeName = "jsonb")]
        public IDictionary<string, string> Data { get; set; }

        [Column(TypeName = "jsonb")]
        public Address Address { get; set; }

        public string AxisUserId { get; set; }
        public string AxisAddressId { get; set; }
        
        public void Configure(EntityTypeBuilder<AppUser> builder)
        {
        }

        public bool IsValid()
        {
            var guards = new[]
            {
                !string.IsNullOrWhiteSpace(UserName),
                !string.IsNullOrWhiteSpace(FirstName),
                !string.IsNullOrWhiteSpace(LastName),
                !string.IsNullOrWhiteSpace(Email)
            };

            return guards.All(g => g);
        }
    }
}