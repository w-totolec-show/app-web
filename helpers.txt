aws ecs register-task-definition --cli-input-json file://taskdef-web.json --profile totolec > /dev/null
aws ecs register-task-definition --cli-input-json file://taskdef-tasks.json --profile totolec > /dev/null

aws ecs create-service --service-name svc-app-web --cli-input-json file://create-service-web.json --profile totolec
aws ecs create-service --service-name svc-app-tasks --cli-input-json file://create-service-tasks.json --profile totolec




=== Configs
aws ssm put-parameter --name "MyStringParameter" --value "Vici" --type "SecureString" --overwrite  > /dev/null

aws ssm put-parameter --name "Prod-AllowedHosts" --value "" --type "SecureString" --overwrite

aws ssm put-parameter --name "Prod-AwsS3AccessKeyId" --value "" --type "SecureString" --overwrite
aws ssm put-parameter --name "Prod-AwsS3SecretAccessKey" --value "" --type "SecureString" --overwrite
aws ssm put-parameter --name "Prod-AwsS3Bucket" --value "" --type "SecureString" --overwrite
aws ssm put-parameter --name "Prod-AwsS3Region" --value "" --type "SecureString" --overwrite
aws ssm put-parameter --name "Prod-AwsS3ServiceURL" --value "" --type "SecureString" --overwrite

aws ssm put-parameter --name "Prod-CdnPath" --value "" --type "SecureString" --overwrite

aws ssm put-parameter --name "Prod-PostgreHost" --value "prod-app-web.chhweqd2avei.us-east-1.rds.amazonaws.com" --type "SecureString" --overwrite
aws ssm put-parameter --name "Prod-PostgrePort" --value "" --type "SecureString" --overwrite
aws ssm put-parameter --name "Prod-PostgrePooling" --value "" --type "SecureString" --overwrite
aws ssm put-parameter --name "Prod-PostgreDatabase" --value "" --type "SecureString" --overwrite
aws ssm put-parameter --name "Prod-PostgreUserId" --value "" --type "SecureString" --overwrite
aws ssm put-parameter --name "Prod-PostgrePassword" --value "" --type "SecureString" --overwrite

aws ssm put-parameter --name "Prod-RedisHost" --value "" --type "SecureString" --overwrite
aws ssm put-parameter --name "Prod-RedisPort" --value "" --type "SecureString" --overwrite
aws ssm put-parameter --name "Prod-RedisPassword" --value "" --type "SecureString" --overwrite

aws ssm put-parameter --name "Prod-FacebookAppId" --value "" --type "SecureString" --overwrite
aws ssm put-parameter --name "Prod-FacebookAppSecret" --value "" --type "SecureString" --overwrite

aws ssm put-parameter --name "Prod-GoogleClientId" --value "" --type "SecureString" --overwrite
aws ssm put-parameter --name "Prod-GoogleClientSecret" --value "" --type "SecureString" --overwrite

aws ssm put-parameter --name "Prod-TrackingCode" --value "" --type "SecureString" --overwrite

aws ssm put-parameter --name "Prod-TokenSafe2Pay" --value "" --type "SecureString" --overwrite
aws ssm put-parameter --name "Prod-SecretKeySafe2Pay" --value "" --type "SecureString" --overwrite
aws ssm put-parameter --name "Prod-CallbackUrlSafe2pay" --value "" --type "SecureString" --overwrite
aws ssm put-parameter --name "Prod-IsSandbox" --value "" --type "SecureString" --overwrite

aws ssm put-parameter --name "Prod-ClientIdJuno" --value "" --type "SecureString" --overwrite --profile totolec
aws ssm put-parameter --name "Prod-ClientSecretJuno" --value "" --type "SecureString" --overwrite --profile totolec
aws ssm put-parameter --name "Prod-PublicTokenJuno" --value "" --type "SecureString" --overwrite --profile totolec
aws ssm put-parameter --name "Prod-PrivateTokenJuno" --value "" --type "SecureString" --overwrite --profile totolec
aws ssm put-parameter --name "Prod-AuthorizationServerJuno" --value "" --type "SecureString" --overwrite --profile totolec
aws ssm put-parameter --name "Prod-URLChargeJuno" --value "" --type "SecureString" --overwrite --profile totolec
aws ssm put-parameter --name "Prod-TokenizeCreditCardJuno" --value "" --type "SecureString" --overwrite --profile totolec
aws ssm put-parameter --name "Prod-PaymentChargeJuno" --value "" --type "SecureString" --overwrite --profile totolec
aws ssm put-parameter --name "Prod-ConsultCharge" --value "" --type "SecureString" --overwrite --profile totolec
